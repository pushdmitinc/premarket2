<?php

use yii\db\Migration;

class m240618_183845_friend extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%vk_friend}}', [
            'id' => $this->primaryKey(),
            'vk_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'nickname' => $this->string(),
            'track_code' => $this->string(),
            'sex' => $this->tinyInteger(),
            'photo_100' => $this->string(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'can_access_closed' => $this->boolean(),
            'is_closed' => $this->boolean(),
            'delete_date' => $this->date(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%vk_friend_log}}', [
            'id' => $this->primaryKey(),
            'friend_id' => $this->integer(),
            'type' => $this->tinyInteger(),
            'date' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('inx_friend_vk_id', '{{%vk_friend}}', 'vk_id');
        $this->createIndex('inx_friend_user_id', '{{%vk_friend}}', 'user_id', true);

        $this->createTable('{{%vk_friend_online}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'date_start' => $this->dateTime()->notNull(),
            'date_finish' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('inx_friend_online_user_id', '{{%vk_friend_online}}', 'user_id');
    }

    public function down()
    {
        $this->dropTable('{{%vk_friend_log}}');
        $this->dropTable('{{%vk_friend}}');
        $this->dropTable('{{%vk_friend_online}}');
    }
}
