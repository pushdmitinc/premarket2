<?php

use yii\db\Migration;

class m230208_181048_note extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%note_group}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'name' => $this->string()->notNull()->comment('Название'),
            'sort' => $this->integer()->comment('Сортировка'),
        ], $tableOptions);

        $this->createIndex('note_group_user_id', '{{%note_group}}', 'user_id');

        $this->createTable('{{%note}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'group_id' => $this->integer()->comment('Группа'),
            'stock_id' => $this->integer()->comment('Акция'),
            'text' => $this->text()->notNull()->comment('Текст'),
            'sort' => $this->integer()->comment('Сортировка'),
            'date' => $this->date()->defaultExpression('now()')->comment('Дата'),
        ], $tableOptions);

        $this->createIndex('note_user_id', '{{%note}}', 'user_id');
        $this->createIndex('note_stock_id', '{{%note}}', 'stock_id');
        $this->createIndex('note_group_id', '{{%note}}', 'group_id');
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('{{%note}}');
        $this->dropTable('{{%note_group}}');
    }
}
