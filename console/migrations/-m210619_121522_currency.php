<?php

use yii\db\Migration;

class m210619_121522_currency extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%test_currency}}', [
            'id' => $this->primaryKey(),
            'code' => $this->char(3)->notNull()->unique()->comment('Код валюты'),
            'name' => $this->string()->notNull()->comment('Название'),
            'nominal' => $this->integer()->defaultValue(1)->comment('Номинал'),
            'rate' => $this->double(4)->notNull()->comment('Курс валюты к рублю'),
            'date' => $this->date()->notNull()->comment('Дата обновления'),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('{{%test_currency}}');
    }
}
