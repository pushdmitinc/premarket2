<?php

use yii\db\Migration;

class m230213_102731_calendar extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%calendar}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull()->comment('Название'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'alias' => $this->string()->notNull()->unique()->comment('Алиас'),
            'image' => $this->string()->comment('Картинка'),
            'date_start' => $this->date()->comment('Дата начала'),
            'description_short' => $this->string()->comment('Описание краткое'),
            'keywords' => $this->string()->comment('Ключевые слова'),
            'description' => $this->text()->comment('Описание'),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
            'params' => $this->json()->comment('Параметры'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('ind_calendar_user_id', '{{%calendar}}', 'user_id');

        $this->createTable('{{%calendar_event}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull()->comment('Дата'),
            'time' => $this->time()->null()->comment('Время'),
            'calendar_id' => $this->integer()->notNull()->comment('Календарь'),
            'parent_id' => $this->integer()->comment('Основное событие'),
            'status_id' => $this->integer()->defaultValue(0)->comment('Статус'),
            'name' => $this->string()->notNull()->comment('Название'),
            'value' => $this->integer()->comment('Значение'),
            'image' => $this->string()->comment('Картинка'),
            'style_class' => $this->string()->comment('Класс стилей'),
            'description_short' => $this->string()->comment('Описание краткое'),
            'description' => $this->text()->comment('Описание'),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
            'user_id' => $this->integer()->comment('Пользователь'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('ind_calendar_event_calendar_id', '{{%calendar_event}}', 'calendar_id');
        $this->createIndex('ind_calendar_event_data', '{{%calendar_event}}', 'date');
        $this->createIndex('ind_calendar_event_time', '{{%calendar_event}}', 'time');

        $this->createTable('{{%calendar_user}}', [
            'id' => $this->primaryKey(),
            'calendar_id' => $this->integer()->notNull()->comment('Календарь'),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
        ], $tableOptions);

        $this->createIndex('ind_calendar_user_user_id', '{{%calendar_user}}', 'user_id');
        $this->createIndex('ind_calendar_user_calendar_id', '{{%calendar_user}}', 'calendar_id');
    }

    public function down()
    {
        $this->dropTable('{{%calendar_user}}');
        $this->dropTable('{{%calendar_event}}');
        $this->dropTable('{{%calendar}}');
    }
}
