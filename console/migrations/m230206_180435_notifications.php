<?php

use yii\db\Migration;

class m230206_180435_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%stock_notification}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'stock_id' => $this->integer()->notNull()->comment('Акция'),
            'portfolio_id' => $this->integer()->comment('Портфель'),
            'stock_price' => $this->double(6)->comment('Цена акции'),
            'take_profit' => $this->double(6)->comment('Тейк профит'),
            'stop_loss' => $this->double(6)->comment('Стоп лосс'),
            'text' => $this->string()->comment('Текст'),
            'date' => $this->date()->defaultExpression('now()')->comment('Дата'),
        ], $tableOptions);

        $this->createIndex('stock_notification_user_id', '{{%stock_notification}}', 'user_id');
        $this->createIndex('stock_notification_stock_id', '{{%stock_notification}}', 'stock_id');
        $this->createIndex('stock_notification_portfolio_id', '{{%stock_notification}}', 'portfolio_id');
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('{{%stock_notification}}');
    }
}
