<?php

use yii\db\Migration;

class m220617_204924_dividend extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%dividend}}', [
            'id' => $this->primaryKey(),
            'stock_id' => $this->integer()->notNull()->comment('Акция'),
            'dividend' => $this->double(9)->notNull()->comment('Дивиденды'),
            'period' => $this->string()->comment('Период'),
            'stock_price' => $this->double(9)->comment('Цена закрытия инструмента'),
            'currency' => $this->string(9)->comment('Валюта'),
            'date' => $this->date(),
            'date_t2' => $this->date(),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
        ], $tableOptions);

        $this->createTable('{{%dividend_portfolio}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->notNull()->comment('Портфель'),
            'portfolio_stock_id' => $this->integer()->notNull()->comment('Позиция'),
            'dividend_id' => $this->integer()->comment('Дивиденды'),
            'dividend' => $this->double(2)->notNull()->comment('Дивиденды'),
            'date' => $this->date()->notNull()->comment('Дата'),
            'type' => $this->tinyInteger()->defaultValue(1)->comment('Тип'),
            'comment' => $this->string()->comment('Комментарий'),
        ], $tableOptions);

        $this->createIndex('dividend_stock_id', '{{%dividend}}', 'stock_id');
        $this->createIndex('dividend_portfolio_stock_id', '{{%dividend_portfolio}}', 'portfolio_stock_id');
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('{{%dividend_portfolio}}');
        $this->dropTable('{{%dividend}}');
    }
}
