<?php

use yii\db\Migration;

class m221230_195948_portfolio_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%portfolio_history}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->notNull()->comment('Портфель'),
            'price_start' => $this->decimal()->notNull()->comment('Цена начальная'),
            'price_current' => $this->decimal()->notNull()->comment('Цена на дату'),
            'date' => $this->date()->notNull()->comment('Сортировка'),
        ], $tableOptions);

        $this->createIndex('ind-portfolio_history_portfolio_id', '{{%portfolio_history}}', 'portfolio_id');
        $this->createIndex('ind-portfolio_history_date', '{{%portfolio_history}}', 'date');
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('{{%portfolio_history}}');
    }
}
