<?php

use yii\db\Migration;

class m241213_160424_create_stock_future_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('stock_future', [
            'id' => $this->primaryKey(),
            'stock_id' => $this->integer()->notNull()->comment('Акция'),
            'future_id' => $this->integer()->notNull()->unique()->comment('Фьючерс'),
            'quantity' => $this->integer()->notNull()->comment('Акции во фьючерсе'),
            'percent_diff' => $this->double(3)->comment('Разница с активом'),
            'price_future' => $this->double(5)->comment('Цена фьючерса'),
            'price_stock' => $this->double(5)->comment('Цена акции'),
            'quantity_position' => $this->integer()->comment('Куплено'),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
        ], $tableOptions);

        $this->createIndex('stock_future_stock_id', 'stock_future', 'stock_id');
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('stock_future');
    }
}
