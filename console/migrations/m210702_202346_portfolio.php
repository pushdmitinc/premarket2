<?php

use yii\db\Migration;

class m210702_202346_portfolio extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%portfolio}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->comment('Пользователь'),
            'name' => $this->string()->notNull()->comment('Название'),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createTable('{{%portfolio_stock}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->notNull()->comment('Портфель'),
            'stock_id' => $this->integer()->notNull()->comment('Акция'),
            'price' => $this->double(6)->notNull()->comment('Цена'),
            'price_close' => $this->double(6)->comment('Цена закрытия'),
            'quantity' => $this->integer()->notNull()->comment('Кол-во'),
            'date' => $this->date()->comment('Дата'),
            'date_close' => $this->date()->notNull()->comment('Дата закрытия'),
            'take_profit' => $this->double(6)->comment('Тейк профит'),
            'stop_loss' => $this->double(6)->comment('Стоп лосс'),
            'date_purpose' => $this->date()->comment('Дата цели'),
            'commission' => $this->double(6)->defaultValue(0)->comment('Комиссия'),
            'currency_price' => $this->double(6)->comment('Курс покупки валюты'),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
        ], $tableOptions);

        $this->createIndex('portfolio_stock_portfolio_id', '{{%portfolio_stock}}', 'portfolio_id');
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropTable('{{%portfolio_stock}}');
        $this->dropTable('{{%portfolio}}');
    }
}
