<?php

use yii\db\Migration;

class m220722_182534_sector extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up(): void
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%portfolio_sector}}', [
            'id' => $this->primaryKey(),
            'portfolio_id' => $this->integer()->notNull()->comment('Портфель'),
            'name' => $this->string()->notNull()->comment('Сектор'),
            'sort' => $this->integer()->defaultValue(1)->comment('Сортировка'),
        ], $tableOptions);

        $this->addColumn('{{%portfolio_stock}}',
            'sector_id', $this->integer()->comment('Сектор')
        );

        $this->createIndex('portfolio_sector_portfolio_id',
            '{{%portfolio_sector}}', 'portfolio_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function down(): void
    {
        $this->dropColumn('{{%portfolio_stock}}', 'sector_id');

        $this->dropTable('{{%portfolio_sector}}');
    }
}
