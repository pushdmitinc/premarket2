<?php

use yii\db\Migration;

class m231203_163523_world extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%world}}', [
            'id' => $this->primaryKey(),
            'world' => $this->string()->notNull()->unique()->comment('Слово'),
            'signs' => $this->integer()->defaultValue(0)->comment('Признаки'),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%world}}');
    }
}
