<?php

use yii\db\Migration;

class m200609_201034_securities extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%stock}}', [
            'id' => $this->primaryKey(),
            'figi' => $this->string(12)->unique()->comment('Figi-идентификатор инструмента'),
            'ticker' => $this->string(36)->notNull()->comment('Тикер'),
            'isin' => $this->string(36)->comment('Isin-идентификатор инструмента'),
            'type' => $this->smallInteger()->defaultValue(1)->comment('Тип инструмента 1 - акция, 2 - валюта'),

            'class_code' => $this->string(12)->comment('Класс-код (секция торгов)'),
            'lot' => $this->integer()->comment('Размер лота'),
            'currency' => $this->string(12)->comment('Валюта'),
            'name' => $this->string()->comment('Название'),
            'exchange' => $this->string()->comment('Торговая площадка'),
            'real_exchange' => $this->string()->comment('Реальная площадка исполнения расчётов'),
            'nominal' => $this->double(9)->comment('Номинальная стоимость'),
            'min_price_increment' => $this->double(9)->comment('Шаг цены'),
            'uid' => $this->string()->comment('Уникальный идентификатор инструмента'),

            'date_ipo' => $this->date()->comment('Дата IPO акции в часовом поясе UTC'),
            'issue_size' => $this->bigInteger()->comment('Размер выпуска'),
            'sector' => $this->string()->comment('Сектор экономики'),

            'iso_currency_name' => $this->string()->comment('Строковый ISO-код валюты'),

            'price_prev' => $this->double(9)->comment('Предварительная цена'),
            'date_prev' => $this->date()->comment('Предварительная дата'),

            'price_ytd_max' => $this->double(9)->comment('Мах. цена за год'),
            'price_ytd_min' => $this->double(9)->comment('Мин. цена за год'),

            'percent_ytd_max' => $this->double(2)->comment('Отклонение % от мах'),
            'percent_ytd_min' => $this->double(2)->comment('Отклонение % от мин'),

            'percent_ytd' => $this->double(2)->comment('Изменение с начала года %'),
            'percent_year' => $this->double(2)->comment('Изм, год %'),
            'percent_month' => $this->double(2)->comment('Изм, месяц %'),
            'percent_mtd' => $this->double(2)->comment('Изменение с начала месяца %'),
            'percent_week' => $this->double(2)->comment('Изм, неделя %'),
            'percent_wtd' => $this->double(2)->comment('Изменение с начала недели %'),
            'percent_day' => $this->double(2)->comment('Изм, день %'),

            'is_upload' => $this->boolean()->defaultValue(false)->comment('Обновление'),
            'is_portfolio' => $this->boolean()->defaultValue(false)->comment('Есть в портфелях'),
        ], $tableOptions);

        $this->createTable('{{%stock_history}}', [
            'id' => $this->primaryKey(),
            'stock_id' => $this->integer()->notNull()->comment('Ид валюты'),
            'date' => $this->date()->comment('Дата'),
            'price_low' => $this->double(6)->comment('Цена минимальная'),
            'price_open' => $this->double(6)->comment('Цена открытия'),
            'price_close' => $this->double(6)->comment('Цена закрытия'),
            'price_high' => $this->double(6)->comment('Цена максимальная'),
            'volume' => $this->bigInteger()->comment('Объем'),
        ], $tableOptions);

        $this->createIndex('stock_history_stock_id_date_uid',
            '{{%stock_history}}', ['stock_id', 'date'], true
        );
    }

    public function down()
    {
        $this->dropTable('{{%stock_history}}');
        $this->dropTable('{{%stock}}');
    }
}
