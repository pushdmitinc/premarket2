<?php

declare(strict_types=1);

namespace console\controllers;

use common\components\clients\CbrClient;
use common\models\Currency;
use DateTime;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;

/**
 * Для работы с АПИ сайта www.cbr.ru
 *
 * Class CbrController
 * @package console\controllers
 */
class CbrController extends Controller
{
    /**
     * Обновить курсы валют
     * php yii cbr/currency
     *
     * @return int
     * @throws \common\components\clients\ClientException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionCurrency(): int
    {
        $client = new CbrClient();

        $data = $client->currency();
        $date = new DateTime($data['@attributes']['Date']);

        foreach ($data['Valute'] as $item) {
            $currency = Currency::findOne(['code' => $item['CharCode']]);

            if (!$currency instanceof Currency) {
                $currency = new Currency();
                $currency->code = $item['CharCode'];
            }

            $rate = strtr($item['Value'], [',' => '.']);

            $currency->name = $item['Name'];
            $currency->nominal = $item['Nominal'];
            $currency->rate = (float) $rate;
            $currency->date = $date->format('Y-m-d');

            if ($currency->save()) {
                $this->stdout('Обновлена валюта - ' . $item['CharCode'] . PHP_EOL);
            } else {
                $this->stdout('Не удалось обновить ' . $item['CharCode'] . PHP_EOL, Console::FG_RED);
            }
        }

        return ExitCode::OK;
    }
}
