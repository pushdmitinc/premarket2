<?php

declare(strict_types=1);

namespace console\controllers;

use common\helpers\ArrayHelper;
use common\models\VkFriend;
use common\models\VkFriendOnline;
use DateTime;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\BaseConsole;

/**
 * Class FriendController
 * @package console\controllers
 * @see FriendController
 */
class FriendController extends Controller
{
    /**
     * ./yii friend/test $userId
     *
     * @param int $userId
     *
     * @return int
     *
     * @throws yii\db\Exception
     * @throws yii\httpclient\Exception
     */
    public function actionTest(int $userId): int
    {
        $count = 1;
        $dateStart = date('Y-m-d H:i:s');
        $data = Yii::$app->vk->api('friends.get', $userId);

        $error = ArrayHelper::getValue($data, 'error.error_msg');

        if ($error !== null) {
            $this->stdout($error . PHP_EOL, BaseConsole::FG_RED);

            return ExitCode::UNSPECIFIED_ERROR;
        }

        $this->stdout($data['response']['count'] . PHP_EOL);

        foreach ($data['response']['items'] as $item) {
            $model = VkFriend::findOne([
                'vk_id' => $userId,
                'user_id' => $item['id'],
            ]);

            if ($model === null) {
                $model = new VkFriend();
                $model->vk_id = $userId;
                $model->user_id = $item['id'];
            } else {
                $model->updated_at = $dateStart;
                $model->delete_date = null;
            }

            if ($model->isNewRecord || $item['first_name'] !== VkFriend::NAME_DELETED) {
                $model->load($item, '');
            }

            if ($model->isNewRecord || $model->isAttributeChanged('delete_date')) {
                if ($count > 2) {
                    $this->stdout(PHP_EOL);
                }

                $stdout = $model->first_name . ' ' . $model->last_name . ' (' . $model->user_id . ')';
                $this->stdout($stdout . PHP_EOL, BaseConsole::FG_GREEN);
            } else {
                $this->stdout('.');
            }

            $model->save();

            $count++;
        }

        $this->stdout(PHP_EOL);

        $models = VkFriend::find()
            ->where(['<', 'updated_at', $dateStart])
            ->andWhere(['delete_date' => null])
            ->all();

        foreach ($models as $model) {
            $model->delete_date = date('Y-m-d');
            $model->save();

            $stdout = $model->first_name . ' ' . $model->last_name . ' (' . $model->user_id . ')';
            $this->stdout($stdout . PHP_EOL, BaseConsole::FG_RED);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii friend/online $userId
     *
     * @param int $userId
     * @param int $sleep
     *
     * @return int
     *
     * @throws yii\httpclient\Exception
     */
    public function actionOnline(int $userId, int $sleep = 30): int
    {
        $isOnline = null;
        $dateStart = null;
        $model = VkFriendOnline::findCurrent($userId);

        while (true) {
            try {
                $data = Yii::$app->vk->api('users.get', $userId, 'online');
                $error = ArrayHelper::getValue($data, 'error.error_msg');
            } catch (yii\httpclient\Exception $e) {
                $error = $e->getMessage();
            }

            if ($error !== null) {
                $this->stdout(PHP_EOL . $error . PHP_EOL, BaseConsole::FG_RED);

                Yii::$app->telegram->sendVkOnline($error);

                return ExitCode::UNSPECIFIED_ERROR;
            }

            $online = (bool) ArrayHelper::getValue($data, 'response.0.online');
            $name = ArrayHelper::getValue($data, 'response.0.first_name');
            $name = $name . ' ' . ArrayHelper::getValue($data, 'response.0.last_name');

            if ($online === true) {
                if ($model === null) {
                    $model = VkFriendOnline::findOrCreate($userId);
                }
            } else {
                if ($model !== null) {
                    $model->finish();

                    $model = null;
                }
            }

            if ($online !== $isOnline) {
                $text = $name . ' (' . $userId . ') ' . ($online ? 'онлайн' : 'офлайн');
                $dateCurrent = new DateTime();

                if ($dateStart instanceof DateTime) {
                    $interval = $dateStart->diff($dateCurrent);
                    $interval = Yii::$app->formatter->asDuration($interval);

                    $text .= ', ' . $interval;

                    $this->stdout(PHP_EOL . $interval . PHP_EOL);
                }

                $this->stdout($dateCurrent->format('Y-m-d H:i:s') . ' ');

                if ($isOnline !== null) {
                    Yii::$app->telegram->sendVkOnline($text);
                }

                $isOnline = $online;
                $dateStart = $dateCurrent;
            }


            $this->stdout($online ? '+' : '-');

            sleep($sleep);
        }
    }
}
