<?php

declare(strict_types=1);

namespace console\controllers;

use common\models\World;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\BaseConsole;
use yii\httpclient\Client;

class WorldController extends Controller
{
    /**
     * ./yii world/test *а*о*
     */
    public function actionTest(string $search): int
    {
        $url = 'https://gospodaretsva.com/5-bukv?dir=2';
        $url .= '&name-directory-search-famous-letter=';
        $url .= '&name-directory-search-not-famous-letter=';
        $url .= '&name-directory-search-value=' . $search;
        $response = (new Client())->get($url)->send();

        $document = new \DOMDocument('1.0', 'utf-8');

        libxml_use_internal_errors(true);
        $document->loadHTML($response->getContent());
        libxml_use_internal_errors(false);

        $tables = $document->getElementsByTagName('strong');

        /** @var \DOMElement $item */
        foreach ($tables as $item) {
            if (mb_strlen($item->nodeValue) !== 5) {
                continue;
            }

            $model = new World();
            $model->world = mb_strtolower($item->nodeValue);

            $color = $model->save() ? BaseConsole::FG_GREEN : BaseConsole::FG_RED;

            $this->stdout($model->world . PHP_EOL, $color);
        }

        return ExitCode::OK;
    }
}
