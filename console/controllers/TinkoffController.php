<?php
namespace console\controllers;

use common\components\exception\ValidationModelException;
use common\helpers\ArrayHelper;
use common\helpers\SignsHelper;
use common\helpers\StockHelper;
use common\helpers\TinkoffHelper;
use common\models\PortfolioStock;
use common\models\stock\StockEntity;
use common\models\stock\StockFuture;
use common\models\stock\StockHistory;
use Yii;
use yii\base\InvalidConfigException;
use yii\console\ExitCode;
use yii\helpers\BaseConsole;
use yii\httpclient\Exception;
use ZipArchive;

class TinkoffController extends BaseController
{
    /**
     * ./yii tinkoff/shares
     *
     * @param int $flag
     * @param null $ticker
     *
     * @return mixed
     *
     * @throws ValidationModelException
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function actionShares(int $flag = 1, $ticker = null): int
    {
        $count = 0;

        $fileName = Yii::getAlias('@console/runtime/shares.txt');
        if ($flag === 2) {
            file_put_contents($fileName, '', FILE_APPEND);
        }

        if ($ticker) {
            $data = ['instruments' => Yii::$app->tinkoff->shareBy($ticker)];
        } else {
            $data = Yii::$app->tinkoff->shares();
        }

        foreach ($data['instruments'] as $item) {
            if ($flag === 1) {
                TinkoffHelper::loadStock($item);
            }

            $stdout = '+ ' . $item['ticker'] . ' (' . $item['figi'] . ') ' . ++$count . PHP_EOL;

            if ($flag === 2) {
                file_put_contents($fileName, $stdout, FILE_APPEND);
            }

            $this->stdout($stdout);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii tinkoff/bond RU000A106TM6
     *
     * @param null $ticker
     *
     * @return mixed
     *
     * @throws ValidationModelException
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function actionBond($ticker, $code = 'TQCB'): int
    {
        $count = 0;

        $data = ['instruments' => Yii::$app->tinkoff->bondBy($ticker, $code)];

        foreach ($data['instruments'] as $item) {
            TinkoffHelper::loadStock($item, StockEntity::TYPE_BOND);

            $stdout = '+ ' . $item['ticker'] . ' (' . $item['figi'] . ') ' . ++$count . PHP_EOL;

            $this->stdout($stdout);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii tinkoff/future IMOEXF
     *
     * @param null $ticker
     *
     * @return mixed
     *
     * @throws ValidationModelException
     * @throws InvalidConfigException
     * @throws Exception
     */
    public function actionFuture($ticker, $code = 'SPBFUT'): int
    {
        $count = 0;

        $data = ['instruments' => Yii::$app->tinkoff->futureBy($ticker, $code)];

        foreach ($data['instruments'] as $item) {
            TinkoffHelper::loadStock($item, StockEntity::TYPE_FUTURE);

            $stdout = '+ ' . $item['ticker'] . ' (' . $item['figi'] . ') ' . ++$count . PHP_EOL;

            $this->stdout($stdout);
        }

        return ExitCode::OK;
    }

    /**
     * php yii tinkoff/currencies
     *
     * @return int
     *
     * @throws ValidationModelException
     */
    public function actionCurrencies(): int
    {
        $count = 0;
        $data = Yii::$app->tinkoff->currencies();

        foreach ($data['instruments'] as $item) {
            TinkoffHelper::loadStock($item, StockEntity::TYPE_CURRENCY);

            $this->stdout('+ ' . $item['ticker'] . ' (' . $item['figi'] . ') ' . ++$count . PHP_EOL);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii tinkoff/etf
     *
     * @param null|string $ticker
     *
     * @return int
     *
     * @throws ValidationModelException
     */
    public function actionEtf($ticker = null): int
    {
        $count = 0;

        if ($ticker) {
            $data = ['instruments' => Yii::$app->tinkoff->etfBy($ticker)];
        } else {
            $data = Yii::$app->tinkoff->etfs();
        }

        foreach ($data['instruments'] as $item) {
            TinkoffHelper::loadStock($item, StockEntity::TYPE_ETF);

            $this->stdout('+ ' . $item['ticker'] . ' (' . $item['figi'] . ') ' . ++$count . PHP_EOL);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii tinkoff/dividends 1 2023-01-01 2023-12-31
     *
     * @param int $portfolioId
     * @param string|null $dateFrom
     * @param string|null $dateTo
     *
     * @return int
     *
     * @throws ValidationModelException
     */
    public function actionDividends(int $portfolioId, string $dateFrom = null, string $dateTo = null): int
    {
        /** @var PortfolioStock[] $stock */
        $portfolioStocks = PortfolioStock::find()
            ->where(['portfolio_id' => $portfolioId])
            ->indexBy('stock_id')
            ->with(['stock'])
            ->all();

        foreach ($portfolioStocks as $portfolioStock) {
            $count = 0;
            $stock = $portfolioStock->stock;

            if (!in_array($stock->type, [StockEntity::TYPE_STOCK, StockEntity::TYPE_BOND])) {
                continue;
            }

            if ($stock->type === StockEntity::TYPE_BOND) {
                $data = Yii::$app->tinkoff->bondEvents($stock->figi, $dateFrom, $dateTo);
                $data = $data['events'] ?? [];
            } else {
                $data = Yii::$app->tinkoff->dividends($stock->figi, $dateFrom, $dateTo);
                $data = $data['dividends'] ?? [];
            }

            $this->stdout('+ ' . $stock->ticker . ' (' . $stock->figi . ') ');

            foreach ($data as $item) {
                if ($stock->type === StockEntity::TYPE_BOND) {
                    TinkoffHelper::loadCoupon($stock, $item);
                } else {
                    TinkoffHelper::loadDividend($stock, $item);
                }

                $count++;

                $this->stdout('+');
            }

            $this->stdout(' ' . $count . PHP_EOL);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii tinkoff/history 2024 1 P3D
     * ./yii tinkoff/history 2024 3 0 RU000A106540
     *
     * @return mixed
     *
     * @throws ValidationModelException
     * @throws yii\base\InvalidConfigException
     * @throws yii\httpclient\Exception
     */
    public function actionHistory(int $startYear = null, int $countYear = 1, string $interval = null, string $ticker = null): int
    {
        $count = 0;

        $query = StockEntity::find()
            ->where(['not', ['figi' => null]])
            ->orderBy(['is_portfolio' => SORT_DESC]);

        if ($ticker) {
            $query->andWhere(['ticker' => $ticker]);
        } else {
            $query->andWhere(['is_upload' => false]);
        }

        if (!$interval) {
            $interval = null;
        }

        $startYear =  (int) ($startYear ?: date('Y'));
        $countTotal = (clone $query)->count();

        /** @var StockEntity[] $stocks */
        foreach ($query->batch() as $stocks) {
            foreach ($stocks as $stock) {
                $this->stdout('#' . $stock->id . ' ' . $stock->name . ' ' . $stock->ticker . '(' . $stock->figi . ') ' . PHP_EOL);

                $year = $startYear;

                while ($year > ($startYear - $countYear)) {
                    $data = Yii::$app->tinkoff->history($stock->figi, $year, $interval);

                    $this->stdout($year--);

                    if (count($data) === 0) {
                        $this->stdout(PHP_EOL);
                        continue;
                    }

                    foreach ($data as $item) {
                        TinkoffHelper::loadHistory($stock, $item);
                        $this->stdout('.');
                    }

                    $this->stdout('.' . PHP_EOL);
                }

                StockHelper::changeParams($stock);

                $remained = $countTotal - $count;
                $this->stdout($stock->date_prev . ' ' .$stock->price_prev);
                $this->stdout(' ' . ++$count . ' ' . $countTotal . ' ' . $remained . PHP_EOL);

                $stock->is_upload = true;
                $stock->save();
            }
        }

        TinkoffHelper::updateStockForUpload();

        return ExitCode::OK;
    }

    /**
     * ./yii tinkoff/history-batch
     *
     * @return mixed
     *
     * @throws yii\base\InvalidConfigException
     * @throws yii\httpclient\Exception
     */
    public function actionHistoryBatch(): int
    {
        $count = 0;

        $query = StockEntity::find()
            ->where(['not', ['figi' => null]])
            ->andWhere(['is_upload' => false]);

        $countTotal = (clone $query)->count();

        /** @var StockEntity[] $stocks */
        foreach ($query->batch() as $stocks) {
            foreach ($stocks as $stock) {
                $this->stdout($stock->ticker);

                $batchInsert = [];
                $year = date('Y');

                $data = Yii::$app->tinkoff->history($stock->figi, $year);

                StockHistory::deleteAll(['and',
                    ['stock_id' => $stock->id],
                    ['between', 'date', $year . '-01-01', $year . '-12-31'],
                ]);

                $this->stdout(' .');

                foreach ($data as $item) {
                    $batchInsert[] = TinkoffHelper::prepareDataHistory($item) + ['stock_id' => $stock->id];
                }

                $column = ['date', 'price_open', 'price_low', 'price_high', 'price_close', 'stock_id'];
                Yii::$app->db->createCommand()->batchInsert(StockHistory::tableName(), $column, $batchInsert);

                StockHelper::changeParams($stock);

                $this->stdout(' ' . $stock->date_prev . ' ' . $stock->price_prev);
                $this->stdout(' (' . $stock->figi . ') ' . ++$count . ' ' . $countTotal . PHP_EOL);

                $stock->is_upload = true;
                $stock->save();
            }
        }

        return ExitCode::OK;
    }

	/**
	 * php yii tinkoff/market-data [1, 2, 3]
	 *
	 * @param int $signs
	 * @param int|null $portfolioId
	 * @param int $recursive
	 *
	 * @return int
	 *
	 * @throws ValidationModelException
	 * @throws \yii\db\Exception
	 */
    public function actionMarketData(int $signs = 0, ?int $portfolioId = null, int $recursive = 0): int
    {
        $count = 0;

        $query = TinkoffHelper::queryForMarketData($signs, $portfolioId);

        $countTotal = (clone $query)->count();

        /** @var StockEntity[] $stocks */
        foreach ($query->batch() as $stocks) {
            $figi = ArrayHelper::getColumn($stocks, 'figi', false);
            $data = Yii::$app->tinkoff->marketData($figi);

            foreach ($data as $item) {
				++$count;

                if (($item['time'] ?? null) === null) {
                    continue;
                }

                $stock = $stocks[$item['figi']];
                $stockOldPrice = $stock->price_prev;

                TinkoffHelper::loadMarketData($stock, $item);

                if ($recursive === 0 || ($recursive % 10) === ($stock->id % 10)) {
                    StockHelper::changeParams($stock);
                }

				$stdout = '#' . $stock->id . ' ' . $stock->name . ' ' . $stock->ticker . '(' . $stock->figi . ')';

				if (!$stockOldPrice) {
					$this->stdout($stdout, BaseConsole::BG_RED);
					$this->stdout(PHP_EOL);

					continue;
				}

                $percent = round($stock->price_prev / $stockOldPrice * 100 - 100, 2);
                $color = $percent === 0.0 ? null : ($percent < 0 ? BaseConsole::FG_RED : BaseConsole::FG_GREEN);

                $this->stdout($stdout);
                $this->stdout(' ' . $stockOldPrice . ' -> ' . $stock->price_prev . ' ' . $percent . '%', $color);
                $this->stdout(' ' . $count . ' ' . $countTotal . PHP_EOL);

                TinkoffHelper::afterMarketDataStock($stock, $signs);
            }
        }

        TinkoffHelper::afterMarketData($signs);

        if ($recursive !== 0 && $recursive < 101) {
            $this->stdout('recursive ' . $recursive . PHP_EOL);

            sleep(5);

            $this->actionMarketData($signs, ++$recursive);
        }

        TinkoffHelper::updateStockForUpload();

        return ExitCode::OK;
    }

    /**
     * @param string $dir
     *
     * @return int
     *
     * @throws ValidationModelException
     */
    public function actionDownload(string $dir): int
    {
        $count = 0;
        $files = scandir($dir);

        /** @var StockEntity[] $stocks */
        foreach ($files as $file) {
            if (!strpos($file, '.zip')) {
                continue;
            }

            [$figi] = explode('_', strtr($file, ['.zip' => '']));

            $stock = StockEntity::findOne(['figi' => $figi]);

            if ($stock === null) {
                $this->stdout('figi not fond - ' . $figi . PHP_EOL, BaseConsole::FG_RED);
                continue;
            }

            $data = [];

            $zip = new ZipArchive();
            $zip->open($dir . '/' . $file);

            $i = 0;
            while($name = $zip->getNameIndex($i++)) {
                $contents = [];
                [, $date] = explode('_', strtr($name, ['.csv' => '']));

                $fp = $zip->getStream($name);

                while (!feof($fp)) {
                    $contents[] = fgetcsv($fp, 100, ';');
                }

                foreach ($contents as $item) {
                    $data[$date] = [
                        'date' => $item[1],
                        'price_open' => $data[$date]['price_open'] ?? $item[2],
                        'price_low' => ($data[$date]['price_low'] ?? 100000) < $item[3] ? $data[$date]['price_low'] : $item[3],
                        'price_high' => ($data[$date]['price_high'] ?? 0) > $item[4] ? $data[$date]['price_high'] : $item[4],
                        'price_close' => $item[5],
                        'volume' => ($data[$date]['volume'] ?? 0) + $item[6],
                    ];
                }

                foreach ($data as $item) {
                    TinkoffHelper::loadHistory($stock, $item);

                    $this->stdout('Добавлен figi - ' . $figi . ++$count . PHP_EOL);
                }
            }

            $zip->close();
        }

        return ExitCode::OK;
    }
}
