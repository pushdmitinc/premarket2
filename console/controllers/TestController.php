<?php

declare(strict_types=1);

namespace console\controllers;

use DateTime;
use yii\console\Controller;
use yii\console\ExitCode;

/**
 * Class TestController
 * @package console\controllers
 */
class TestController extends Controller
{
    /**
     * php yii test/test
     */
    public function actionTest(): int
    {
        $count = 0;
        $rand = [1.2, 0.9];

        for ($j = 0; $j < 1000; ++$j) {
            $a = 100;
            $b = 100;

            for ($i = 0; $i < 10; ++$i) {
                $a = $a * 1.05;
                $b = $b * $rand[array_rand($rand)];
            }

            $this->stdout($a . '-' . $b . '-' . array_rand($rand) . PHP_EOL);

            if ($b > $a) {
                $count++;
                $this->stdout($count . PHP_EOL);
            }
        }

        var_dump($count / 1000 * 100);

        return ExitCode::OK;
    }

    /**
     * php yii test/door
     */
    public function actionDoor(int $num): int
    {
        $array = range(1, $num + 1);

        unset($array[0]);

        foreach ($array as $key => $value) {
            $array[$key] = 0;

            if (fmod(sqrt($key), 1) === 0.0) {
                $array[$key] = 1;
            }
        }

        var_dump($array);

        return ExitCode::OK;
    }

	/**
	 * ./yii test/days 2024-08-29
	 *
	 * @param string $date1
	 * @param string $date2
	 *
	 * @return int
	 *
	 * @throws \Exception
	 */
    public function actionDays(string $date1, string $date2 = 'now'): int
    {
        $dateStart = new DateTime($date1);
        $dateFinish = new DateTime($date2);

		$this->stdout($dateStart->diff($dateFinish)->days . PHP_EOL);

        return ExitCode::OK;
    }

	/**
	 * ./yii test/day-of-year 2024-09-13
	 *
	 * @param string $date
	 *
	 * @return int
	 *
	 * @throws \Exception
	 */
    public function actionDayOfYear(string $date = 'now'): int
    {
        $dateStart = new DateTime($date);

		$this->stdout($dateStart->format('z') . PHP_EOL);

        return ExitCode::OK;
    }

	/**
	 * ./yii test/sum 49937537676 114,126,143,156
	 *
	 * @param int $number
	 * @param string $tmp
	 *
	 * @return int
	 *
	 * @throws \Exception
	 */
    public function actionSum(int $number, string $tmp): int
    {
		$tmp = explode(',', $tmp);

		foreach ($tmp as $item) {
			$result = $number / $item;

			if (is_int($result)) {
				var_dump($number, $item, $result);
				break;
			}
		}

        return ExitCode::OK;
    }

	/**
	 * ./yii test/sum1 114 156
	 *
	 * @param int $number1
	 * @param int $number2
	 *
	 * @return int
	 *
	 * @throws \Exception
	 */
    public function actionSum1(int $number1, int $number2): int
    {
		$i = 3;

		while (true) {
			$result = $number2 * $i / $number1;

			if (is_int($result)) {
				var_dump($i, $result, $number1, $number2);
				break;
			}

			var_dump($i);

			$i += 2;

			sleep(1);
		}

        return ExitCode::OK;
    }
}
