<?php

declare(strict_types=1);

namespace console\controllers;

use common\helpers\StockHelper;
use common\models\Dividend;
use common\models\DividendPortfolio;
use common\models\PortfolioHistory;
use common\models\PortfolioStock;
use common\models\stock\StockEntity;
use common\models\Portfolio;
use DateInterval;
use DateTime;
use yii\console\ExitCode;
use yii\helpers\BaseConsole;

/**
 * Class PortfolioController
 * @package console\controllers
 */
class PortfolioController extends BaseController
{
    /**
     * ./yii portfolio/dividend 1
     */
    public function actionDividend(int $id): int
    {
        $portfolio = Portfolio::findOne($id);

        foreach ($portfolio->portfolioStocksFull as $stock) {
            DividendPortfolio::deleteAll(['portfolio_stock_id' => $stock->id, 'type' => DividendPortfolio::TYPE_DIVIDEND]);

            foreach ($stock->dividends as $dividend) {
                if ($dividend->canSign(Dividend::SING_DELETE)) {
                    continue;
                }

                $price = $dividend->dividend;

                if ($dividend->currency !== StockHelper::getCurrencySystem()) {
                    $price = StockHelper::priceToCurrency($dividend->dividend, $dividend->currency);
                }

                $portfolioDividend = new DividendPortfolio();

                $portfolioDividend->dividend = round($price * $stock->quantity, 2);
                $portfolioDividend->dividend_id = $dividend->id;
                $portfolioDividend->portfolio_id = $portfolio->id;
                $portfolioDividend->portfolio_stock_id = $stock->id;
                $portfolioDividend->date = $dividend->date;

                $portfolioDividend->save();
            }

            $this->stdout($stock->id . ' ' . $stock->stock->ticker . ' - ' . count($stock->dividends) . PHP_EOL);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii portfolio/stock-portfolio
     */
    public function actionStockPortfolio(?int $portfolioId = null): int
    {
        $query = StockEntity::find();
        $query->innerJoin(['ps' => PortfolioStock::tableName()], 'ps.stock_id = stock.id');

		if ($portfolioId !== null) {
			$query->where(['portfolio_id' => $portfolioId]);
		}

        /** @var StockEntity $stock */
        foreach ($query->each() as $stock) {
            $stock->is_portfolio = true;
            $stock->is_upload = false;
            $stock->save(false);

            $this->stdout($stock->id . ' ' . $stock->ticker . PHP_EOL);
        }

        return ExitCode::OK;
    }

    /**
     * ./yii portfolio/history 1 2023-08-26
     */
    public function actionHistory(int $id, string $date = null): int
    {
        $portfolio = Portfolio::findOne($id);

        $data = [];
        $stockStart = $portfolio->portfolioStocksFull[0];
        $dateStart = $date ?: $stockStart->date;
        $date = new DateTime($dateStart);

//        $portfolioStock = PortfolioStock::findOne(228);
//        var_dump(StockHelper::priceForDate($portfolioStock, $dateStart)); die;

        while ($date->format('Y-m-d') <= date('Y-m-d')) {
            $dateFormat = $date->format('Y-m-d');

            foreach ($portfolio->portfolioStocksFull as $portfolioStock) {
                if ($portfolioStock->date > $dateFormat) {
                    continue;
                }

                if ($portfolioStock->date_close && $portfolioStock->date_close < $dateFormat) {
                    continue;
                }

                $priceDate = StockHelper::priceForDate($portfolioStock, $dateFormat);

                if (!$priceDate) {
                    $this->stdout($portfolioStock->stock->ticker . PHP_EOL, BaseConsole::FG_RED);
                    continue;
                }

                $priceStart = StockHelper::priceToCurrency($portfolioStock->getPrice(), $portfolioStock->stock->currency);
                $priceStart = round($priceStart * $portfolioStock->quantity, 2);

                $priceCurrent = StockHelper::priceToCurrency($priceDate, $portfolioStock->stock->currency);
                $priceCurrent = round($priceCurrent * $portfolioStock->quantity, 2);

                $data[$dateFormat]['date'] = $dateFormat;
                $data[$dateFormat]['price_start'] = ($data[$dateFormat]['price_start'] ?? 0) + $priceStart;
                $data[$dateFormat]['price_current'] = ($data[$dateFormat]['price_current'] ?? 0) + $priceCurrent;
            }

            $this->stdout(implode(';', $data[$dateFormat]) . PHP_EOL);

            $date->add(new DateInterval('P1D'));
        }

        $maxPercent = 0;
        $minPercent = null;

        PortfolioHistory::deleteAll(['and',
            ['portfolio_id' => $portfolio->id],
            ['>=', 'date', $dateStart],
        ]);

        foreach ($data as $item)
        {
            $model = new PortfolioHistory();

            $model->portfolio_id = $portfolio->id;
            $model->load($item, '');

            $this->stdout($model->save() ? '.' : '-');

            $model->save();

            $percent = round($item['price_current'] / $item['price_start'] * 100 - 100, 2);

            if ($maxPercent < $percent) {
                $maxPercent = $percent;
            }

            if (!$minPercent || $minPercent > $percent) {
                $minPercent = $percent;
            }
        }

        printf("\n%d %d\n", $maxPercent, $minPercent);

        return ExitCode::OK;
    }

    /**
     * php yii portfolio/load-file 1 runtime/stocks.csv
     */
    public function actionLoadFile(int $portfolio_id, string $file): int
    {

        if (($handle = fopen($file, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $stock = StockEntity::findOne(['ticker' => $data[0]]);

                if (!$stock) {
                    $this->stdout('-' . $data[0] . PHP_EOL, BaseConsole::FG_RED);
                    continue;
                }

                $date = (new DateTime($data[1]))->format('Y-m-d');
                $dateClose = (new DateTime($data[2]))->format('Y-m-d');

                $portfolioStock = PortfolioStock::findOne([
                    'portfolio_id' => $portfolio_id,
                    'stock_id' => $stock->id,
                    'date' => $date,
                    'date_close' => $dateClose,
                ]);

                if ($portfolioStock) {
                    continue;
                }

                $portfolioStock = new PortfolioStock();

                $portfolioStock->portfolio_id = $portfolio_id;
                $portfolioStock->stock_id = $stock->id;
                $portfolioStock->date = $date;
                $portfolioStock->date_close = $dateClose;
                $portfolioStock->quantity = $data[3];
                $portfolioStock->price = strtr($data[4], [',' => '.']);
                $portfolioStock->price_close = strtr($data[5], [',' => '.']);

                if ($portfolioStock->save()) {
                    $this->stdout('+' . $data[0] . PHP_EOL);
                } else {
                    var_dump($portfolioStock->getErrors());
                }
            }

            fclose($handle);
        }

        return ExitCode::OK;
    }
}
