<?php

declare(strict_types=1);

namespace console\controllers;

use common\components\exception\ValidationModelException;
use common\models\Dividend;
use common\models\Portfolio;
use common\models\PortfolioStock;
use common\models\stock\StockEntity;
use common\models\stock\StockHistory;
use DateTime;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\BaseConsole;
use yii\httpclient\Client;
use Yii\httpclient\Exception;

/**
 * Class SmartLabController
 * @package console\controllers
 */
class SmartLabController extends Controller
{
    /**
     * php yii smart-lab/dividend GMKN
     * https://smart-lab.ru/q/GMKN/dividend/
     */
    public function actionDividend(string $ticker = 'GMKN', string $tickerp = null): int
    {
        $response = (new Client())->get('https://smart-lab.ru/q/' . $ticker . '/dividend/')->send();

        $document = new \DOMDocument('1.0', 'utf-8');

        libxml_use_internal_errors(true);
        $document->loadHTML($response->getContent());
        libxml_use_internal_errors(false);

        $ticker = $tickerp ?: $ticker;

        $stock = StockEntity::findOne(['ticker' => $ticker]);
        $tables = $document->getElementsByTagName('tr');

        /** @var \DOMElement $item */
        foreach ($tables as $item) {
            if ($item->childElementCount < 8) {
                continue;
            }

            if ($item->childNodes[1]->nodeValue !== $ticker) {
                continue;
            }

            if ($item->childNodes[12]->nodeValue === '') {
                continue;
            }

            if (strlen($item->childNodes[5]->nodeValue) < 10) {
                continue;
            }

            $date = new DateTime(trim($item->childNodes[5]->nodeValue));
            $dateT2 = new DateTime(trim($item->childNodes[3]->nodeValue));

            $dividend = Dividend::findOne([
                'stock_id' => $stock->id,
                'date' => $date->format('Y-m-d'),
            ]);

            if ($dividend === null) {
                $dividend = new Dividend();
                $dividend->stock_id = $stock->id;
            }

            $data = [
                'date' => $date->format('Y-m-d'),
                'date_t2' => $dateT2->format('Y-m-d'),
                'year' => trim($item->childNodes[7]->nodeValue),
                'period' => trim($item->childNodes[9]->nodeValue),
                'dividend' => strtr(trim($item->childNodes[10]->nodeValue), [',' => '.']),
                'stock_price' => strtr(trim($item->childNodes[12]->nodeValue), [',' => '.']),
            ];

            $dividend->load($data, '');

            if (!$dividend->save()) {
                var_dump($dividend->getErrors()); die;
            }

            $this->stdout(implode(', ', $data) . PHP_EOL);
        }

        return ExitCode::OK;
    }

    /**
     * php yii smart-lab/portfolio 8 20831 DmitriyPushkin
     *
     * @param string $portfolio_id
     * @param string $sl_id
     * @param string $sl_user
     *
     * @return int
     *
     * @throws ValidationModelException
     * @throws Exception
     */
    public function actionPortfolio(string $portfolio_id, string $sl_id, string $sl_user): int
    {
        $url = 'https://smart-lab.ru/q/portfolio/' . $sl_user . '/' . $sl_id . '/more/order_by_date/asc/';
        $response = (new Client())->get($url)->send();

        $document = new \DOMDocument('1.0', 'utf-8');

        libxml_use_internal_errors(true);
        $document->loadHTML($response->getContent());
        libxml_use_internal_errors(false);

        $portfolio = Portfolio::findOne($portfolio_id);

        if ($portfolio === null) {
            $this->stdout('- '. $portfolio_id . PHP_EOL, BaseConsole::FG_RED);

            return ExitCode::DATAERR;
        }

        $items = $document->getElementsByTagName('tr');

        /** @var \DOMElement $item */
        foreach ($items as $item) {
            if ($item->childElementCount < 11) {
                continue;
            }

            if ($item->childNodes[1]->nodeValue === '№') {
                continue;
            }

            $ticker = trim($item->childNodes[7]->nodeValue);

            if (strpos($ticker, '-')) {
                $ticker = explode('-', $ticker)[0];
            }

            $price = trim($item->childNodes[19]->nodeValue);
            $price = strtr($price, [' ' => '', '$' => '']);
            $quantity = trim($item->childNodes[17]->nodeValue);

            $date = explode(' ', trim($item->childNodes[3]->nodeValue))[0];

            $stock = StockEntity::findOne(['ticker' => $ticker]);

            if ($stock === null) {
                $this->stdout('- '. $ticker . PHP_EOL, BaseConsole::FG_RED);
                continue;
            }

            $pStock = PortfolioStock::findOne([
                'portfolio_id' => $portfolio->id,
                'stock_id' => $stock->id,
                'date' => $date,
                'price' => $price,
                'quantity' => $quantity,
            ]);

            if ($pStock) {
                $this->stdout('-+ '. $ticker . PHP_EOL, BaseConsole::FG_GREEN);
                continue;
            }

            $pStock = new PortfolioStock();

            $pStock->portfolio_id = $portfolio->id;
            $pStock->stock_id = $stock->id;
            $pStock->date = $date;
            $pStock->price = $price;
            $pStock->quantity = $quantity;

            if (!$pStock->save()) {
                throw new ValidationModelException($pStock);
            }

            $this->stdout('+ '. $ticker . ' ' . $pStock->quantity . ' ' . $pStock->price . PHP_EOL);
        }

        return ExitCode::OK;
    }

    /**
     * php yii smart-lab/moex 9 2020-03-16
     *
     * @return int
     *
     * @throws ValidationModelException
     * @throws Exception
     */
    public function actionMoex(string $portfolio_id, string $date, int $sum = 100000): int
    {
        $url = 'https://smart-lab.ru/q/index_stocks/IMOEX/';
        $response = (new Client())->get($url)->send();

        $document = new \DOMDocument('1.0', 'utf-8');

        libxml_use_internal_errors(true);
        $document->loadHTML($response->getContent());
        libxml_use_internal_errors(false);

        $portfolio = Portfolio::findOne($portfolio_id);

        $items = $document->getElementsByTagName('tr');

        /** @var \DOMElement $item */
        foreach ($items as $item) {
            if ($item->childElementCount < 11) {
                continue;
            }

            if ($item->childNodes[1]->nodeValue === '№') {
                continue;
            }

            /** @var \DOMElement $node */
            foreach ($item->childNodes[5]->childNodes as $node) {
                $ticker = explode('/', $node->getAttribute('href'))[2];
            }

//            $percent = trim($item->childNodes[7]->nodeValue);

            $stock = StockEntity::findOne(['ticker' => $ticker]);

            if ($stock === null) {
                $this->stdout('- '. $ticker . PHP_EOL, BaseConsole::FG_RED);
                continue;
            }

            $stockHistory = StockHistory::find()
                ->andWhere(['stock_id' => $stock->id])
                ->andWhere(['>=', 'date', $date])
                ->orderBy(['date' => SORT_ASC])
                ->one();

            if ($stockHistory === null) {
                $this->stdout('-- '. $ticker . PHP_EOL, BaseConsole::FG_RED);
                continue;
            }

            $attribute = [
                'portfolio_id' => $portfolio->id,
                'stock_id' => $stock->id,
                'date' => $stockHistory->date,
                'price' => $stockHistory->price_close,
                'quantity' => ceil($sum / $stockHistory->price_close),
            ];

            $pStock = PortfolioStock::findOne($attribute);

            if ($pStock) {
                $this->stdout('-+ '. $ticker . PHP_EOL, BaseConsole::FG_GREEN);
                continue;
            }

            $pStock = new PortfolioStock();

            $pStock->load($attribute, '');

            if (!$pStock->save()) {
                throw new ValidationModelException($pStock);
            }

            $this->stdout('+ '. $ticker . ' ' . $pStock->quantity . ' ' . $pStock->price . PHP_EOL);
        }

        return ExitCode::OK;
    }
}
