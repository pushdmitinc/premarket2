<?php

namespace console\controllers;

use common\components\clients\MoexClient;
use common\helpers\ArrayHelper;
use common\helpers\MoexHelper;
use common\helpers\StockHelper;
use common\models\stock\StockHistory;
use common\models\stock\StockEntity;
use yii\helpers\Console;

class MoexController extends BaseController
{
    /**
     * php yii moex/securities TQPI
     *
     * @return mixed
     */
    public function actionSecurities(string $board = 'TQBR'): int
    {
        $client = new MoexClient();

        $data = $client->securities($board);

        foreach ($data as $item) {
            MoexHelper::loadMoex($item);

            $this->stdout('Добавлен тикер - ' . $item[0] . ' ' . $item[3] . PHP_EOL);
        }

        return 1;
    }

    /**
     * Загрузить историю по акциям
     * php yii moex/histories
     *
     * @return int
     */
    public function actionHistories(): int
    {
        $count = 0;

        /** @var StockEntity[] $securities */
        $query = StockEntity::find()
            ->where(['>=', 'start', 0])
            ->andWhere(['is_upload' => 0]);

        $countTotal = (clone $query)->count();

        foreach ($query->batch() as $securities) {
            foreach ($securities as $stock) {
                $name = $stock->ticker . ' (' . $stock->name . ')';
                $this->stdout('Начало загрузки ' . $name . PHP_EOL, Console::FG_BLUE);
                $this->actionHistory($stock->ticker);
                $this->stdout('Загружены данные по ' . $name . ' ' . ++$count . '/' . $countTotal . PHP_EOL, Console::FG_YELLOW);

                sleep(1);

                $stock->refresh();
                $stock->is_upload = 1;
                $stock->save();
            }
        }

        StockEntity::updateAll(['is_upload' => 0]);

        $count = StockEntity::updateAll(['percent_ytd_max' => 0], ['percent_ytd_max' => -100]);

        $this->stdout('Обновлены = -100, количество '  . ++$count . PHP_EOL, Console::FG_RED);

        return 1;
    }

    /**
     * Загрузить историю по акции
     * php yii moex/history NMTP
     *
     * @param string $ticker
     *
     * @return int
     */
    public function actionHistory(string $ticker = 'AFLT'): int
    {
        $client = new MoexClient();
        $stock = StockEntity::findOne(['ticker' => $ticker]);

        $start = $stock->start;
        $datePrev = null;
        $pricePrev = null;

        while (true) {
            $data = $client->history($ticker, start: $start);

            foreach ($data as $item) {
                $datePrev = $item[1];
                $pricePrev = $item[11];

                if ($pricePrev === null || $item[4] < 1) {
                    continue;
                }

                MoexHelper::historyLoad($stock, $item);

                $this->stdout('Тикер ' . $ticker . ' дата - ' . $item[1] . ', цена ' . $item[11] . PHP_EOL);
            }

            $this->stdout('Количество ' . count($data) . ' старт ' . $start . PHP_EOL, Console::FG_RED);

            sleep(1);

            if (count($data) < 100) {
                break;
            }

            $start += 100;
            $stock->start = $start;
            $stock->save();
        }

        if ($stock->date_prev <= $datePrev) {
            $stock->date_prev = $datePrev;
            $stock->price_prev = $pricePrev;
        }

        $stock->save();

        $this->actionChange($ticker);

        return 1;
    }

    /**
     * php yii moex/marketdata
     *
     * @return int
     */
    public function actionMarketdata(): int
    {
        $client = new MoexClient();

        $count = 0;
        $date = date('Y-m-d');
        $data = $client->marketData();

        foreach ($data as $item) {
            $ticker = $item[0];
            $price = $item[12];

            if ($price === null) {
                $this->stderr('Тикер ' . $ticker . ' цена = null' . PHP_EOL, Console::FG_RED);
                continue;
            }

            $stock = StockEntity::findOne(['ticker' => $ticker]);

            if (!$stock instanceof StockEntity) {
                $this->stderr('Тикер ' . $ticker . ' не найден' . PHP_EOL, Console::FG_RED);
                continue;
            }

            MoexHelper::marketDataLoad($stock, $item, $date);

            $this->stdout('Тикер ' . $ticker . ' цена ' . $price . ' количество ' . ++$count . PHP_EOL);

            $stock->date_prev = $date;
            $stock->price_prev = $price;
            $stock->save();

            $this->actionChange($ticker);
        }

        $this->stdout('Количество ' . count($data) . PHP_EOL, Console::FG_RED);

        return 1;
    }

    /**
     * php yii moex/change
     *
     * @param string $ticker
     *
     * @return int
     */
    public function actionChange(string $ticker = 'NMTP'): int
    {

        $stock = StockEntity::findOne(['ticker' => $ticker]);

        StockHelper::changeParams($stock);

        $this->stdout('Изм, max % ' . $stock->percent_ytd_max . ', min % ' . $stock->percent_ytd_min . PHP_EOL);

        $this->stdout('Изм ytd % ' . $stock->percent_ytd);
        $this->stdout(', год % ' . $stock->percent_year);
        $this->stdout(', месяц % ' . $stock->percent_month);
        $this->stdout(', неделя % ' . $stock->percent_week);
        $this->stdout(', день % ' . $stock->percent_day . PHP_EOL);

        return 1;
    }

    /**
     * php yii moex/analysis
     *
     * @param string $ticker
     *
     * @return int
     */
    public function actionAnalysis(string $ticker = 'AFLT'): int
    {
        $count = 0;
        $maxPrice = 0;
        $minPrice = 0;
        $maxPriceCurrent = 0;

        $percentSum = 0;
        $moneySum = 0;
        $countBay = 0;

//        $volumesLast = [];
        $payedPrices = [];

        $stopLoss = 0;
        $takeProfit = 0;

        $stock = StockEntity::findOne(['ticker' => $ticker]);

        $query = StockHistory::find()
            ->where(['ticker' => $ticker])
            ->orderBy(['date' => SORT_ASC]);

        foreach ($query->batch() as $batch) {
            /** @var StockHistory $model */
            foreach ($batch as $model) {
                if ($maxPrice < $model->price_close) {
                    $maxPrice = $model->price_close;
                }

                if ($minPrice === 0 || $minPrice > $model->price_close) {
                    $minPrice = $model->price_close;
                }

                if ($countBay === 0 && $maxPriceCurrent < $model->price_close) {
                    $maxPriceCurrent = $model->price_close;
                }

                if ($maxPriceCurrent * 0.7 > $model->price_close) {
                    $maxPriceCurrent = $model->price_close;
                    $payedPrices[] = $model->price_close;

                    $priceAvg = round(ArrayHelper::avg($payedPrices), $stock->decimals);

                    $string = 'Bay ' . $model->price_close;
                    $string .= ', maxPriceCurrent ' . $maxPriceCurrent;
                    $string .= ', priceAvg ' . $priceAvg;
                    $string .= ', countBay ' . ++$countBay;
                    $string .= ', date ' . $model->date;

                    $takeProfit = $priceAvg * 1.3;

                    $this->stdout($string, Console::BG_GREEN);
                    $this->stdout(PHP_EOL);
                }

                if ($stopLoss > 0 || $takeProfit > 0) {
                    $priceAvg = round(ArrayHelper::avg($payedPrices), $stock->decimals);
                    $stopPrice = $stopLoss > 0 ? $stopLoss : $takeProfit;
                    $percent = round($stopPrice / ($priceAvg / 100) - 100, 2);
                    $money = 10000 * $countBay * ($percent / 100);

                    if ($stopLoss > $model->price_low || ($takeProfit > 0 && $takeProfit < $model->price_high)) {
                        $string = 'Sell ' . $stopLoss;
                        $string .= ', price ' . $stopPrice;
                        $string .= ', percent ' . $percent;
                        $string .= ', money ' . $money;
                        $string .= ', date ' . $model->date;

                        $this->stdout($string, Console::BG_RED);
                        $this->stdout(PHP_EOL);

                        $stopLoss = 0;
                        $takeProfit = 0;
                        $countBay = 0;
                        $payedPrices = [];
                        $percentSum += $percent;
                        $moneySum += $money;
                    }

                    if ($stopLoss > 0 && $stopLoss * 1.11 < $model->price_high) {
                        $percentAdd = ceil($model->price_high / ($stopLoss / 100)) - 110;
                        $stopLoss = round($stopLoss * ($percentAdd / 100 + 1), $stock->decimals);

                        $string = 'StopLoss ' . $percentAdd . '% ' . $stopLoss;
                        $string .= ', price ' . $model->price_high;
                        $string .= ', percent ' . $percent;
                        $string .= ', date ' . $model->date;

                        $this->stdout($string . PHP_EOL);
                    }
                }

                if (!$stopLoss && $takeProfit === 0 && count($payedPrices)) {
                    $payedPrice = ArrayHelper::avg($payedPrices);

                    if ($payedPrice * 1.2 < $model->price_high) {
                        $stopLoss = round($payedPrice * 1.1, $stock->decimals);

                        $string = 'StopLoss 10% ' . $stopLoss;
                        $string .= ', price ' . $model->price_high;
                        $string .= ', date ' . $model->date;

                        $this->stdout($string . PHP_EOL);
                    }
                }

//                if (count($volumesLast) > 10) {
//                    if ((ArrayHelper::avg($volumesLast) * 10) < $model->volume) {
//                        $this->stdout('Volume ' . $model->volume . ', date ' . $model->date . PHP_EOL);
//                    }
//                }

                $volumesLast[$count % 30] = $model->volume;

                ++$count;
            }
        }

        $this->stdout('MaxPrice - ' . $maxPrice . PHP_EOL);
        $this->stdout('MinPrice - ' . $minPrice . PHP_EOL);
        $this->stdout('PercentSum - ' . $percentSum . PHP_EOL);
        $this->stdout('MoneySum - ' . $moneySum . PHP_EOL);

        return 1;
    }
}
