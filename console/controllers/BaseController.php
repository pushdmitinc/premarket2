<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;

class BaseController extends Controller
{
    /**
     * {@inheritdoc }
     */
    public function afterAction($action, $result)
    {
        $this->stdoutTime();

        return parent::afterAction($action, $result);
    }

    /**
     * Выводит время выполнения команды
     *
     * @return void
     */
    private function stdoutTime(): void
    {
        $time = round(Yii::getLogger()->getElapsedTime(), 2);

        $this->stdout('Time - ' . Yii::$app->formatter->asDuration($time) . ' sec.' . PHP_EOL);
    }
}
