<?php

declare(strict_types=1);

namespace common\components\vk;

use common\components\clients\BaseClient;

/**
 * @package common\components\vk
 */
class Vk extends BaseClient
{
    /**
     * @var ?string
     */
    public ?string $appId;
    /**
     * @var ?string
     */
    public ?string $appSecret;

    /**
     * @var ?string Токен доступа
     */
    public ?string $token;

    /**
     * @var string
     */
    public string $version = '5.199';

    /**
     * @var string
     */
    public string $baseUrl = 'https://api.vk.ru/method/';

    public function getHeaders(): array
    {
        return [
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->token,
            'Content-Type' => 'application/json',
        ];
    }

    /**
     * @param string $method
     * @param int $userId
     * @param string $fields
     *
     * @return array
     *
     * @throws \yii\httpclient\Exception
     */
    public function api(string $method, int $userId, string $fields = 'nickname,sex'): array
    {
        $url = '/' . $method;
        $data = [
            'user_id' => $userId,
            'fields' => $fields,
            'v' => $this->version,
        ];

        $response = $this->client()->get($url, $data, $this->getHeaders())->send();

        return $response->getData();
    }
}

