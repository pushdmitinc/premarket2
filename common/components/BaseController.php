<?php

namespace common\components;

use common\helpers\SettingsHelper;
use yii\web\Controller;

class BaseController extends Controller
{
    public $title;

    public $keywords;

    public $description;

    public $ogImage;

    public function beforeAction($action)
    {
        $this->setMetaTags();

        return parent::beforeAction($action);
    }

    protected function setMetaTags()
    {
        $this->view->title = $this->title ?: SettingsHelper::getTitle();
        $this->view->registerMetaTag([
            'name' => 'keywords',
            'content' => $this->keywords ?: SettingsHelper::getKeywords(),
            ], 'keywords');
        $this->view->registerMetaTag([
            'name' => 'description',
            'content' => $this->description ?: SettingsHelper::getDescription(),
        ], 'description');
        $this->view->registerMetaTag([
            'property' => 'og:image',
            'content' => $this->ogImage ?: SettingsHelper::getOgImage(),
        ], 'og:image');
    }
}

