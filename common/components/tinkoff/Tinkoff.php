<?php

declare(strict_types=1);

namespace common\components\tinkoff;

use common\components\clients\BaseClient;
use common\helpers\TinkoffHelper;
use yii\httpclient\Response;
use yii\base\InvalidConfigException;
use yii\httpclient\Exception;

/**
 * Class Tinkoff
 * @package common\components\tinkoff
 */
class Tinkoff extends BaseClient
{
    public const METHOD = 'POST';

    /**
     * @var string Токен доступа к Tinkoff Invest API 2
     *
     * @see https://tinkoff.github.io/investAPI/token/
     */
    public $apiToken;

    /**
     * @var string|null Значение AppName для запросов к Tinkoff Invest API 2
     *
     * @see https://tinkoff.github.io/investAPI/grpc/#appname
     */
    public $appName = null;

    /**
     * @var string
     */
    public string $baseUrl = 'https://invest-public-api.tinkoff.ru/rest';

    public function getHeaders(): array
    {
        return [
            'accept' => 'application/json',
            'Authorization' => 'Bearer ' . $this->apiToken,
            'Content-Type' => 'application/json',
            'x-app-name' => $this->appName,
        ];
    }

    public function shares(): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/Shares';

        $response = $this->send($url, ['instrumentStatus' => 2], 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    /**
     * @param string $id
     * @param string $code
     * @param string $type
     *
     * @return array
     *
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function shareBy(string $id, string $code = 'TQCB', string $type = 'INSTRUMENT_ID_TYPE_TICKER'): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/ShareBy';

        $data = ['id' => $id, 'idType' => $type, 'classCode' => $code];

        $response = $this->send($url, $data, 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    /**
     * @param string $id
     * @param string $code
     * @param string $type
     *
     * @return array
     *
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function bondBy(string $id, string $code = 'TQCB', string $type = 'INSTRUMENT_ID_TYPE_TICKER'): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/BondBy';

        $data = ['id' => $id, 'idType' => $type, 'classCode' => $code];

        $response = $this->send($url, $data, 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    /**
     * @param string $id
     * @param string $code
     * @param string $type
     *
     * @return array
     *
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function futureBy(string $id, string $code = 'SPBFUT', string $type = 'INSTRUMENT_ID_TYPE_TICKER'): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/FutureBy';

        $data = ['id' => $id, 'idType' => $type, 'classCode' => $code];

        $response = $this->send($url, $data, 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    public function checkResponse(Response $response): void
    {
        $remaining = (int) $response->getHeaders()->get('x-ratelimit-remaining');
        $reset = (int) $response->getHeaders()->get('x-ratelimit-reset');

        if ($remaining < 2 || $response->getStatusCode() === '429') {
            sleep($reset);
        }
    }

    public function currencies(): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/Currencies';

        $response = $this->send($url, ['instrumentStatus' => 2], 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    public function etfs(): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/Etfs';

        $response = $this->send($url, ['instrumentStatus' => 2], 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    public function etfBy(string $id, string $code = 'TQTF', string $type = 'INSTRUMENT_ID_TYPE_TICKER'): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/EtfBy';

        $data = ['id' => $id, 'idType' => $type, 'classCode' => $code];

        $response = $this->send($url, $data, 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    public function dividends(string $figi, string $dateFrom = null, string $dateTo = null): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/GetDividends';

        $data = [
            'figi' => $figi,
            'from' => ($dateFrom ?: '2000-01-01') . 'T00:00:00Z',
            'to' => ($dateTo ?: date('Y-m-d')) . 'T00:00:00Z',
        ];

        $response = $this->send($url, $data, 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    public function bondEvents(string $figi, string $dateFrom = null, string $dateTo = null): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.InstrumentsService/GetBondEvents';

        $data = [
            'instrumentId' => $figi,
            'from' => ($dateFrom ?: '2000-01-01') . 'T00:00:00Z',
            'to' => ($dateTo ?: date('Y-m-d')) . 'T23:59:59Z',
            'type' => 'EVENT_TYPE_CPN',
        ];

        $response = $this->send($url, $data, 'POST');
        $this->checkResponse($response);

        return $response->getData();
    }

    /**
     * @param string $figi
     * @param int $year
     * @param string|null $period
     *
     * @return array
     *
     * @throws Exception
     * @throws InvalidConfigException
     */
    public function history(string $figi, int $year, ?string $interval = null): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.MarketDataService/GetCandles';

        [$dateFrom, $dateTo] = TinkoffHelper::dateFromTo($year, $interval);

        $data = [
            'figi' => $figi,
            'from' => $dateFrom,
            'to' => $dateTo,
            'interval' => 5,
        ];

        $response = $this->send($url, $data, 'POST');
        $this->checkResponse($response);

        $data = $response->getData();

        if (($data['candles'] ?? null) === null) {
            var_dump($data);
        }

        return $data['candles'] ?? [];
    }

    public function marketData(array $figi): array
    {
        $url = '/tinkoff.public.invest.api.contract.v1.MarketDataService/GetLastPrices';

        $response = $this->send($url, ['figi' => $figi], 'POST');

        $remaining = (int) $response->getHeaders()->get('x-ratelimit-remaining');
        $reset = (int) $response->getHeaders()->get('x-ratelimit-reset');

        if ($remaining < 3 || $response->getStatusCode() === '429') {
            sleep($reset);
        }

        $data = $response->getData();

        if (($data['lastPrices'] ?? null) === null) {
            var_dump($figi, $remaining, $reset, $response); die;
        }

        return $data['lastPrices'];
    }
}

