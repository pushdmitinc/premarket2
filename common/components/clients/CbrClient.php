<?php

declare(strict_types=1);

namespace common\components\clients;

use yii\httpclient\Client;
use yii\httpclient\Response;

class CbrClient extends BaseClient
{
    /**
     * {@inheritdoc}
     */
    public string $baseUrl = 'http://www.cbr.ru/';

    /**
     * Получение курса валют
     *
     * @return array
     * @throws ClientException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    #[\JetBrains\PhpStorm\ArrayShape(['@attributes' => ['Date' => ""], 'Valute' => "array[]"])]
    public function currency(): array
    {
        $response = $this->send('/scripts/XML_daily.asp');

        $response->setFormat(Client::FORMAT_XML);
        $data = $response->getData();

        if (!$this->validateResponseCurrency($data)) {
            throw new ClientException('Не валидные данные АПИ');
        }

        return $data;
    }

    /**
     * Проверяем данные от АПИ
     *
     * @param array $data
     *
     * @return bool
     */
    protected function validateResponseCurrency(array $data): bool
    {
        if (!isset($data['@attributes']) || !$data['@attributes']['Date']) {
            return false;
        }

        if (!isset($data['Valute'])) {
            return false;
        }

        return true;
    }
}
