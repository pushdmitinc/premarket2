<?php

declare(strict_types=1);

namespace common\components\clients;

use Exception;

/**
 * Class ClientException
 * @package common\components\clients
 */
class ClientException extends Exception
{
}
