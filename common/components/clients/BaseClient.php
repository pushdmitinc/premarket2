<?php

declare(strict_types=1);

namespace common\components\clients;

use yii\base\Component;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\httpclient\Response;

/**
 * Базовый класс для клиентов АПИ
 *
 * Class BaseClient
 * @package common\components\clients
 */
abstract class BaseClient extends Component
{
    public const METHOD = 'GET';

    /**
     * @var string base request URL.
     */
    public string $baseUrl = '';

    /**
     * @var Client|null
     */
    private ?Client $client = null;

    /**
     * Возвращает клиент АПИ
     *
     * @return Client
     */
    public function client(): Client
    {
        if ($this->client === null) {
            $this->client = new Client(['baseUrl' => $this->baseUrl]);
        }

        return $this->client;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return [];
    }

    /**
     * Выполняем запрос к АПИ
     *
     * @param string $url
     *
     * @return Response
     *
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function send(string $url, array $data = [], string $method = self::METHOD): Response
    {
        $request = $this->client()->createRequest();

        if ($this->getHeaders()) {
            $request->setHeaders($this->getHeaders());
        }

        if ($data) {
            if ($method === 'POST') {
                $request->setContent(Json::encode($data));
            } else {
                $request->setData(Json::encode($data));
            }
        }

        return $request->setUrl($url)
            ->setMethod($method)
            ->send();
    }
}

