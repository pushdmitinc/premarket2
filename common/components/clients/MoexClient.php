<?php

namespace common\components\clients;

class MoexClient extends BaseClient
{
    public string $baseUrl = 'http://iss.moex.com/iss';

    public function securities($board = 'TQBR', $market = 'shares', $engine = 'stock'): array
    {
        $url = '/engines/' . $engine;
        $url .= '/markets/' . $market;
        $url .= '/boards/' . $board;
        $url .= '/securities.json';

        $data = $this->send($url)->getData();

        return $data['securities']['data'];
    }

    public function history($ticker, $board = null, $market = 'shares', $engine = 'stock', $start = 0): array
    {
        $url = '/history/engines/' . $engine;
        $url .= '/markets/' . $market;
        if ($board) $url .= '/boards/' . $board;
        $url .= '/securities/' . $ticker . '.json';
        $url .= '?start=' . $start;

        $data = $this->send($url)->getData();

        return $data['history']['data'];
    }

    public function marketData($market = 'shares', $engine = 'stock'): array
    {
        $url = '/engines/' . $engine;
        $url .= '/markets/' . $market;
        $url .= '/securities.json';

        $data = $this->send($url)->getData();

        return $data['marketdata']['data'];
    }
}

