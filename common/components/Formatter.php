<?php

namespace common\components;

use NumberFormatter;
use yii\base\InvalidConfigException;

/**
 * Class Formatter
 * @package common\components
 */
class Formatter extends \yii\i18n\Formatter
{
    /**
     * @param float $number
     * @param string|null $currency
     *
     * @return string
     *
     * @throws InvalidConfigException
     */
    public function stockCurrency(float $number, string $currency = null): string
    {
        $precision = 2;

        if ($number < 1) {
            $precision = 3;
        }

        if ($number < 0.1) {
            $precision = 4;
        }

        return $this->asCurrency($number, $currency, [NumberFormatter::FRACTION_DIGITS => $precision]);
    }
}
