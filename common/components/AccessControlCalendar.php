<?php

namespace common\components;

use common\models\Calendar;
use common\models\CalendarEvent;
use yii\filters\AccessRule;

class AccessControlCalendar extends AccessControl
{
	/** @var Calendar */
	public $calendar;

	/** @var CalendarEvent */
	public $calendarEvent;

	public function beforeAction($action)
	{
	    if ($this->calendarEvent instanceof CalendarEvent) {
            $this->calendar = $this->calendarEvent->calendar;
        }

		if($this->calendar instanceof Calendar){
			/* @var $rule AccessRule */
			foreach ($this->rules as $rule) {
				if(in_array($action->id, $rule->actions) || $rule->roles){
					foreach($rule->roles as $role){
						if(is_integer($role)){
							if($this->calendar->canUserSing($role)){
								return true;
							}
						}
					}
				}
			}
		}

		return parent::beforeAction($action);
	}
}
