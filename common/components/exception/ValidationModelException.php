<?php

namespace common\components\exception;

use Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\web\ServerErrorHttpException;

/**
 * Class ValidationModelException
 * @package app\exceptions
 */
class ValidationModelException extends ServerErrorHttpException
{
    /**
     * ValidationModelException constructor.
     *
     * @param Model $model Проблемная модель
     * @param string|null $message Сообщение об ошибке
     *
     * @throws Exception
     */
    public function __construct(Model $model, $message = null)
    {
        $delimiter = ' | ';

        if (!empty($message)) {
            $message .= $delimiter;
        }

        $message .= 'Error in ' . get_class($model) . $delimiter;

        foreach ($model->getFirstErrors() as $field => $error) {
            if (($pos = strpos($field, '[')) !== false) {
                $field_modified = substr($field, 0, $pos);
                $value = ArrayHelper::getValue($model, $field_modified);
            } else {
                $value = $model->$field;
            }

            $message .= "err:  $error ($field = " . VarDumper::export($value) . ") $delimiter";
        }

        parent::__construct($message);
    }
}
