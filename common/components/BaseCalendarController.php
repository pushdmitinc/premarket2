<?php

namespace common\components;

use common\models\Calendar;
use common\models\CalendarEvent;
use Yii;
use yii\web\NotFoundHttpException;

class BaseCalendarController extends BaseController
{
    /** @var Calendar */
    public $calendar;

    /** @var CalendarEvent */
    public $calendarEvent;

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if (Yii::$app->request->get('calendar_id')) {
            if (!$this->calendar instanceof Calendar) {
                $this->calendar = $this->findCalendarModel(Yii::$app->request->get('calendar_id'));
            }
        }

        return parent::beforeAction($action);
    }

    protected function findCalendarModel(int $id): Calendar
    {
        if ($this->calendar instanceof Calendar) {
            if ($this->calendar->id === $id) {
                return $this->calendar;
            }
        }

        $this->calendar = Calendar::findOne($id);

        if ($this->calendar !== null) {
            return $this->calendar;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findEventModel(int $id): CalendarEvent
    {
        if ($this->calendarEvent instanceof CalendarEvent) {
            if ($this->calendarEvent->id === $id) {
                return $this->calendarEvent;
            }
        }

        $this->calendarEvent = CalendarEvent::findOne($id);

        if ($this->calendarEvent !== null) {
            return $this->calendarEvent;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

