<?php

namespace common\components;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ActiveModel
 * @package common\components
 *
 * @property int $id
 * @property int $created_at
 * @property int $updated_at
 */
class ActiveModel extends ActiveRecord
{
    /**
     * Используем свой построитель запросов
     *
     * @return ActiveQuery
     */
    public static function find(): ActiveQuery
    {
        return new ActiveQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array
    {
        return [
            'timestampBehavior' => [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => $this->hasAttribute('created_at') ? 'created_at' : null,
                'updatedAtAttribute' => $this->hasAttribute('updated_at') ? 'updated_at' : null,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public static function getMapList(string $name = 'name'): array
    {
        return self::find()
            ->select([$name])
            ->indexBy('id')
            ->column();
    }

    public function formId(): string
    {
        $formId = self::tableName();

        if (!$this->getIsNewRecord()) {
            $formId .= '_' . $this->primaryKey;
        }

        return $formId;
    }

    public function cloneModel(array $attributes = []): self
    {
        $model = new static();

        $model->setAttributes(array_merge($this->attributes, $attributes));

        return $model;
    }
}
