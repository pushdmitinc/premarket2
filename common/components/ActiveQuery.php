<?php

namespace common\components;

use yii\db\Expression;
use yii\db\QueryTrait;

class ActiveQuery extends \yii\db\ActiveQuery
{
    /**
     * Сортирует данные согласно заданому в масиве порядку
     *
     * @param array $ids
     * @return bool|ActiveQuery
     */
    public function orderByCase(array $ids): QueryTrait
    {
        if(!$ids) return false;

        $order = 'CASE';

        foreach($ids as $key => $item){
            $order .= ' WHEN id=\'' . $item . '\' THEN ' . $key;
        }

        $order .= ' END';

        $order = new Expression($order);

        return $this->orderBy($order);
    }
}
