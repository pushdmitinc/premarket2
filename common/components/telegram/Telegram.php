<?php

declare(strict_types=1);

namespace common\components\telegram;

use aki\telegram\base\Response;

/**
 * Class Telegram
 * @package common\components\telegram
 */
class Telegram extends \aki\telegram\Telegram
{
    public array $messages = [];

    public function getMe(): Response
    {
        $body = $this->send('/getMe', []);

        return new Response([
            'ok' => $body['ok'],
            'result' => [
                'user' => $body['result']
            ]
        ]);
    }

    public function sendChat(string $chatId, string $text): void
    {
        $this->sendMessage([
            'chat_id' => $chatId,
            'text' => $text,
        ]);
    }

    public function sendVkOnline(string $text): void
    {
        $this->sendChat('796199071', $text);
    }

    public function sendFutureStock(string $text): void
    {
        $this->sendChat('796199071', $text);
    }

    public function queueMessage(string $text): void
    {
        $this->messages[] = $text;
    }

    public function queueSend(): void
    {
        if ($this->messages === []) {
            return;
        }

        $text = implode("\n", $this->messages);

        $this->sendChat('796199071', $text);
    }
}

