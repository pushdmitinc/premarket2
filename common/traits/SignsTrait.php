<?php

namespace common\traits;

use yii\db\ActiveQuery;

trait SignsTrait
{
    public $inputSigns = [];

    public function canSign(int $index = 0): bool
    {
        return $this->signs >> $index & 0b1;
    }

    public function notCanSign(int $index = 0): bool
    {
        return !$this->canSign($index);
    }

    public function addSigns(array $signs, bool $isSave = false): int
    {
        foreach ($signs as $index => $value) {
            if ($value) {
                $this->addSign($index);
            } else {
                $this->removeSigns($index);
            }
        }

        if($isSave){
            $this->save();
        }

        return $this->signs;
    }

    public function addSign(int $index = 0, bool $isSave = false): int
    {
        $this->signs = $this->signs | (0b1 << $index);

        if($isSave){
            $this->save();
        }

        return $this->signs;
    }

    public function removeSign(int $index = 0, bool $isSave = false): int
    {
        $this->signs = $this->signs & ~(0b1 << $index);

        if($isSave){
            $this->save();
        }

        return $this->signs;
    }

    public static function signsLabels(): array
    {
        return [
            0 => 'Показывать на сайте',
        ];
    }

    public static function setWhere(ActiveQuery $query, array $signs)
    {
        foreach ($signs as $index => $sign) {
            $bit = 0b1 << $index;
            $query->andWhere('(signs::bit(8) & ' . $bit . '::bit(8))::int = ' . $sign);
        }
    }
}