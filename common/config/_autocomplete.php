<?php

class Yii
{
    /**
     * @var yii\web\Application|yii\console\Application|__Application
     */
    public static $app;
}

/**
 * @property yii\rbac\DbManager $authManager
 * @property yii\web\User|__WebUser $user
 * @property common\components\telegram\Telegram $telegram
 * @property common\components\tinkoff\Tinkoff $tinkoff
 * @property common\components\vk\Vk $vk
 */
class __Application
{
}

/**
 * @property common\models\User $identity
 */
class __WebUser
{
}