<?php
return [
    'name' => 'PreMarket',
    'language' => 'ru_RU',
    'sourceLanguage' => 'ru_RU',
    'timeZone' => 'Europe/Moscow',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'cache-db' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@common/runtime/cache',
        ],
        'formatter' => [
            'class' => 'common\components\Formatter',
            'thousandSeparator' => ' ',
            'decimalSeparator' => ','
        ],
        'telegram' => [
            'class' => 'common\components\telegram\Telegram',
            'botToken' => '',
        ],
        'tinkoff' => [
            'class' => 'common\components\tinkoff\Tinkoff',
            'apiToken' => '',
            'appName' => 'ru.premarket',
        ],
        'vk' => [
            'class' => 'common\components\vk\Vk',
            'token' => '',
        ],
    ],
];
