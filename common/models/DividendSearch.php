<?php

namespace common\models;

use common\models\stock\StockEntity;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Dividend;

/**
 * DividendSearch represents the model behind the search form of `common\models\Dividend`.
 */
class DividendSearch extends Dividend
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'stock_id', 'signs'], 'integer'],
            [['dividend', 'stock_price'], 'number'],
            [['ticker'], 'string'],
            [['period', 'date', 'date_t2'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params): ActiveDataProvider
    {
        $query = Dividend::find()
            ->alias('d')
            ->select(['d.*', 's.ticker']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->innerJoin(['s' => StockEntity::tableName()], 's.id = stock_id');
        $query->with(['stock']);

        $query->andFilterWhere([
            'd.id' => $this->id,
            'd.stock_id' => $this->stock_id,
            'd.period' => $this->period,
            'd.dividend' => $this->dividend,
            'd.stock_price' => $this->stock_price,
            'd.date' => $this->date,
            'd.date_t2' => $this->date_t2,
            'd.signs' => $this->signs,
            's.ticker' => $this->ticker,
        ]);

        return $dataProvider;
    }
}
