<?php

namespace common\models;

use common\models\stock\StockEntity;
use common\traits\SignsTrait;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "dividend".
 *
 * @property int $id
 * @property int $stock_id Акция
 * @property float $dividend Название
 * @property string $currency Валюта
 * @property string $period
 * @property float|null $stock_price
 * @property string|null $date
 * @property string|null $date_t2
 * @property int|null $signs Признаки
 *
 * @property StockEntity $stock
 * @see Dividend::getStock()
 */
class Dividend extends ActiveRecord
{
    use SignsTrait;

    public const SING_DELETE = 0;

    public ?string $ticker = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'dividend';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['stock_id', 'dividend', 'currency'], 'required'],
            [['stock_id', 'signs'], 'default', 'value' => null],
            [['stock_id', 'signs'], 'integer'],
            [['dividend', 'stock_price'], 'number'],
            [['date', 'date_t2'], 'safe'],
            [['period', 'currency'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'stock_id' => 'Stock ID',
            'dividend' => 'Dividend',
            'currency' => 'Валюта',
            'period' => 'Period',
            'stock_price' => 'Stock Price',
            'date' => 'Date',
            'date_t2' => 'Date T2',
            'signs' => 'Signs',
        ];
    }

    public function getStock(): ActiveQuery
    {
        return $this->hasOne(StockEntity::class, ['id' => 'stock_id']);
    }

    public function getPercent(): float
    {
        return round($this->dividend / $this->stock_price * 100, 2);
    }
}
