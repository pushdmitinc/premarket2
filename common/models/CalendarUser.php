<?php

namespace common\models;

use common\components\ActiveModel;
use common\models\interfaces\SignsInterface;
use common\traits\SignsTrait;

/**
 * This is the model class for table "calendar_user".
 *
 * @property int $id
 * @property int $calendar_id Календарь
 * @property int $user_id Пользователь
 * @property int|null $signs Признаки
 * @property string|null $options Дополнительные опции
 *
 * @property Calendar $calendar
 * @property User $user
 */
class CalendarUser extends ActiveModel implements SignsInterface
{
    use SignsTrait;

    const SING_VIEW = 1;

    const SING_UPDATE = 2;

    const SING_VIEW_LINK = 31;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'calendar_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['calendar_id', 'user_id'], 'required'],
            [['calendar_id', 'user_id', 'signs'], 'default', 'value' => null],
            [['calendar_id', 'user_id', 'signs'], 'integer'],
            [['options', 'inputSigns'], 'safe'],
            [['calendar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Calendar::class, 'targetAttribute' => ['calendar_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'calendar_id' => 'Календарь',
            'user_id' => 'Пользователь',
            'signs' => 'Signs',
            'options' => 'Дополнительные опции',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->inputSigns) {
            $this->setSigns($this->inputSigns);
        }

        return parent::beforeSave($insert);
    }

    /**
     * Gets query for [[Calendar]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCalendar()
    {
        return $this->hasOne(Calendar::class, ['id' => 'calendar_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function signsLabels(): array
    {
        return [
            self::SING_VIEW => 'Просмотр',
            self::SING_UPDATE => 'Управление',
        ];
    }
}
