<?php

namespace common\models\portfolio;

use Yii;

/**
 * This is the model class for table "portfolio_sector".
 *
 * @property int $id
 * @property int $portfolio_id Портфель
 * @property string $name Сектор
 * @property int|null $sort Сортировка
 */
class PortfolioSector extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portfolio_sector';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['portfolio_id', 'name'], 'required'],
            [['portfolio_id', 'sort'], 'default', 'value' => null],
            [['portfolio_id', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Portfolio ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }
}
