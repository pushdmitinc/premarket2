<?php

namespace common\models\portfolio;

use common\helpers\StockHelper;
use common\models\Portfolio;
use common\models\PortfolioStock;
use common\models\stock\Stock;
use common\models\stock\StockEntity;
use common\models\stock\StockHistory;
use DateTime;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use Yii;
use yii\base\Model;

/**
 * @property StockEntity $stock
 * @see Portfolio::getStock()
 */
class PortfolioStockCollection extends Model
{
    public const GROUP_TYPE_CURRENCY = 1;

    public const GROUP_TYPE_SECTOR = 2;

    public const GROUP_TYPE_TYPE = 3;

    public ?int $stock_id = null;

    /** @var PortfolioStock[] */
    public array $collections = [];

    /** @var PortfolioStock[] */
    public array $collectionsClose = [];

    public function fields(): array
    {
        return [
            'id',
            'name',
            'ticker',
            'quantity',
            'icon' => function () {
                return $this->getLogo();
            },
            'price_purchase' => function () {
                return $this->getPrice();
            },
            'price_stock' => function () {
                return $this->getPriceCurrent();
            },
            'price_total' => function () {
                return $this->totalPrice();
            },
            'price_current' => function () {
                return $this->totalPriceCurrent();
            },
            'profit' => function () {
                return $this->profit();
            },
            'percent' => function () {
                return $this->changePercent();
            },
            'currency' => function () {
                return $this->stock->currency;
            },
            'currency_symbol' => function () {
                return $this->stock->getCurrencySymbol();
            },
        ];
    }

    public function add(PortfolioStock $portfolioStock): self
    {
        if ($portfolioStock->canSign(PortfolioStock::SING_CLOSE)) {
            $this->collectionsClose[$portfolioStock->id] = $portfolioStock;
        } else {
            $this->collections[$portfolioStock->id] = $portfolioStock;
        }


        return $this;
    }

    /**
     * @return PortfolioStock[]
     */
    public function getFullCollection(): array
    {
        return $this->collections + $this->collectionsClose;
    }

    /**
     * @return StockEntity
     */
    public function getStock(): StockEntity
    {
        return $this->getPortfolioStock()->stock;
    }

    /**
     * @return PortfolioStock
     */
    public function getPortfolioStock(): PortfolioStock
    {
        return array_values($this->getFullCollection())[0];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->getStock()->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->getStock()->name;
    }

    /**
     * @return string
     */
    public function getTicker(): string
    {
        return $this->getStock()->ticker;
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        return $this->getStock()->getLogo();
    }

    /**
     * @return string
     */
    public function getNameShort(): string
    {
        return $this->getStock()->name;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->getPortfolioStock()->date;
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    public function getDateDiffDays(): string
    {
        $dateStart = new DateTime($this->getDate());
        $dateFinish = new DateTime();

        return $dateStart->diff($dateFinish)->days;
    }

    /**
     * Текущая цена акции
     *
     * @return float
     */
    public function getPriceCurrent(): float
    {
        if ($this->getStock()->price_prev) {
            return $this->getStock()->getPricePrev();
        }

        return 0.0;
    }

    /**
     * Текущая цена акции
     *
     * @return float
     */
    public function getPriceClose(): float
    {
        if ($this->getPortfolioStock()->price_close) {
            return StockHelper::round($this->totalPriceClose() / $this->getQuantity());
        }

        return $this->getPriceCurrent();
    }

    /**
     * Изменение за день
     *
     * @return float
     */
    public function getChangeDay(): float
    {
        return $this->getPriceCurrent() / (100 + $this->getPercentDay()) * $this->getPercentDay();
    }

    /**
     * Средняя цена покупки акции
     *
     * @return float
     */
    public function getPrice(): float
    {
        return StockHelper::round($this->totalPrice() / $this->getQuantity());
    }

    public function getQuantity(): int
    {
        return $this->totalQuantity();
    }

    public function totalQuantity(): int
    {
        $sum = 0;

        foreach ($this->getFullCollection() as $stock) {
            $sum += $stock->quantity;
        }

        return $sum;
    }

    /**
     * Валюта
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->getStock()->currency;
    }

    /**
     * Валюта
     *
     * @return string
     */
    public function getCurrencySymbol(): string
    {
        return $this->getStock()->getCurrencySymbol();
    }

    /**
     * @return Int
     */
    public function getType(): Int
    {
        return $this->getStock()->type ?: 0;
    }

    /**
     * @return String
     */
    public function getGroupType(int $type = self::GROUP_TYPE_CURRENCY): String
    {
        if ($type === self::GROUP_TYPE_SECTOR) {
            return $this->getSector();
        }

        if ($type === self::GROUP_TYPE_TYPE) {
            return (string) $this->getType();
        }

        return $this->getCurrency();
    }

    /**
     * Валюта
     *
     * @return string
     */
    public function getSector(): string
    {
        $sector = $this->getStock()->sector;

        if (!$sector) {
            if ($this->getType() == StockEntity::TYPE_CURRENCY) {
                return 'валюта и др. металлы';
            }

            if ($this->getType() == StockEntity::TYPE_ETF) {
                return 'ETF';
            }
        }

        return $sector ?: 'other';
    }

    /**
     * Валюта
     *
     * @return string
     */
    public function getSection(): string
    {
        return $this->getPortfolioStock()->sector->name ?: 'other';
    }

    /**
     * Процент отклонения за день
     *
     * @return float
     */
    public function getPercentDay(): float
    {
        if ($this->getStock()->date_prev !== date('Y-m-d')) {
            return .0;
        }

        return $this->getStock()->percent_day ?? .0;
    }

    /**
     * Процент отклонения от текущей цены
     *
     * @return float
     */
    public function getPercentCurrent(): float
    {
        if ($this->totalPrice() === .0) {
            return 0;
        }

        return round($this->totalPriceCurrent() / $this->totalPrice() * 100 - 100, 2);
    }

    /**
     * Процент отклонения от текущей цены с учетом стоимости валюты
     *
     * @return float
     */
    public function getPercentCurrencyCurrent(): float
    {
        if ($this->totalPrice() === .0 || !$this->getPortfolioStock()->currency_price) {
            return 0;
        }

        $priceStart = ($this->totalPrice() * $this->getPortfolioStock()->currency_price);
        $priceFinish = StockHelper::priceToCurrency($this->totalPriceCurrent(), $this->getCurrency());

        return round($priceFinish / $priceStart * 100 - 100, 2);
    }

    /**
     * Процент отклонения от текущей цены
     *
     * @return float
     */
    public function getPercentOfClosing(): float
    {
        return round($this->totalPriceCurrent() / $this->totalPriceClose() * 100 - 100, 2);
    }

    /**
     * Процент отклонения от текущей цены
     *
     * @return float
     */
    public function getPercentClose(): float
    {
        if ($this->totalPrice() === .0) {
            return 0;
        }

        return round($this->totalPriceClose() / $this->totalPrice() * 100 - 100, 2);
    }

    /**
     * Процент отклонения от текущей цены
     *
     * @return float
     */
    public function getCurrentClose(): float
    {
        return round($this->totalPriceClose() / $this->totalPrice() * 100 - 100, 2);
    }

    /**
     * Процент выплаченных дивидендов
     *
     * @return float
     */
    public function getDividendPercent(): float
    {
        $totalPrice = $this->totalPrice(StockHelper::getCurrencySystem());

        if ($totalPrice === .0) {
            return 0;
        }

        return round($this->dividend() / $totalPrice * 100, 2);
    }

    /**
     * Процент с учетом дивидендов
     *
     * @return float
     */
    public function getPercentTotal(): float
    {
        return $this->getDividendPercent() + $this->getPercentClose();
    }

    /**
     * Тип отклонения
     *
     * @return string
     */
    public function typeDeviation(): string
    {
        return $this->getPercentClose() > 0 ? 'up' : 'down';
    }

    /**
     * Тип отклонения
     *
     * @return string
     */
    public function typeDeviationCurrent(): string
    {
        return $this->getPercentCurrent() > 0 ? 'up' : 'down';
    }

    /**
     * Тип отклонения
     *
     * @return string
     */
    public function typeDeviationClose(): string
    {
        return $this->getPercentOfClosing() > 0 ? 'up' : 'down';
    }

    /**
     * Тип отклонения за день
     *
     * @return string
     */
    public function typeDeviationDay(): string
    {
        return $this->getPercentDay() > 0 ? 'up' : 'down';
    }

    /**
     * Тип отклонения по дивидендам
     *
     * @return string
     */
    public function typeDeviationDividend(): string
    {
        return $this->dividend() > 0 ? 'up' : 'left';
    }

    /**
     * Тип отклонения
     *
     * @return string
     */
    public function typeClass(): string
    {
        return $this->getPercentClose() > 0 ? 'green' : 'red';
    }

    /**
     * Тип отклонения
     *
     * @return string
     */
    public function typeClassCurrent(): string
    {
        return $this->getPercentCurrent() > 0 ? 'green' : 'red';
    }

    /**
     * Тип отклонения
     *
     * @return string
     */
    public function typeClassClose(): string
    {
        return $this->getPercentOfClosing() > 0 ? 'green' : 'red';
    }

    /**
     * Тип отклонения день
     *
     * @return string
     */
    public function typeClassDay(): string
    {
        return $this->getPercentDay() > 0 ? 'green' : 'red';
    }

    /**
     * Тип отклонения с дивидендами
     *
     * @return string
     */
    public function typePercentTotal(): string
    {
        if ($this->getPercentTotal() > 20) {
            return 'green';
        }

        if ($this->getPercentTotal() > 0) {
            return 'aqua';
        }

        if ($this->getPercentTotal() > -10) {
            return 'yellow';
        }

        return 'red';
    }

    /**
     * Тип отклонения по дивидендам
     *
     * @return string
     */
    public function typeClassDividend(): string
    {
        return $this->dividend() > 0 ? 'green' : 'yellow';
    }

    /**
     * Тип отклонения по тейк профит
     *
     * @return string
     */
    public function typeClassPurpose(): string
    {
        if ($this->percentToPurpose() <= 0) {
            return '';
        }

        if ($this->percentToPurpose() < 50) {
            return 'blue';
        }

        if ($this->percentToPurpose() < 80) {
            return 'blue active';
        }

        return 'green active';
    }

    /**
     * Тип отклонения по тейк профит
     *
     * @return string
     */
    public function typeClassStopLoss(): string
    {
        if ($this->percentToStopLoss() <= 0) {
            return '';
        }

        if ($this->percentToStopLoss() < 50) {
            return 'yellow';
        }

        if ($this->percentToStopLoss() < 80) {
            return 'yellow active';
        }

        return 'red active';
    }

    public function totalPrice(string $currency = null): float
    {
        $totalPrice = 0;

        foreach ($this->getFullCollection() as $stock) {
            $totalPrice += $stock->totalPrice($currency);
        }

        return $totalPrice;
    }

    /**
     * Текущая стоимость позиции
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function totalPriceCurrent(string $currency = null): float
    {
        $totalPrice = 0;

        foreach ($this->getFullCollection() as $stock) {
            $totalPrice += $stock->totalPriceCurrent($currency);
        }

        return $totalPrice;
    }

    /**
     * Текущая стоимость позиции
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function totalPriceClose(string $currency = null): float
    {
        $totalPrice = 0;

        foreach ($this->getFullCollection() as $stock) {
            $totalPrice += $stock->totalPriceClose($currency);
        }

        return $totalPrice;
    }

    /**
     * Текущая стоимость + дивиденды
     *
     * @return float
     */
    public function getCurrentPriceFull(): float
    {
        return $this->totalPriceClose() + $this->dividend();
    }

    public function profit(string $currency = null): float
    {
        return $this->totalPriceClose($currency) - $this->totalPrice($currency);
    }

    public function profitCurrent(): float
    {
        return $this->totalPriceCurrent() - $this->totalPrice();
    }

    public function getProfitTotal(): float
    {
        return $this->profit(StockHelper::CURRENCY_SYSTEM) + $this->dividend(StockHelper::CURRENCY_SYSTEM);
    }

    public function purpose(): float
    {
//        $value = 0;
//
//        foreach ($this->getPortfolioStock() as $stock) {
//            $value += $stock->purpose();
//        }

        return StockHelper::round($this->getPortfolioStock()->purpose());
    }

    public function purposePercent(): float
    {
        if ($this->getPrice() === .0) {
            return 0;
        }

        return round(($this->purpose() - $this->getPrice()) / $this->getPrice() * 100);
    }

    public function percentToPurpose(): float
    {
        $percent = $this->getPercentCurrent();

        if ($percent <= 0) {
            return 0.0;
        }

        return round($percent / $this->purposePercent() * 100, 2);
    }

    public function stopLoss(): float
    {
        return $this->getPortfolioStock()->stop_loss ?: 0.0;
    }

    public function stopLossPercent(): float
    {
        if ($this->stopLoss() === 0.0) {
            return 0.0;
        }

        return round(($this->getPrice() - $this->stopLoss()) / $this->getPrice() * 100);
    }

    public function percentToStopLoss(): float
    {
        if ($this->stopLoss() === 0.0) {
            return 0.0;
        }


        if ($this->getPriceCurrent() >= $this->getPrice()) {
            return 0.0;
        }

        $diffStop = $this->getPrice() - $this->stopLoss();
        $diffPrice = $this->getPrice() - $this->getPriceCurrent();

        if ($diffPrice >= $diffStop) {
            return 100.0;
        }

        return round($diffPrice / $diffStop * 100);
    }

    public function dividend(string $currency = null): float
    {
        $dividend = 0;

        foreach ($this->getFullCollection() as $stock) {
            if ($stock->dividendTotal !== null) {
                $sum = $stock->dividendTotal->total_sum;

                if ($currency) {
                    $dividend += StockHelper::priceToCurrency($sum, $stock->getCurrency(), $currency);
                } else {
                    $dividend += $sum;
                }
            }
        }

        return $dividend;
    }

    public function changePercent(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->profit() / ($this->totalPrice() / 100), 2);
    }

    /**
     * @throws Exception
     */
    #[ArrayShape(['name' => "string", 'data' => "array"])]
    public function chartSerie(array $categories, string $type = 'percent'): array
    {
        $data = [];
        $chartData = $this->chartData($type);

        foreach ($categories as $value) {
            $data[] = $chartData[$value] ?? null;
        }

        return [
            'name' => $this->getTicker(),
            'data' => $data,
        ];
    }

    /**
     * @return array
     * @throws Exception
     */
    public function chartCategories(): array
    {
        $categories = [];

        foreach ($this->getStockHistories() as $history) {
            $categories[] = Yii::$app->formatter->asDate($history['d'], 'short');
        }

        return $categories;
    }

    /**
     * @return StockHistory[]
     *
     * @throws Exception
     */
    public function getStockHistories(): array
    {
        static $histories;
        static $diffDays;

        if ($diffDays === null) {
            $diffDays = $this->getDateDiffDays();
        }

        if (($histories[$this->getId()] ?? null) === null) {
            $select = ['d' => 'date', 'p' => 'price_close'];

            if ($diffDays > 100) {
                $select = ['d' => 'MAX(date)', 'p' => 'MAX(price_close)'];
            }

            $query = StockHistory::find()
                ->select($select)
                ->andWhere(['stock_id' => $this->getPortfolioStock()->stock_id])
                ->andWhere(['>=', 'date', $this->getDate()])
                ->orderBy(['date' => SORT_ASC])
                ->asArray();

            if ($diffDays > 100) {
                $query->orderBy(['MAX("date")' => SORT_ASC]);

                if ($diffDays > 300) {
                    $query->groupBy(['date_part(\'year\', "date")', 'date_part(\'month\', "date")']);
                } else {
                    $query->groupBy(['date_part(\'year\', "date")', 'date_part(\'week\', "date")']);
                }
            }

            $histories[$this->getId()] = $query->all();
        }

        return $histories[$this->getId()];
    }

    /**
     * @param string $name
     * @return array
     * @throws Exception
     */
    public function chartData(string $name): array
    {
        static $data = [];

        $price = 0;
        $quantity = 0;

        if (($data[$this->getId()] ?? null) === null) {
            foreach ($this->getPortfolioStock() as $collection) {
                Yii::beginProfile('test-' . $this->getId());

                $price += $collection->quantity * $collection->price;
                $quantity += $collection->quantity;

                foreach ($this->getStockHistories() as $history) {
                    if ($history['d'] < $collection->date) {
                        continue;
                    }

                    $date = Yii::$app->formatter->asDate($history['d'], 'short');
                    $priceHistory = $history['p'] * $quantity;

                    $profit = $priceHistory - $price;
                    $percent = round($priceHistory / $price * 100 - 100, 2);

                    $data[$this->getId()]['price'] = $priceHistory;
                    $data[$this->getId()]['profit'][$date] = $profit;
                    $data[$this->getId()]['percent'][$date] = $percent;
                }

                Yii::endProfile('test-' . $this->getId());
            }
        }

        return $data[$this->getId()][$name] ?? [];
    }

    /**
     * @param Portfolio $portfolio
     *
     * @return PortfolioStockCollection[]
     */
    public static function getStockGroup(Portfolio $portfolio): array
    {
        /** @var PortfolioStockCollection[] $groups */
        $groups = [];

        foreach ($portfolio->portfolioStocks as $stock) {
            if ($portfolio->stock_type && $stock->stock->type !== $portfolio->stock_type) {
                continue;
            }

            if (($groups[$stock->stock_id] ?? null) === null) {
                $groups[$stock->stock_id] = new PortfolioStockCollection();
                $groups[$stock->stock_id]->stock_id = $stock->stock_id;
            }

            $groups[$stock->stock_id]->add($stock);
        }

        return $groups;
    }

    /**
     * @param Portfolio $portfolio
     *
     * @return PortfolioStockCollection[]
     */
    public static function getStockGroupClose(Portfolio $portfolio): array
    {
        /** @var PortfolioStockCollection[] $groups */
        $groups = [];

        foreach ($portfolio->portfolioStocksClose as $stock) {
            if (($groups[$stock->stock_id] ?? null) === null) {
                $groups[$stock->stock_id] = new PortfolioStockCollection();
                $groups[$stock->stock_id]->stock_id = $stock->stock_id;
            }

            $groups[$stock->stock_id]->add($stock);
        }

        return $groups;
    }
}
