<?php

namespace common\models\portfolio;

use common\models\Portfolio;
use common\models\stock\StockEntity;
use Yii;
use yii\base\Model;

/**
 * @property StockEntity $stock
 * @see Portfolio::getStock()
 */
class PortfolioCollection extends Model
{
    public string $id;
    public string $name;
    public float $price;
    public float $price_current;
    public float $profit;
    public string $currency;
    public string $currency_symbol;

    /** @var PortfolioStockCollection[] */
    public array $positions = [];

    public function add(PortfolioStockCollection $position): self
    {
        $this->positions[] = $position;

        return $this;
    }

    public function fields(): array
    {
        return [
            'id',
            'name',
            'price' => function () {
                return round($this->price, 2);
            },
            'price_current' => function () {
                return round($this->price_current, 2);
            },
            'profit' => function () {
                return round($this->profit, 2);
            },
            'percent' => function () {
                return round($this->price_current / $this->price * 100 - 100, 2);
            },
            'currency' => function () {
                return strtoupper($this->currency);
            },
            'currency_symbol',
            'positions',
        ];
    }
}
