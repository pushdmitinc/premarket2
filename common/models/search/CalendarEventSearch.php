<?php

namespace common\models\search;

use common\components\ActiveQuery;
use common\helpers\CalendarHelper;
use common\helpers\SettingsHelper;
use common\models\Calendar;
use common\models\interfaces\EventInterface;
use DateTime;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\CalendarEvent;
use yii\db\Expression;
use yii\helpers\Html;

/**
 * CalendarEventSearch represents the model behind the search form of `common\models\CalendarEvent`.
 */
class CalendarEventSearch extends CalendarEvent implements EventInterface
{
    public $dateStart;

    public $dateFinish;

    public $notCalendarIds;

    public $signParams = [];

    public $startYear;

    public $startMonth;

    public $isTime;

    public $_meta;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'calendar_id', 'status_id', 'signs'], 'integer'],
            [['date', 'time', 'name', 'image', 'style_class', 'description_short', 'description'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['startYear', 'startMonth', 'signParams', 'dateStart', 'dateFinish'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveQuery
     */
    public function query(array $params)
    {
        $query = CalendarEvent::find();

        $this->load($params);

        if (!$this->validate()) {
            return $query;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'calendar_id' => $this->calendar_id,
            'user_id' => $this->user_id,
            'date' => $this->date,
            'time' => $this->time,
            'status_id' => $this->status_id,
            'signs' => $this->signs,
        ]);

        $query->andFilterWhere(['>=', 'date', $this->dateStart])
            ->andFilterWhere(['<', 'date', $this->dateFinish]);

        $query->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'image', $this->image])
            ->andFilterWhere(['ilike', 'style_class', $this->style_class])
            ->andFilterWhere(['ilike', 'description_short', $this->description_short])
            ->andFilterWhere(['ilike', 'description', $this->description]);

        if ($this->notCalendarIds) {
            $query->andFilterWhere(['not', ['calendar_id' => $this->notCalendarIds]]);
        }

        if ($this->isTime) {
            $query->andFilterWhere(['<>', 'time', null]);
            $query->andFilterWhere(['>=', 'date', date('Y-m-d')]);
            $query->andFilterWhere(['or',
                ['>', 'time', date('H:i:s')],
                ['>', 'date', date('Y-m-d')],
            ]);
        }

        $query->orderBy(new Expression('date, time ASC NULLS FIRST'));

        return $query;
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params = []): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'query' => $this->query($params),
        ]);
    }

    /**
     * Возвращает ближайшие события (обратный отсчет)
     *
     * @param array $notCalendarIds
     *
     * @return ActiveDataProvider
     */
    public static function closelyForUser(array $notCalendarIds = null): ActiveDataProvider
    {
        $search = new self();

        $search->isTime = true;
        $search->user_id = \Yii::$app->user->id;
        $search->dateStart = date('Y-m-d');
        $search->notCalendarIds = $notCalendarIds;

        $dataProvider = $search->search();
        $dataProvider->pagination->setPageSize(4);
        $dataProvider->pagination->validatePage = false;

        return $dataProvider;
    }

    public static function closelyForUserModel()
    {
        $calendarIds = [];
        /** @var CalendarEvent[] $models */
        $models = self::closelyForUser()->getModels();

        foreach ($models as $model) {
            $calendarIds[$model->calendar_id] = $model->calendar_id;
        }

        if (count($calendarIds) === 1) {
            $subModels = self::closelyForUser($calendarIds)->getModels();

            if ($subModels) {
                $models[3] = $subModels[0];
            }

            if (count($subModels) > 1) {
                $models[2] = $subModels[0];
                $models[3] = $subModels[1];
            }
        }

        return $models;
    }

    public function dataEvent(array $params): array
    {
        $data = [];
        $query = $this->query(['CalendarEventSearch' => $params]);
        $query->select(['id', 'name', 'date',  'time', 'style_class', 'status_id', 'value']);

        /** @var CalendarEvent $model */
        foreach ($query->all() as $model) {
            $content = $model->nameHtml(true);

            if ($model->value) {
                $content .= ' ' . $model->value;
            }

            if (!$model->style_class) {
                $model->style_class = EventInterface::DEFAULT_CLASS;
            }

            if (!isset($data[$model->date])) {
                $data[$model->date] = [
                    'class' => $model->style_class,
                    'popover' => $content,
                ];
            } else {
                $data[$model->date]['popover'] .= "<br>" . $content;
            }

            $this->metaItem($model);
        }

        if ($this->calendar && $this->calendar->canSign(Calendar::SIGN_VIEW_META)) {
            $data['meta'] = $this->_meta;
        }

        return $data;
    }

    private function meta()
    {
        return [
            'items' => [],
            'months' => [],
            'total' => [
                'totalCount'=>[
                    'name' => $this->calendar->getParamValue('totalCountName'),
                    'value' => 0,
                ],
                'totalSum'=>[
                    'name' => $this->calendar->getParamValue('totalSumName'),
                    'value' => 0,
                ],
            ],
        ];
    }

    private function metaItem(CalendarEvent $model)
    {
        if ($this->_meta === null) {
            $this->_meta = $this->meta();
        }

        $name = $model->style_class;
        if ($this->calendar) {
            $name = $this->calendar->getStatusName($model->style_class);
        }

        if (!isset($this->_meta['items'][$model->style_class])) {
            $this->_meta['items'][$model->style_class] = [
                'name' => $name,
                'count' => 0,
                'sum' => 0,
            ];
        }

        if (isset($this->_meta['months'][$model->monthName()][$model->style_class])) {
            if ($model->value) {
                $this->_meta['months'][$model->monthName()][$model->style_class]['value'] += $model->value;
            }
            ++$this->_meta['months'][$model->monthName()][$model->style_class]['count'];
        } else {
            $this->_meta['months'][$model->monthName()][$model->style_class]['value'] = $model->value;
            $this->_meta['months'][$model->monthName()][$model->style_class]['count'] = 1;

            if (!SettingsHelper::isMobile()) {
                $this->_meta['months'][$model->monthName()][$model->style_class]['name'] = $name;
            }
        }

        ++$this->_meta['total']['totalCount']['value'];
        ++$this->_meta['items'][$model->style_class]['count'];

        if ($model->value) {
            $this->_meta['items'][$model->style_class]['sum'] += $model->value;
            $this->_meta['total']['totalSum']['value'] += $model->value;
        }
    }

    /**
     * @param DateTime $date
     * @return DateTime
     */
    public function dateStart(DateTime $date = null): DateTime
    {
        if (!$date instanceof DateTime) {
            $date = new DateTime();
        }

        $year = $this->startYear ?: $date->format('Y');
        $month = $this->startMonth ?: $date->format('m');

        return new DateTime($year . '-' . $month . '-01');
    }

    public function dateDiff(): ?string
    {
        if (!$this->date || $this->date === date('Y-m-d')) {
            return '';
        }

        return CalendarHelper::getIntervalText(new DateTime($this->date));
    }
}
