<?php

namespace common\models\search;

use common\helpers\SignsHelper;
use common\models\CalendarUser;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Calendar;

/**
 * CalendarSearch represents the model behind the search form of `common\models\Calendar`.
 */
class CalendarSearch extends Calendar
{
    public $isMy = false;

    public $isJoin = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'signs', 'user_id'], 'integer'],
            [['isMy'], 'boolean'],
            [['name', 'alias', 'image', 'description_short', 'description', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Calendar::find();
        $query->with(['calendarUsers']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['updated_at' => SORT_DESC]],
        ]);

        $this->load($params, '');
        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if (!Yii::$app->user->isGuest) {
            if ($this->isMy) {
                $this->user_id = Yii::$app->user->id;
            }

            if ($this->isJoin) {
                $query->innerJoin(CalendarUser::tableName(), 'calendar_id = ' . Calendar::tableName() . '.id');
                $query->andFilterWhere([
                    CalendarUser::tableName() . '.user_id' => Yii::$app->user->id
                ]);
            }
        }

        if (Yii::$app->user->isGuest || $this->user_id !== Yii::$app->user->id) {
            $this->signs = SignsHelper::getIntFromSigns([self::SIGN_IS_PUBLIC]);
        }

        $query->andFilterWhere([
            Calendar::tableName() . '.id' => $this->id,
            '(' . Calendar::tableName() . '.signs & ' . $this->signs . ')' => $this->signs,
            Calendar::tableName() . '.user_id' => $this->user_id,
            Calendar::tableName() . '.created_at' => $this->created_at,
            Calendar::tableName() . '.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['ilike', Calendar::tableName() . '.name', $this->name])
            ->andFilterWhere(['ilike', Calendar::tableName() . '.alias', $this->alias])
            ->andFilterWhere(['ilike', Calendar::tableName() . '.image', $this->image])
            ->andFilterWhere(['ilike', Calendar::tableName() . '.description', $this->description_short]);

        return $dataProvider;
    }

    public function joinCalendar(): ActiveDataProvider
    {
        $search = new self();

        $search->isJoin = true;

        return $search->search([]);
    }
}
