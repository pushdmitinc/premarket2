<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vk_friend_online".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $date_start
 * @property string|null $date_finish
 */
class VkFriendOnline extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%vk_friend_online}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id', 'date_start'], 'required'],
            [['user_id'], 'integer'],
            [['date_start', 'date_finish'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'date_start' => 'Date Start',
            'date_finish' => 'Date Finish',
        ];
    }

    public function finish(): bool
    {
        $this->date_finish = date('Y-m-d H:i:s');

        return $this->save();
    }

    public static function findCurrent($userId): ?self
    {
        return self::find()
            ->where(['user_id' => $userId])
            ->andWhere(['date_finish' => null])
            ->one();
    }

    public static function findOrCreate($userId): self
    {
        $model = self::findCurrent($userId);

        if ($model === null) {
            $model = new self();

            $model->user_id = $userId;
            $model->date_start = date('Y-m-d H:i:s');

            $model->save();
        }

        return $model;
    }
}
