<?php

namespace common\models;

use common\components\ActiveModel;
use common\helpers\ArrayHelper;
use common\helpers\StockHelper;
use common\models\portfolio\PortfolioSector;
use common\models\stock\Stock;
use common\models\stock\StockEntity;
use common\models\stock\StockHistory;
use common\traits\SignsTrait;
use DateTime;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "portfolio_stock".
 *
 * @property int $portfolio_id Акция
 * @property int $stock_id Акция
 * @property int $quantity Кол-во
 * @property float $price Цена
 * @property float $price_close Цена закрытия
 * @property float $take_profit Цель
 * @property float $stop_loss Стоп лосс
 * @property string $date Дата
 * @property string $date_close Дата
 * @property string $date_purpose Дата цели
 * @property string $currency_price Курс покупки валюты
 * @property float|null $commission Комиссия
 * @property int|null $signs Признаки
 * @property int $sector_id Сектор
 *
 * @property Stock $stock
 * @see PortfolioStock::getStock()
 *
 * @property PortfolioSector $sector
 * @see PortfolioStock::getSector()
 *
 * @property StockHistory[] $stockHistory
 * @see PortfolioStock::getStockHistory()
 *
 * @property Portfolio $portfolio
 * @see PortfolioStock::getPortfolio()
 *
 * @property Dividend[] $dividends
 * @see PortfolioStock::getDividends()
 *
 * @property DividendPortfolio[] $portfolioDividends
 * @see PortfolioStock::getPortfolioDividends()
 *
 * @property DividendPortfolio $dividendTotal
 * @see PortfolioStock::getDividendTotal()
 */
class PortfolioStock extends ActiveModel
{
    use SignsTrait;

    public const SING_CLOSE = 0;

    public int $purposePercent = 21;

    public ?int $stopLossPercent = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'portfolio_stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['portfolio_id', 'stock_id', 'price', 'quantity', 'date'], 'required'],
            [['portfolio_id', 'stock_id', 'signs'], 'default', 'value' => null],
            [['portfolio_id', 'stock_id', 'quantity', 'signs'], 'integer'],
            [['price', 'price_close', 'take_profit', 'stop_loss', 'currency_price', 'commission'], 'number'],
            [['date', 'date_close', 'date_purpose'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Портфель',
            'stock_id' => 'Акция',
            'price' => 'Цена',
            'price_close' => 'Цена закрытия',
            'take_profit ' => 'Цель',
            'stop_loss ' => 'Стоп лосс',
            'quantity' => 'Кол-во',
            'date' => 'Дата',
            'date_close' => 'Дата закрытия',
            'date_purpose' => 'Дата цели',
            'commission' => 'Комиссия',
            'currency_price' => 'Курс покупки валюты',
            'signs' => 'Signs',
            'purposePercent ' => 'Цель%',
            'stopLossPercent ' => 'Стоп%',
        ];
    }

    public function beforeSave($insert): bool
    {
        if ($this->date_close) {
            $this->addSign(self::SING_CLOSE);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return ActiveQuery
     */
    public function getStock(): ActiveQuery
    {
        return $this->hasOne(Stock::class, ['id' => 'stock_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getSector(): ActiveQuery
    {
        return $this->hasOne(PortfolioSector::class, ['id' => 'sector_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStockHistory(): ActiveQuery
    {
        return $this->hasOne(StockHistory::class, ['stock_id' => 'stock_id'])
            ->andWhere(['>=', 'date', $this->date])
            ->orderBy(['date' => SORT_ASC]);
    }

    /**
     * @return ActiveQuery
     */
    public function getPortfolio(): ActiveQuery
    {
        return $this->hasOne(Portfolio::class, ['id' => 'portfolio_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDividends(): ActiveQuery
    {
        $query = $this->hasMany(Dividend::class, ['stock_id' => 'stock_id'])
            ->andWhere(['>=', 'date_t2', $this->date]);

        if ($this->date_close) {
            $query->andWhere(['<', 'date_t2', $this->date_close]);
        }

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getPortfolioDividends(): ActiveQuery
    {
        return $this->hasMany(DividendPortfolio::class, ['portfolio_stock_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDividendTotal(): ActiveQuery
    {
        return $this->hasOne(DividendPortfolio::class, ['portfolio_stock_id' => 'id'])
            ->select(['portfolio_stock_id', 'SUM(dividend) total_sum'])
            ->groupBy('portfolio_stock_id');
    }

    /**
     * Цена покупки акции
     *
     * @return float
     */
    public function getPrice(): float
    {
        $price = $this->price;

        if ($this->stock->type === StockEntity::TYPE_BOND) {
            $price =  $this->stock->nominal * $price / 100;
        }

        return StockHelper::round($price);
    }

    /**
     * Цена продажи акции
     *
     * @return float
     */
    public function getPriceClose(): float
    {
        $priceClose = $this->price_close;

        if ($this->stock->type === StockEntity::TYPE_BOND) {
            $priceClose =  $this->stock->nominal * $this->price_close / 100;
        }

        return StockHelper::round($priceClose ?: 0);
    }

    /**
     * Валюта акции
     *
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->stock->currency;
    }

    /**
     * Цена позиции на момент покупки
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function totalPrice(string $currency = null): float
    {
        $price = $this->quantity * $this->getPrice();

        if ($currency && $this->stock->currency !== $currency) {
            return StockHelper::priceToCurrency($price, $this->stock->currency, $currency);
        }

        return $price;
    }

    /**
     * Цена позиции текущая
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function totalPriceCurrent(string $currency = null): float
    {
        $price = $this->quantity * $this->stock->getPricePrev();

        if ($currency && $this->stock->currency !== $currency) {
            return StockHelper::priceToCurrency($price, $this->stock->currency, $currency);
        }

        return $price;
    }

    /**
     * Цена закрытия позиции или текущая цена
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function totalPriceClose(string $currency = null): float
    {
        if (!$this->price_close) {
            return $this->totalPriceCurrent($currency);
        }

        $price = $this->quantity * $this->getPriceClose();

        if ($currency && $this->stock->currency !== $currency) {
            return StockHelper::priceToCurrency($price, $this->stock->currency, $currency);
        }

        return $price;
    }

    /**
     * Прибыль позиции
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function profit(string $currency = null): float
    {
        return $this->totalPriceClose($currency) - $this->totalPrice($currency);
    }

    /**
     * Прибыль позиции
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function profitCurrent(string $currency = null): float
    {
        return $this->totalPriceCurrent($currency) - $this->totalPrice($currency);
    }

    /**
     * Прибыль позиции с дивидендами
     *
     * @return float
     */
    public function profitTotal(): float
    {
        return $this->profit() + $this->dividend();
    }

    /**
     * Изменение позиции в %
     *
     * @return float
     */
    public function percent(): float
    {
        if ($this->totalPrice() === .0) {
            return 0;
        }

        return round($this->profit() / ($this->totalPrice() / 100), 2);
    }

    /**
     * Изменение позиции в %
     *
     * @return float
     */
    public function percentCurrent(): float
    {
        if ($this->totalPrice() === .0) {
            return 0;
        }

        return round($this->profitCurrent() / ($this->totalPrice() / 100), 2);
    }

    public function percentTotal(): float
    {
        return $this->percent() + $this->dividendPercent();
    }

    public function purpose(): float
    {
        return $this->take_profit ?: $this->price * ($this->purposePercent / 100 + 1);
    }

    public function purposeProfit(): float
    {
        return round($this->purpose() - $this->price, 2);
    }

    public function purposePercent(): float
    {
        return round(($this->purpose() - $this->price) / $this->price * 100, 2);
    }

    public function purposeStopLoss(): float
    {
        if ($this->stop_loss === null) {
            return 100.0;
        }

        return round(($this->price - $this->stop_loss) / $this->price * 100, 2);
    }

    public function purposeIntervalDays(): int
    {
        if ($this->date_purpose) {
            return (new DateTime($this->date))->diff(new DateTime($this->date_purpose))->days;
        }

        return 365;
    }

    public function purposePercentDay(): float
    {
        return $this->purposePercent() / $this->purposeIntervalDays();
    }

    public function purposeProfitDay(): float
    {
        return $this->purposeProfit() / $this->purposeIntervalDays();
    }

    public function percentToPurpose(): float
    {
        $percent = $this->percent();

        if ($percent <= 0) {
            return 0.0;
        }

        return round($percent / $this->purposePercent() * 100, 2);
    }

    public function percentToStopLoss(): float
    {
        $percent = $this->percent() * -1;

        if ($percent < 0 || $this->stop_loss === null) {
            return 0.0;
        }

        return round($percent / $this->purposeStopLoss() * 100, 2);
    }

    public function percentTotalToPurpose(): float
    {
        return round($this->percentTotal() / $this->purposePercent() * 100, 2);
    }

    public function dividend(string $currency = null): float
    {
        $dividend = $this->dividendTotal->total_sum ?? 0.0;

        if ($currency && $dividend > 0) {
            $dividend = StockHelper::priceToCurrency($dividend, $this->getCurrency(), $currency);
        }

        return $dividend;
    }

    /**
     * Изменение за день
     *
     * @param string|null $currency
     *
     * @return float
     */
    public function getChangeDay(string $currency = null): float
    {
        if ($this->stock->date_prev !== date('Y-m-d')) {
            return .0;
        }

        $price = $this->stock->getPricePrev();
        $percent = $this->stock->percent_day;

        $diff = $price / (100 + $percent) * $percent * $this->quantity;

        if ($currency && $this->stock->currency !== $currency) {
            return StockHelper::priceToCurrency($diff, $this->stock->currency, $currency);
        }

        return $diff;
    }

    public function dividendPercent(): float
    {
        if ($this->dividendTotal === null) {
            return 0.0;
        }

        return round($this->dividendTotal->total_sum / $this->totalPrice() * 100, 2);
    }

    public function daysBefore(): bool|int
    {
        $date = new DateTime($this->date);

        return (new DateTime())->diff($date)->days;
    }

    public function closePosition(): bool
    {
        $positions = PortfolioStock::find()
            ->where(['stock_id' => $this->stock_id])
            ->andWhere(['portfolio_id' => $this->portfolio_id])
            ->andWhere(['date_close' => null])
            ->orderBy(['date' => SORT_ASC])
            ->all();

        $sumQuantity = array_sum(ArrayHelper::getColumn($positions, 'quantity'));

        if ($sumQuantity < $this->quantity) {
            $this->addError('quantity', 'Количество больше чем сейчас в портфеле');

            return false;
        }

        foreach ($positions as $position) {
            if ($position->quantity > $this->quantity) {
                /** @var PortfolioStock $model */
                $model = $position->cloneModel([
                    'quantity' => $this->quantity,
                    'date_close' => $this->date_close,
                    'price_close' => $this->price_close,
                ]);

                $model->save();

                $position->quantity -= $this->quantity;
                $position->save();

                break;
            } else {
                $position->date_close = $this->date_close;
                $position->price_close = $this->price_close;

                $position->save();

                $this->quantity -= $position->quantity;

                if ($this->quantity === 0) {
                    break;
                }
            }
        }

        return true;
    }
}
