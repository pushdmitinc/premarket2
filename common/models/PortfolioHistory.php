<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "portfolio_history".
 *
 * @property int $id
 * @property int $portfolio_id Портфель
 * @property float $price_start Цена начальная
 * @property float $price_current Цена на дату
 * @property string $date Сортировка
 */
class PortfolioHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'portfolio_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['portfolio_id', 'price_start', 'price_current', 'date'], 'required'],
            [['portfolio_id'], 'default', 'value' => null],
            [['portfolio_id'], 'integer'],
            [['price_start', 'price_current'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'portfolio_id' => 'Portfolio ID',
            'price_start' => 'Price Start',
            'price_current' => 'Price Current',
            'date' => 'Date',
        ];
    }
}
