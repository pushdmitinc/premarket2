<?php

namespace common\models;

use common\components\ActiveModel;
use common\components\exception\ValidationModelException;
use Yii;

/**
 * This is the model class for table "friend".
 *
 * @property int $id
 * @property int $user_id
 * @property int $vk_id
 * @property string|null $nickname
 * @property string|null $track_code
 * @property int|null $sex
 * @property string|null $photo_100
 * @property string|null $first_name
 * @property string|null $last_name
 * @property bool|null $can_access_closed
 * @property bool|null $is_closed
 * @property string|null $delete_date
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class VkFriend extends ActiveModel
{
    const NAME_DELETED = 'DELETED';

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%vk_friend}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id'], 'required'],
            [['sex'], 'default', 'value' => null],
            [['vk_id', 'user_id', 'sex'], 'integer'],
            [['can_access_closed', 'is_closed'], 'boolean'],
            [['delete_date', 'created_at', 'updated_at'], 'safe'],
            [['nickname', 'track_code', 'photo_100', 'first_name', 'last_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'nickname' => 'Nickname',
            'track_code' => 'Track Code',
            'sex' => 'Sex',
            'photo_100' => 'Photo 100',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'can_access_closed' => 'Can Access Closed',
            'is_closed' => 'Is Closed',
            'delete_date' => 'Delete Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert): bool
    {
        if ($this->isAttributeChanged('delete_date')) {
            $this->setLog();
        }

        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes): void
    {
        if ($insert) {
            $this->setLog();
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return bool
     *
     * @throws ValidationModelException
     * @throws yii\db\Exception
     */
    public function setLog(): bool
    {
        $log = new VkFriendLog();

        $log->friend_id = $this->id;
        $log->date = date('Y-m-d H:i:s');
        $log->type = $this->delete_date ? 2 : 1;

        if (!$log->save()) {
            throw new ValidationModelException($log);
        }

        return true;
    }
}
