<?php

namespace common\models;

use common\components\ActiveModel;
use common\helpers\StockHelper;
use common\models\stock\Stock;
use common\traits\SignsTrait;
use frontend\assets\AppAsset;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "portfolio".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $name Название
 * @property int|null $signs Признаки
 *
 * @property PortfolioStock[] $portfolioStocksFull
 * @see Portfolio::getPortfolioStocksFull()
 *
 * @property PortfolioHistory[] $portfolioHistories
 * @see Portfolio::getPortfolioHistories()
 *
 * @property PortfolioStock[] $portfolioStocks
 * @see Portfolio::getPortfolioStocks()
 *
 * @property PortfolioStock[] $portfolioStocksClose
 * @see Portfolio::getPortfolioStocksClose()
 */
class Portfolio extends ActiveModel
{
    public ?int $stock_type = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'portfolio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id', 'name'], 'required'],
            [['signs'], 'default', 'value' => 1],
            [['user_id', 'signs'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'name' => 'Название',
            'signs' => 'Признаки',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    public function getPortfolioHistories(): ActiveQuery
    {
        return $this->hasMany(PortfolioHistory::class, ['portfolio_id' => 'id'])
            ->orderBy(['date' => SORT_ASC]);
    }

    public function getPortfolioStocksFull(): ActiveQuery
    {
        return $this->hasMany(PortfolioStock::class, ['portfolio_id' => 'id'])
            ->with(['stock', 'dividendTotal'])
            ->orderBy(['date' => SORT_ASC]);
    }

    public function getPortfolioStocks(): array
    {
        $stocks = [];

        foreach ($this->portfolioStocksFull as $portfolioStock) {
            if (!$portfolioStock->canSign(PortfolioStock::SING_CLOSE)) {
                $stocks[] = $portfolioStock;
            }
        }

        return $stocks;
    }

    public function getPortfolioStocksClose(): array
    {
        $stocks = [];

        foreach ($this->portfolioStocksFull as $portfolioStock) {
            if ($portfolioStock->canSign(PortfolioStock::SING_CLOSE)) {
                $stocks[] = $portfolioStock;
            }
        }

        return $stocks;
    }

    public function totalPrice(int $type = null): float
    {
        $totalPrice = 0;

        foreach ($this->portfolioStocks as $portfolioStock) {
            if ($type && $portfolioStock->stock->type !== $type) {
                continue;
            }

            $totalPrice += $portfolioStock->totalPrice(StockHelper::CURRENCY_RUB);
        }

        return $totalPrice;
    }

    public function totalPriceType(): float
    {
        return $this->totalPrice($this->stock_type);
    }

    public function totalPriceCurrent(int $type = null): float
    {
        $totalPrice = 0;

        foreach ($this->portfolioStocks as $portfolioStock) {
            if ($type && $portfolioStock->stock->type !== $type) {
                continue;
            }

            $totalPrice += $portfolioStock->totalPriceCurrent(StockHelper::CURRENCY_SYSTEM);
        }

        return $totalPrice;
    }

    public function totalPriceCurrentType(): float
    {
        return $this->totalPriceCurrent($this->stock_type);
    }

    public function profit(): float
    {
        return $this->totalPriceCurrent() - $this->totalPrice();
    }

    public function profitType(): float
    {
        return $this->totalPriceCurrent($this->stock_type) - $this->totalPrice($this->stock_type);
    }

    public function profitFull(): float
    {
        return $this->profit() + $this->dividend() + $this->profitClose();
    }

    public function profitWithClose(): float
    {
        return $this->profit() + $this->profitClose();
    }

    public function profitClose(): float
    {
        $sum = 0.0;

        foreach ($this->portfolioStocksClose as $portfolioStock) {
            $sum += $portfolioStock->profit(StockHelper::CURRENCY_SYSTEM);
        }

        return $sum;
    }

    public function dividend(): float
    {
        return DividendPortfolio::find()
            ->where(['portfolio_id' => $this->id])
            ->sum('dividend');
    }

    public function dividendType(): float
    {
        $query = DividendPortfolio::find()
            ->alias('dp')
            ->where(['dp.portfolio_id' => $this->id]);

        if ($this->stock_type) {
            $query->innerJoin(['ps' => PortfolioStock::tableName()], 'ps.id = dp.portfolio_stock_id');
            $query->innerJoin(['s' => Stock::tableName()], 's.id = ps.stock_id');
            $query->andWhere(['s.type' => $this->stock_type]);
        }

        return $query->sum('dp.dividend') ?: .0;
    }

    public function dividendClose(): float
    {
        $sum = 0;

        foreach ($this->portfolioStocksClose as $portfolioStock) {
            $sum += $portfolioStock->dividend(StockHelper::CURRENCY_SYSTEM);
        }

        return $sum;
    }

    public function getChangeDay(): float
    {
        $sum = 0;

        foreach ($this->portfolioStocks as $portfolioStock) {
            $sum += $portfolioStock->getChangeDay(StockHelper::CURRENCY_SYSTEM);
        }

        return $sum;
    }

    public function getChangePercentDay(): float
    {
		$totalPriceCurrent = $this->totalPriceCurrent();

		if ($totalPriceCurrent === 0.0) {
			return 0.0;
		}

        return round($this->getChangeDay() / $totalPriceCurrent * 100, 2);
    }


    /**
     * Доля позиции в портфеле
     *
     * @return float
     */
    public function getStockProportion(float $pricePosition): float
    {
        return round($pricePosition / $this->totalPriceCurrent() * 100, 2);
    }

    /**
     * Массив figi акций портфеля
     *
     * @return array
     */
    public function getFigiList(): array
    {
        $figi = [];

        foreach ($this->portfolioStocks as $portfolioStock) {
            if (!in_array($portfolioStock->stock->figi, $figi)) {
                $figi[] = $portfolioStock->stock->figi;
            }
        }

        return $figi;
    }

    public function changePercent(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->profit() / ($this->totalPrice() / 100), 2);
    }

    public function changePercentType(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->profitType() / ($this->totalPriceType() / 100), 2);
    }

    public function changePercentWithClose(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->profitWithClose() / ($this->totalPrice() / 100), 2);
    }

    public function changePercentFull(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->profitFull() / ($this->totalPrice() / 100), 2);
    }

    public function changePercentClose(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->profitClose() / ($this->totalPrice() / 100), 2);
    }

    public function changePercentDividend(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->dividend() / ($this->totalPrice() / 100), 2);
    }

    public function changePercentDividendType(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->dividendType() / ($this->totalPriceType() / 100), 2);
    }

    public function changePercentDividendClose(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return round($this->dividendClose() / ($this->totalPrice() / 100), 2);
    }

    public function changeTotalPercent(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return $this->changePercent() + $this->changePercentDividend();
    }

    public function changeTotalPercentType(): float
    {
        if ($this->totalPrice() === 0.0) {
            return 0;
        }

        return $this->changePercentType() + $this->changePercentDividendType();
    }
}
