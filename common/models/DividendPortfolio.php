<?php

namespace common\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "dividend_portfolio".
 *
 * @property int $id [integer]
 * @property string $portfolio_id [integer]  Портфель
 * @property string $portfolio_stock_id [integer]  Портфель
 * @property int $dividend_id [integer]  Дивиденд
 * @property float $dividend [double precision]  Дивиденды
 * @property string $date [date]  Дата
 * @property string $comment [varchar(255)]
 * @property int $type
 */
class DividendPortfolio extends ActiveRecord
{
	public const TYPE_DIVIDEND = 1;

	public const TYPE_TAX_DEDUCTION = 2;

	public const TYPE_OTHER = 3;

	public const TYPE_HKD = 4;

    public float $total_sum = 0.0;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'dividend_portfolio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['portfolio_stock_id', 'dividend', 'date'], 'required'],
            [['portfolio_stock_id', 'type'], 'integer'],
            [['dividend'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'portfolio_stock_id' => 'Портфель',
            'dividend' => 'Дивиденды',
            'date' => 'Date',
            'type' => 'Тип',
            'comment' => 'Комментарий',
        ];
    }
}
