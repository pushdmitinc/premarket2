<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "note".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int|null $group_id Группа
 * @property int|null $stock_id Акция
 * @property string $text Текст
 * @property int $sort Сортировка
 * @property string|null $date Дата
 */
class Note extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'note';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id', 'text'], 'required'],
            [['user_id', 'group_id', 'stock_id'], 'default', 'value' => null],
            [['user_id', 'group_id', 'stock_id', 'sort'], 'integer'],
            [['text'], 'string'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'group_id' => 'Group ID',
            'stock_id' => 'Stock ID',
            'text' => 'Text',
            'date' => 'Date',
        ];
    }
}
