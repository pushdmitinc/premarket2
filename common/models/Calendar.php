<?php

namespace common\models;

use common\components\ActiveModel;
use common\helpers\SettingsHelper;
use common\models\interfaces\SignsInterface;
use common\traits\SignsTrait;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "calendar".
 *
 * @property int $id
 * @property string $name Название
 * @property string $alias Алиас
 * @property string|null $image Картинка
 * @property string|null $description_short Описание краткое
 * @property string|null $keywords Ключевые слова
 * @property string|null $description Описание
 * @property int|null $signs Признаки
 * @property int $user_id Пользователь
 * @property string $params Параметры
 * @property string|null $date_start Дата начала
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property-read User $user
 * @property-read CalendarUser[] $calendarUsers
 * @property-read null|CalendarUser $calendarUser
 * @property-read CalendarEvent[] $calendarEvents
 */
class Calendar extends ActiveModel implements SignsInterface
{
    use SignsTrait;

    public const SIGN_IS_PUBLIC = 0;

    public const SIGN_VIEW_META = 1;

    public const SIGN_VIEW_DIFF = 2;

    public const SIGN_VIEW_LINK = 3;

    public const SIGN_SELECT_YEAR = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'user_id'], 'required'],
            [['description'], 'string'],
            [['signs', 'user_id'], 'default', 'value' => null],
            [['signs', 'user_id'], 'integer'],
            [['params', 'inputSigns', 'date_start', 'created_at', 'updated_at'], 'safe'],
            [['name', 'alias', 'image', 'description_short', 'keywords'], 'string', 'max' => 255],
            [['alias'], 'unique'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'alias' => 'Alias',
            'image' => 'Image',
            'description_short' => 'Описание краткое',
            'keywords' => 'Ключевые слова',
            'description' => 'Описание',
            'signs' => 'Signs',
            'user_id' => 'User ID',
            'params' => 'Параметры',
            'params[dangerName]' => 'Параметр[danger]',
            'date_start' => 'Дата начала',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        if ($this->inputSigns) {
            $this->setSigns($this->inputSigns);
        }

        if (!$this->alias) {
            $this->alias = md5(time());
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * @return CalendarUser
     */
    public function getCalendarUser(): ?CalendarUser
    {
        foreach ($this->calendarUsers as $user) {
            if ($user->user_id == SettingsHelper::currentUserId()) {
                return $user;
            }
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalendarUsers(): \yii\db\ActiveQuery
    {
        return $this->hasMany(CalendarUser::class, ['calendar_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalendarEvents()
    {
        return $this->hasMany(CalendarEvent::class, ['calendar_id' => 'id']);
    }

    public static function signsLabels(): array
    {
        return [
            self::SIGN_IS_PUBLIC => 'Публичный календарь',
            self::SIGN_VIEW_META => 'Показывать сумму',
            self::SIGN_VIEW_DIFF => 'Показывать разницу дней',
            self::SIGN_VIEW_LINK => 'Доступен по ссылке',
            self::SIGN_SELECT_YEAR => 'Выбор года',
        ];
    }

    public function getParamValue(string $name, string $default = null): string
    {
        if ($this->params) {
            if (isset($this->params[$name])) {
                return $this->params[$name];
            }
        }

        if ($default) {
            return $default;
        }

        return $name;
    }

    public function getStatusName(string $name, string $default = null): string
    {
        if ($this->params && isset($this->params['n'])) {
            if (isset($this->params['n'][$name]) && $this->params['n'][$name]) {
                return $this->params['n'][$name];
            }
        }

        if ($default) {
            return $default;
        }

        return $name;
    }

    /**
     * Проверка доступности
     *
     * @param int|null $userId
     *
     * @return bool
     */
    public function canUser(int $userId = null): bool
    {
        if ($this->canSign(self::SIGN_IS_PUBLIC)) {
            return true;
        }

        if ($this->user_id === $userId) {
            return true;
        }

        if ($this->calendarUser instanceof CalendarUser) {
            return true;
        }

        return false;
    }

    /**
     * Проверка доступности действия
     *
     * @param int|null $userId
     *
     * @return bool
     */
    public function canUserSing(int $sing, int $userId = null): bool
    {
        if ($this->canSign(Calendar::SIGN_IS_PUBLIC)) {
            if ($sing === CalendarUser::SING_VIEW) {
                return true;
            }
        }

        if ($this->canSign(Calendar::SIGN_VIEW_LINK)) {
            if ($sing === CalendarUser::SING_VIEW_LINK) {
                return true;
            }
        }

        if ($userId === null) {
            $userId = SettingsHelper::currentUserId();

            if ($userId === null) {
                return false;
            }
        }

        if ($this->user_id === $userId) {
            return true;
        }

        if ($this->calendarUser instanceof CalendarUser) {
            if ($this->calendarUser->canSign($sing)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Проверка доступности на обновление
     *
     * @param int|null $userId
     *
     * @return bool
     */
    public function canUpdate(int $userId = null): bool
    {
        if ($this->user_id === $userId) {
            return true;
        }

        if (!$this->calendarUser instanceof CalendarUser) {
            $query = CalendarUser::find();
            $query->where(['user_id' => $userId, 'calendar_id' => $this->id]);
            $this->calendarUser = $query->one();
        }

        if ($this->calendarUser instanceof CalendarUser) {
            return $this->calendarUser->canSign(CalendarUser::SING_UPDATE);
        }

        return false;
    }

    public function getLink()
    {
        return Url::to(['/calendar/alias/' . $this->alias], true);
    }
}
