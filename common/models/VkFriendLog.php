<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "friend_log".
 *
 * @property int $id
 * @property int|null $friend_id
 * @property int|null $type
 * @property string|null $date
 */
class VkFriendLog extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return '{{%vk_friend_log}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['friend_id', 'type'], 'default', 'value' => null],
            [['friend_id', 'type'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'friend_id' => 'Friend ID',
            'type' => 'Type',
            'date' => 'Date',
        ];
    }
}
