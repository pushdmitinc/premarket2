<?php

namespace common\models;

use common\models\stock\StockEntity;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PortfolioStock;

/**
 * PortfolioStockSearch represents the model behind the search form of `common\models\PortfolioStock`.
 */
class PortfolioStockSearch extends PortfolioStock
{
    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'portfolio_id', 'stock_id', 'signs'], 'integer'],
            [['ticker'], 'string'],
            [['price', 'price_prev'], 'number'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PortfolioStock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['name' => SORT_ASC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->alias('ps');
        $query->select([
            'ps.*',
            'ticker' => 's.ticker',
            'price_prev' => 's.price_prev',
            'percent_day' => 's.percent_day',
        ]);

        $query->innerJoin(['s' => StockEntity::tableName()], 's.id = ps.stock_id');

        if ($this->portfolio_id) {
            $dataProvider->pagination = false;
        }

        $query->andFilterWhere([
            'ps.id' => $this->id,
            'ps.portfolio_id' => $this->portfolio_id,
            'ps.stock_id' => $this->stock_id,
            'ps.price' => $this->price,
            'ps.date' => $this->date,
            'ps.signs' => $this->signs,
        ]);

        return $dataProvider;
    }
}
