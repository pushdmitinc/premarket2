<?php

namespace common\models\stock;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class StockSearch extends StockEntity
{
    public ?string $dateStart = null;

    /**
     * @inheritdoc
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['id', 'lot', 'issue_size'], 'integer'],
            [['ticker', 'class_code', 'name'], 'safe'],
            [['sector', 'currency'], 'safe'],
            [['date_prev', 'isin', 'date_ipo'], 'safe'],
            [['price_prev'], 'number'],
            [['min_price_increment', 'price_ytd_max', 'price_ytd_min'], 'number'],
            [['percent_ytd', 'percent_year', 'percent_month', 'percent_week', 'percent_day'], 'number'],
            [['percent_ytd_max', 'percent_ytd_min'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = StockEntity::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['ticker' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'price_prev' => $this->price_prev,
            'lot' => $this->lot,
            'min_price_increment' => $this->min_price_increment,
            'date_prev' => $this->date_prev,
            'issue_size' => $this->issue_size,
            'date_ipo' => $this->date_ipo,
            'percent_ytd_max' => $this->percent_ytd_max,
            'percent_ytd_min' => $this->percent_ytd_min,
        ]);

        $query->andFilterWhere(['ilike', 'ticker', $this->ticker])
            ->andFilterWhere(['ilike', 'class_code', $this->class_code])
            ->andFilterWhere(['ilike', 'name', $this->name])
            ->andFilterWhere(['ilike', 'sector', $this->sector])
            ->andFilterWhere(['ilike', 'currency', $this->currency])
            ->andFilterWhere(['ilike', 'isin', $this->isin]);

        $query->andFilterWhere(['>=', 'percent_day', $this->percent_day]);
        $query->andFilterWhere(['>=', 'date_prev', $this->dateStart]);

        return $dataProvider;
    }
}
