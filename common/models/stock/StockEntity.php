<?php

namespace common\models\stock;

use common\components\ActiveModel;
use common\helpers\StockHelper;
use common\traits\SignsTrait;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * This is the model class for table "stock".
 *
 * @property int $id
 * @property string|null $figi Figi-идентификатор инструмента
 * @property string $ticker Тикер
 * @property string|null $isin Isin-идентификатор инструмента
 * @property string|null $class_code Класс-код (секция торгов)
 * @property int|null $type Тип инструмента 1 - акция, 2 - валюта
 * @property int|null $lot Размер лота
 * @property string|null $currency Валюта
 * @property string|null $name Название
 * @property string|null $exchange Торговая площадка
 * @property string $real_exchange Реальная площадка исполнения расчётов
 * @property float|null $nominal Номинальная стоимость
 * @property float|null $min_price_increment Шаг цены
 * @property string|null $uid Уникальный идентификатор инструмента
 *
 * @property string|null $date_ipo Дата IPO акции в часовом поясе UTC
 * @property string|null $issue_size Размер выпуска
 * @property string|null $sector Сектор экономики
 *
 * @property string|null $iso_currency_name Строковый ISO-код валюты
 *
 * @property float|null $price_prev Предварительная цена
 * @property string|null $date_prev Предварительная дата
 *
 * @property float|null $price_ytd_max Мах. цена за год
 * @property float|null $price_ytd_min Мин. цена за год
 * @property float|null $percent_ytd_max Отклонение % от мах
 * @property float|null $percent_ytd_min Отклонение % от мин
 *
 * @property float|null $percent_year Изм, год %
 * @property float|null $percent_month Изм, месяц %
 * @property float|null $percent_week Изм, неделя %
 * @property float|null $percent_day Изм, день %
 *
 * @property float|null $percent_ytd Изменение с начала года %
 * @property float|null $percent_mtd Изменение с начала месяца %
 * @property float|null $percent_wtd Изменение с начала недели %
 *
 * @property bool|null $is_upload Обновление
 * @property bool|null $is_portfolio Обновление
 * @property int $signs [integer]
 *
 * @property-read  StockFuture $futureStock
 * @see  StockEntity::getFutureStock()
 */
class StockEntity extends ActiveModel
{
    use SignsTrait;

    public const SING_DELETE = 0;

    public const SING_SKIP_CHART = 1;

    public const TYPE_STOCK = 1;

    public const TYPE_CURRENCY = 2;

    public const TYPE_ETF = 3;

    public const TYPE_BOND = 4;

    public const TYPE_FUTURE = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['ticker'], 'required'],
            [['lot', 'type', 'signs'], 'integer'],
            [['nominal', 'price_prev', 'min_price_increment'], 'number'],
            [['price_ytd_max', 'price_ytd_min', 'percent_ytd_max', 'percent_ytd_min'], 'number'],
            [['percent_year', 'percent_month', 'percent_week', 'percent_day'], 'number'],
            [['percent_ytd', 'percent_mtd', 'percent_wtd'], 'number'],
            [['issue_size'], 'integer'],
            [['date_ipo', 'issue_size', 'date_prev'], 'safe'],
            [['is_upload', 'is_portfolio'], 'boolean'],
            [['class_code'], 'string', 'max' => 20],
            [['figi', 'currency'], 'string', 'max' => 12],
            [['ticker', 'isin'], 'string', 'max' => 36],
            [['name', 'exchange', 'real_exchange', 'uid', 'sector', 'iso_currency_name'], 'string', 'max' => 255],
            [['figi'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'ticker' => 'Тикер',
            'isin' => 'Isin',
            'class_code' => 'Класс-код',
            'name' => 'Название',
            'price_prev' => 'Цена',
            'lot' => 'Lot Size',
            'sector' => 'Sector',
            'min_step' => 'Min Step',
            'currency' => 'Currency',
            'date_prev' => 'Дата',
            'issue_size' => 'Issue Size',
            'reg_number' => 'Reg Number',
            'type' => 'Type',
            'date_ipo' => 'Date IPO',
            'price_ytd_max' => 'Мах. год',
            'price_ytd_min' => 'Мин. год',
            'percent_ytd_max' => 'мах %',
            'percent_ytd_min' => 'мин %',
            'percent_year' => 'год %',
            'percent_month' => 'мес %',
            'percent_week' => 'нед %',
            'percent_day' => 'день %',
            'percent_ytd' => 'YTD',
            'percent_mtd' => 'MTD',
            'percent_wtd' => 'WTD',
            'signs' => 'Признаки',
        ];
    }

    /**
     * @return string
     */
    public function getLogo(): string
    {
        $file = '/images/stock/' . $this->ticker . '.png';
        $path = Yii::getAlias('@backend/web') . $file;

        if (file_exists($path)) {
            return  Url::to($file, true);
        }

        return Url::to('/images/default-50x50.gif', true);
    }

    public function getSectorName(): string
    {
        $sector = $this->sector ?: 'Не указан';

        if ($this->sector === StockEntity::TYPE_CURRENCY) {
            $sector = 'Валюта';
        }

        return $sector;
    }

    public function getTypeName(): string
    {
        $list = [
            StockEntity::TYPE_STOCK => 'Акция',
            StockEntity::TYPE_CURRENCY => 'Валюта',
            StockEntity::TYPE_ETF => 'ETF',
            StockEntity::TYPE_BOND => 'Облигации',
        ];

        return $list[$this->type] ?? 'Не указано';
    }
    public function getPricePrev(): float
    {
        if ($this->type === self::TYPE_BOND) {
            return $this->nominal * $this->price_prev / 100;
        }

        return $this->price_prev ?: 0.0;
    }

    public function getCurrencySymbol(): string
    {
        return StockHelper::getCurrencySymbol($this->currency);
    }

    public function getFutureStock(): ActiveQuery
    {
        return $this->hasOne(StockFuture::class, ['future_id' => 'id']);
    }
}
