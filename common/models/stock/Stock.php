<?php

namespace common\models\stock;

/**
 * This is the model class for table "stock".
 */
class Stock extends StockEntity
{
    public function getParams(): array
    {
        return [
            'Максимальное цена за год' => $this->price_ytd_max,
            'Минимальное цена за год' => $this->price_ytd_min,
            'Отклонение от максимума за год %' => $this->percent_ytd_max,
            'Отклонение от минимума за год %' => $this->percent_ytd_min,
            'Изменение за год %' => $this->percent_ytd,
            'Изменение за месяц %' => $this->percent_month,
            'Изменение за неделю %' => $this->percent_week,
            'Изменение за день %' => $this->percent_day,
        ];
    }
}
