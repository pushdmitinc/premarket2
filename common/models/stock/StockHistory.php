<?php

namespace common\models\stock;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "stock_history".
 *
 * @property int $id
 * @property int $stock_id
 * @property string|null $date Дата
 * @property float|null $price_low Цена минимальная
 * @property float|null $price_open Цена открытия
 * @property float|null $price_close Цена закрытия
 * @property float|null $price_high Цена максимальная
 * @property int|null $volume Объем
 */
class StockHistory extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'stock_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['stock_id'], 'required'],
            [['date'], 'safe'],
            [['price_low', 'price_open', 'price_close', 'price_high'], 'number'],
            [['stock_id', 'volume'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'price_low' => 'Price Low',
            'price_open' => 'Price Open',
            'price_close' => 'Price Close',
            'price_high' => 'Price High',
            'volume' => 'Volume',
        ];
    }
}
