<?php

namespace common\models\stock;

use common\models\Portfolio;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "stock_notification".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property int $stock_id Акция
 * @property int|null $portfolio_id Портфель
 * @property float $stock_price [double precision]  Цена акции
 * @property float|null $take_profit Тейк профит
 * @property float|null $stop_loss Стоп лосс
 * @property string|null $text Текст
 * @property string $date Текст
 *
 * @property Stock $stock
 * @see StockNotification::getStock()
 *
 * @property Portfolio $portfolio
 * @see StockNotification::getPortfolio()
 */
class StockNotification extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'stock_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['user_id', 'stock_id'], 'required'],
            [['user_id', 'stock_id', 'portfolio_id'], 'default', 'value' => null],
            [['user_id', 'stock_id', 'portfolio_id'], 'integer'],
            [['stock_price', 'take_profit', 'stop_loss'], 'number'],
            [['text'], 'filter', 'filter' => function($value) {
                return $value ?: null;
            }],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'stock_id' => 'Stock ID',
            'portfolio_id' => 'Portfolio ID',
            'stock_price' => 'Stock Price',
            'take_profit' => 'Take Profit',
            'stop_loss' => 'Stop Loss',
            'text' => 'Text',
            'date' => 'Date',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getStock(): ActiveQuery
    {
        return $this->hasOne(Stock::class, ['id' => 'stock_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPortfolio(): ActiveQuery
    {
        return $this->hasOne(Portfolio::class, ['id' => 'portfolio_id']);
    }
}

