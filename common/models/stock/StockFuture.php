<?php

namespace common\models\stock;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $stock_id Акция
 * @property float $price_stock Цена акции
 * @property int $future_id Фьючерс
 * @property float $price_future Цена фьючерса
 * @property float $percent_diff Разница с активом
 * @property int $quantity Акции во фьючерсе
 * @property int $quantity_position Куплено
 * @property int|null $signs Признаки
 *
 * @property-read  StockEntity $future
 * @see  StockFuture::getFuture()
 * @property-read  StockEntity $stock
 * @see  StockFuture::getStock()
 */
class StockFuture extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'stock_future';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['stock_id', 'future_id', 'quantity'], 'required'],
            [['stock_id', 'future_id', 'signs'], 'default', 'value' => null],
            [['stock_id', 'future_id', 'quantity', 'quantity_position', 'signs'], 'integer'],
            [['price_future', 'price_stock', 'percent_diff'], 'number'],
            [['future_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'stock_id' => 'Акция',
            'price_stock' => 'Цена акции',
            'future_id' => 'Фьючерс',
            'price_future' => 'Цена фьючерса',
            'percent_diff' => 'Разница с активом',
            'quantity' => 'Актива в контракте',
            'quantity_position' => 'Куплено',
            'signs' => 'Signs',
        ];
    }

    public function getStock(): ActiveQuery
    {
        return $this->hasOne(Stock::class, ['id' => 'stock_id']);
    }

    public function getFuture(): ActiveQuery
    {
        return $this->hasOne(Stock::class, ['id' => 'future_id']);
    }

    public function percentDiff(?int $precision = null): float
    {
        $priceFuture = $this->future->price_prev / $this->quantity;
        $percent = ($priceFuture / $this->stock->price_prev - 1) * 100;

        if ($precision !== null) {
            $percent = round($percent, 2);
        }
        return $percent;
    }

    public function percentFullDiff(?int $precision = null): float
    {
        $priceFuture = $this->future->price_prev / $this->quantity;
        $priceStock = $this->stock->price_prev;

        if ($priceFuture > $priceStock) {
            $percent = $this->price_future / $priceFuture - 1;
            $percent += $priceStock / $this->price_stock - 1;
        } else {
            $percent = $priceFuture / $this->price_future - 1;
            $percent += $this->price_stock / $priceStock  - 1;
        }

        if ($precision !== null) {
            $percent = round($percent, 2);
        }

        return $percent;
    }
}
