<?php

namespace common\models;

use common\components\ActiveModel;
use common\helpers\CalendarHelper;
use common\helpers\DateHelper;
use common\helpers\SettingsHelper;
use common\traits\SignsTrait;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * This is the model class for table "calendar_event".
 *
 * @property int $id
 * @property string $date Дата
 * @property string|null $time Время
 * @property int $parent_id Основное событие
 * @property int $calendar_id Календарь
 * @property int|null $status_id Статус
 * @property string $name Название
 * @property int $value Значение
 * @property string|null $image Картинка
 * @property string|null $style_class Класс стилей
 * @property string|null $description_short Описание краткое
 * @property string|null $description Описание
 * @property int|null $signs Признаки
 * @property int|null $user_id Пользователь
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property CalendarSubscribe[] $subscribes
 * @property Calendar $calendar
 */
class CalendarEvent extends ActiveModel
{
    use SignsTrait;

    /** @var int */
    public $repeat;

    public $type;

    public $interval;

    public $dateTo;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calendar_event';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'calendar_id', 'name'], 'required'],
            [['date', 'time', 'created_at', 'updated_at'], 'safe'],
            [['calendar_id', 'status_id', 'signs'], 'default', 'value' => null],
            [['value'], 'safe'],
            [['dateTo', 'interval'], 'safe'],
            [['calendar_id', 'user_id', 'parent_id', 'status_id', 'signs', 'value', 'type', 'repeat'], 'integer'],
            [['description'], 'string'],
            [['name', 'image', 'style_class', 'description_short'], 'string', 'max' => 255],
            [['calendar_id'], 'exist', 'skipOnError' => true, 'targetClass' => Calendar::class, 'targetAttribute' => ['calendar_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'time' => 'Время',
            'calendar_id' => 'Календарь',
            'parent_id' => 'Основное событие',
            'status_id' => 'Статус',
            'name' => 'Название',
            'value' => 'Значение',
            'image' => 'Картинка',
            'style_class' => 'Стиль',
            'description_short' => 'Краткое описание',
            'description' => 'Описание',
            'user_id' => 'Пользователь',
            'signs' => 'Signs',
            'interval' => 'Повторять',
            'dateTo' => 'до даты',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'repeat' => 'Количество копий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCalendar(): ActiveQuery
    {
        return $this->hasOne(Calendar::class, ['id' => 'calendar_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribes(): ActiveQuery
    {
        return $this->hasMany(CalendarSubscribe::class, ['event_id' => 'id']);
    }

    public function getTime(): string
    {
        if ($this->time) {
            return substr($this->time, 0, 5);
        }

        return 'В/Д';
    }

    public function monthName(): string
    {
        [$year, $month,] = explode('-', $this->date);
        return DateHelper::monthName($month) . ' ' . $year;
    }

    public function dateTime(): string
    {
        $date = $this->date;

        if ($this->time) {
            $date .= ' ' . $this->time;
        } else {
            $date .= ' 00:00:00';
        }

        return $date;
    }

    public function isTimer(): bool
    {
        if ($this->time) {
            $date = new \DateTime();
            $date->add(new \DateInterval('P1D'));

            if ($this->date === date('Y-m-d') && $this->time > date('H:i:s')) {
                return true;
            }
            if ($this->date === $date->format('Y-m-d') && $this->time < date('H:i:s')) {
                return true;
            }
        }

        return false;
    }

    public function isTimerDay(): ?string
    {
        if ($this->time) {
            if ($this->isTimer()) {
                return '%H:%M:%S';
            }

            $date = new \DateTime();
            $date->add(new \DateInterval('P1D'));

            $diff = $date->diff(new \DateTime($this->date));

            if ($diff->days > 9) {
                return null;
            }

            if ($this->date > $date->format('Y-m-d')) {
                return '%Dд %Hч';
            }

            if ($this->date === $date->format('Y-m-d') && $this->time > date('H:i:s')) {
                return '%Dд %Hч';
            }
        }

        return null;
    }

    public function countdownFormat()
    {
        $format = '%Hч %Mм %Sс';

        $date = new \DateTime($this->dateTime());
        $date->add(new \DateInterval('P1D'));

        $diff = $date->diff(new \DateTime(), true);
        if ($diff->days > 1) {
            $format = '%Dд ' . $format;
        }

        return $format;
    }

    public function getStyleClass(): string
    {
        if (!$this->style_class) {
            return CalendarHelper::TYPE_DANGER;
        }

        return $this->style_class;
    }

    public function createChild(): void
    {
        if ($this->id && $this->dateTo && $this->interval) {
            $date = new \DateTime($this->date);
            $date->add(new \DateInterval($this->interval));

            while ($date->format('Y-m-d') <= $this->dateTo) {
                $child = self::findOne([
                    'parent_id' => $this->id,
                    'date' => $date->format('Y-m-d')
                ]);

                if (!$child) {
                    $child = new CalendarEvent();
                    $child->parent_id = $this->id;
                    $child->date = $date->format('Y-m-d');
                }

                $child->name = $this->name;
                $child->calendar_id = $this->calendar_id;
                $child->style_class = $this->style_class;
                $child->value = $this->value;

                $child->save();

                $date->add(new \DateInterval($this->interval));
            }
        }
    }

    public function nameHtml(bool $isTime = false): string
    {
        $class = 'text';

        if ($this->style_class) {
            $class .= ' text-' . $this->style_class;
        }

        if (CalendarHelper::getStatusAlias($this->status_id)) {
            $class .= ' text-' . CalendarHelper::getStatusAlias($this->status_id);
        }

        $name = $this->name;
        if ($isTime && $this->time !== null) {
            $name = $this->getTime() . ' ' . $this->name;
        }

        return Html::tag('span', $name, [
            'class' => $class,
        ]);
    }

    public function htmlDescription(): string
    {
        if ($this->value) {
            return 'Значение: <b>' . $this->value . '</b>';
        }

        return $this->description ?: 'нет описания';
    }

    public function isSubscribe()
    {
        if (SettingsHelper::currentUser() === null) {
            return false;
        }

        if (!SettingsHelper::isFirebaseSubscribe()) {
            return false;
        }

        if ($this->dateTime() <= date('Y-m-d H:i:s')) {
            return false;
        }

        return true;
    }

    public function iconSubscribe()
    {
        foreach ($this->subscribes as $subscribe) {
            if ($subscribe->user_id = SettingsHelper::currentUserId()) {
                return '<i class="fas fa-deaf text-secondary pull-right"></i>';
            }
        }

        return '<i class="fas fa-bullhorn text-success pull-right"></i>';
    }
}
