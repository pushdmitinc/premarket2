<?php

namespace common\models\interfaces;

use Yii;

interface EventInterface
{
    const DEFAULT_CLASS = 'danger';

    public function dataEvent(array $params): array;
}
