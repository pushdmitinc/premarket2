<?php

namespace common\models\interfaces;

interface SignsInterface
{
    public static function signsLabels(): array;
}
