<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "world".
 *
 * @property int $id
 * @property string $world Слово
 * @property int|null $signs Признаки
 */
class World extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'world';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['world'], 'required'],
            [['signs'], 'default', 'value' => null],
            [['signs'], 'integer'],
            [['world'], 'string', 'max' => 255],
            [['world'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'world' => 'World',
            'signs' => 'Signs',
        ];
    }
}
