<?php

declare(strict_types=1);

namespace common\fixtures;

use yii\test\ActiveFixture;

class CurrencyFixture extends ActiveFixture
{
    public $modelClass = 'common\models\Currency';
}