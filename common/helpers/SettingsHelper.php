<?php

declare(strict_types = 1);

namespace common\helpers;

use common\models\Calendar;
use common\models\User;
use common\models\UserMotive;
use common\widgets\Modal\ModalWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Application;

class SettingsHelper
{
    public const KEY_EXISTS_MOTIVE = 'existsMotive';

    public const KEY_EXISTS_CALENDAR = 'existsCalindar';

    public static $allowedTags = [
        'p', 'h1', 'h2', 'div',
        'table', 'tbody', 'tr', 'th', 'td',
        'b', 'i', 'u', 'strong', 'br', 'iframe', 'a'
    ];

    public static function get($name, $default = null)
    {
        if(isset(\Yii::$app->params[$name])){
            return \Yii::$app->params[$name];
        }

        return $default;
    }

    public static function getSetting($name, $default = null)
    {
        return self::get($name, $default);
    }

    public static function getTitle(): ?string
    {
        return self::get('defaultTitle');
    }

    public static function getKeywords(): ?string
    {
        return self::get('defaultKeywords');
    }

    public static function getDescription(): ?string
    {
        return self::get('defaultDescription');
    }

    public static function getOgImage(): ?string
    {
        return Url::to('/images/c2.jpg', true);
    }

    public static function currentUser(): ?User
    {
        if (\Yii::$app->user->getIsGuest()) {
            return null;
        }

        return \Yii::$app->user->getIdentity();
    }

    public static function currentUserId(): ?int
    {
        return \Yii::$app->user->getId();
    }

    public static function canSignsForUser(int $sign): ?bool
    {
        if (!\Yii::$app instanceof Application) {
            return false;
        }

        if (\Yii::$app->user->getIsGuest()) {
            return false;
        }

        /** @var User $user */
        $user = \Yii::$app->user->identity;

        return $user->canSign($sign);
    }

    public static function canUserAdmin(): ?bool
    {
        return self::canSignsForUser(User::SIGNS_ADMIN);
    }

    public static function isMobile()
    {
        if (!isset($_SERVER['HTTP_USER_AGENT'])) {
            return false;
        }

        $mobileAgent = [
            'ipad', 'iphone', 'android',
            'pocket', 'palm', 'windows ce',
            'windowsce', 'cellphone', 'opera mobi',
            'ipod', 'small', 'sharp',
            'sonyericsson', 'symbian', 'opera mini',
            'nokia', 'htc_', 'samsung',
            'motorola', 'smartphone', 'blackberry',
            'playstation portable', 'tablet browser'
        ];

        $agent = strtolower($_SERVER['HTTP_USER_AGENT']);

        foreach ($mobileAgent as $value) {
            if (strpos($agent, $value) !== false) return true;
        }

        return false;
    }

    public static function isNotMobile()
    {
        return !self::isMobile();
    }

    public static function countItemIndex()
    {
        return self::isMobile() ? 3 : 4;
    }

    public static function isMarket(): bool
    {
        if (\Yii::$app->user->isGuest) {
            return true;
        }

        if (self::existsNotCalendar()) {
            return true;
        }

        return false;
    }

    public static function existsMotive(): bool
    {
        $session = \Yii::$app->session;

        if ($session->has(self::KEY_EXISTS_MOTIVE)) {
            return $session->get(self::KEY_EXISTS_MOTIVE);
        }

        $session->set(
            self::KEY_EXISTS_MOTIVE,
            UserMotive::find()->where(['user_id' => self::currentUserId()])->exists()
        );

        return $session->get(self::KEY_EXISTS_MOTIVE);
    }

    public static function existsCalendar(): bool
    {
        $session = \Yii::$app->session;

        if ($session->has(self::KEY_EXISTS_CALENDAR)) {
            return $session->get(self::KEY_EXISTS_CALENDAR);
        }

        $session->set(
            self::KEY_EXISTS_CALENDAR,
            Calendar::find()->where(['user_id' => self::currentUserId()])->exists()
        );

        return $session->get(self::KEY_EXISTS_CALENDAR);
    }

    public static function existsNotCalendar(): bool
    {
        return !self::existsCalendar();
    }

    public static function marketTitle(): string
    {
        return 'Подарки, праздники и календари';
    }

    public static function marketSubTitle(): ?string
    {
        return null;

        if (\Yii::$app->user->isGuest) {
            return 'возьми время под контроль';
        }

        if (self::existsNotCalendar()) {
            return 'иметь план и цель первый шаг к успеху';
        }

        if (!self::existsMotive()) {
            return 'мотивация один из важных шагов к успеху';
        }

        return 'возьми время под контроль';
    }

    public static function marketLink(): string
    {
        if (\Yii::$app->user->isGuest) {
            return ModalWidget::widget([
                'url' => ['/site/signup'],
                'content' => '<i class="fas fa-plus"></i> Войти на сайт',
                'cssClass' => 'btn btn-primary',
            ]);
        }

        if (self::existsNotCalendar()) {
            return Html::a(
                '<i class="fas fa-plus"></i> Добавить календарь',
                ['/calendar/create'],
                ['class' => 'btn btn-success']
            );
        }

        if (!self::existsMotive()) {
            return ModalWidget::widget([
                'url' => ['/user-motive/update'],
                'content' => '<i class="fas fa-plus"></i> Добавить девиз',
                'cssClass' => 'btn btn-danger',
            ]);
        }

        return '';
    }

    public static function isFirebaseSubscribe(): bool
    {
        if (isset(\Yii::$app->params['FIREBASE_SUBSCRIBE'])) {
            return \Yii::$app->params['FIREBASE_SUBSCRIBE'];
        }

        return true;
    }

    public static function isMetrika(): bool
    {
        if (isset(\Yii::$app->params['METRIKA'])) {
            return \Yii::$app->params['METRIKA'];
        }

        return YII_ENV_PROD;
    }
}
