<?php

namespace common\helpers;

use DateInterval;
use DateTime;
use Exception;

class DateHelper
{
    public static $monthName = [
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь',
    ];

    public static $dayName = [
        'Понедельник',
        'Вторник',
        'Среда',
        'Четверг',
        'Пятница',
        'Суббота',
        'Воскресенье'
    ];

    public static $dayNameSort = [
        'Пн',
        'Вт',
        'Ср',
        'Чт',
        'Пт',
        'Сб',
        'Вс'
    ];

    public static function monthName(int $month): string
    {
        if (isset(self::$monthName[$month])) {
            return self::$monthName[$month];
        }

        return 'не определен';
    }

    public static function runningDay(int $month, int $year): int
    {
        $runningDay = date('w', mktime(0, 0, 0, $month, 1, $year));
        $runningDay = $runningDay - 1;

        if ($runningDay < 0) $runningDay = 6;

        return $runningDay;
    }

    /**
     * @return array
     */
    public static function yearsList(int $count = 10): array
    {
        $ysar = date('Y');
        $result = [];

        for ($i = -1; $i < $count; $i++) {
            $result[$ysar - $i] = $ysar - $i;
        }

        return $result;
    }

    /**
     * @return DateTime
     *
     * @throws Exception
     */
    public static function weekStart(): DateTime
    {
        $dateWeek = new DateTime();
        $dayOfWeek = (int) $dateWeek->format('w');

        if ($dayOfWeek === 0) {
            $dayOfWeek = 7;
        }

        $dayOfWeek = $dayOfWeek - 1;

        if ($dayOfWeek > 0) {
            $dateWeek->sub(new DateInterval('P' . $dayOfWeek . 'D'));
        }

        return $dateWeek;
    }
}
