<?php

namespace common\helpers;

use common\models\DividendPortfolio;
use common\models\Portfolio;
use common\models\portfolio\PortfolioStockCollection;
use common\models\PortfolioStock;
use common\models\stock\StockEntity;
use common\models\stock\StockHistory;
use DateTime;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use Yii;
use yii\base\InvalidConfigException;

class PortfolioHelper
{
    public static ?array $dataProfit = null;

    public static ?array $dataPercent = null;

    public static ?array $dataPrice = null;

    public static ?array $dataPriceCurrent = null;

    public static ?array $categories = null;

    public static ?int $purposeCloseDays = null;

    /**
     * @throws Exception
     */
    public static function initData(PortfolioStock $stock): void
    {
        if (self::$categories !== null) {
            return;
        }

        self::$dataProfit['actual'][] = [0];
        self::$dataProfit['actualDiv'][] = [0];
        self::$dataProfit['target'][] = [$stock->purposeProfit() * $stock->quantity];
        self::$dataProfit['targetMed'][] = [0];

        self::$dataPercent['actual'][] = [0];
        self::$dataPercent['actualDiv'][] = [0];
        self::$dataPercent['target'][] = [$stock->purposePercent()];
        self::$dataPercent['targetMed'][] = [0];

        self::$categories = [Yii::$app->formatter->asDate($stock->date, 'd MMM')];

        $dividendSum = 0;
        $dateCreate = new DateTime($stock->date);

        $purposeProfit = $stock->purposeProfit() * $stock->quantity;
        $purposeProfitDay = $stock->purposeProfitDay() * $stock->quantity;

        $purposePercentDay = $stock->purposePercentDay();
        $purposePercent = $stock->purposePercent();

        /** @var StockHistory[] $history */
        $histories = StockHistory::find()
            ->andWhere(['stock_id' => $stock->stock_id])
            ->andWhere(['>', 'date', $stock->date])
            ->orderBy(['date' => SORT_ASC])
            ->all();

        /** @var DividendPortfolio[] $dividends */
        $dividends = DividendPortfolio::find()
            ->andWhere(['portfolio_stock_id' => $stock->id])
            ->indexBy('date')
            ->all();

        foreach ($histories as $history) {
            $date = new DateTime($history->date);
            self::$categories[] = Yii::$app->formatter->asDate($history->date, 'd MMM');

            if ($dividends[$history->date] ?? null) {
                $dividendSum += $dividends[$history->date]->dividend;
            }

            $actualPrice = round(($history->price_close - $stock->price) * $stock->quantity, 2);
            self::$dataProfit['actual'][] = $actualPrice;
            self::$dataProfit['actualDiv'][] = round($actualPrice + $dividendSum, 2);

            self::$dataPercent['actual'][]  = round(($history->price_close / $stock->price - 1) * 100, 2);
            self::$dataPercent['actualDiv'][] = round((($history->price_close + $dividendSum) / $stock->price - 1) * 100, 2);

            $days = $dateCreate->diff($date)->days;

            self::$dataProfit['targetMed'][] = round($purposeProfitDay * $days, 2);
            self::$dataProfit['target'][] = $purposeProfit;

            self::$dataPercent['targetMed'][] = round($purposePercentDay * $days, 2);
            self::$dataPercent['target'][] = $purposePercent;

            $price = round($stock->price * $stock->quantity, 2);
            $priceCurrent = round($history->price_close * $stock->quantity, 2);

            self::$dataPrice[$date->format('Y-m-d')] = $price;
            self::$dataPriceCurrent[$date->format('Y-m-d')] = $priceCurrent;

            if (self::$purposeCloseDays === null && $actualPrice >= $purposeProfit) {
                self::$purposeCloseDays = $days;
            }
        }
    }

    /**
     * @param PortfolioStock $stock
     *
     * @return array
     *
     * @throws Exception
     */
    public static function getParams(PortfolioStock $stock): array
    {
        self::initData($stock);

        return [
            'Цель достигнута за дней' => self::$purposeCloseDays,
        ];
    }

    /**
     * @throws Exception
     */
    #[ArrayShape(['series' => "array[]", 'xAxis' => "array[]|null[]"])]
    public static function getChartProfit(PortfolioStock $stock): array
    {
        self::initData($stock);

        return [
            'series' => [
                [
                    'name' => 'Без дивидендов',
                    'data' => self::$dataProfit['actual'],
                ],
                [
                    'name' => 'Цель',
                    'lineWidth' => 1,
                    'dashStyle' => 'dash',
                    'data' => self::$dataProfit['targetMed'],
                ],
                [
                    'name' => 'Цель',
                    'lineWidth' => 1,
                    'dashStyle' => 'dash',
                    'data' => self::$dataProfit['target'],
                ],
                [
                    'name' => 'С дивидентами',
                    'lineWidth' => 1,
                    'data' => self::$dataProfit['actualDiv'],
                    'dashStyle' => 'shortdot',
                ],
            ],
            'xAxis' => [
                'categories' => self::$categories,
            ],
        ];
    }

    /**
     * @throws Exception
     */
    #[ArrayShape(['series' => "array[]", 'xAxis' => "array[]|null[]"])]
    public static function getChartPercent(PortfolioStock $stock): array
    {
        self::initData($stock);

        return [
            'series' => [
                [
                    'name' => 'Без дивидендов',
                    'data' => self::$dataPercent['actual'],
                ],
                [
                    'name' => 'Цель',
                    'lineWidth' => 1,
                    'dashStyle' => 'dash',
                    'data' => self::$dataPercent['targetMed'],
                ],
                [
                    'name' => 'Цель %',
                    'lineWidth' => 1,
                    'dashStyle' => 'dash',
                    'data' => self::$dataPercent['target'],
                ],
                [
                    'name' => 'С дивидентами',
                    'lineWidth' => 1,
                    'data' => self::$dataPercent['actualDiv'],
                    'dashStyle' => 'shortdot',
                ],
            ],
            'xAxis' => [
                'categories' => self::$categories,
            ],
        ];
    }

    /**
     * @param PortfolioStockCollection[] $groups
     * @param string $type
     *
     * @return array
     *
     * @throws Yii\base\InvalidConfigException
     */
    #[ArrayShape(['series' => "array", 'xAxis' => "array[]|null[]"])]
    public static function getChart(array $groups, string $type = 'percent'): array
    {
        $date = null;

        $series = [];
        $categories = [];

        foreach ($groups as $group) {
            if ($date === null || $group->getDate() < $date) {
                $date = $group->getDate();
                $categories = $group->chartCategories();
            }
        }

        foreach ($groups as $group) {
            $series[] = $group->chartSerie($categories, $type);
        }

        return [
            'series' => $series,
            'xAxis' => [
                'categories' => $categories,
            ],
        ];
    }

    #[ArrayShape(['series' => "array[]", 'xAxis' => "array[]|null[]"])]
    public static function getChartPrice(Portfolio $portfolio): array
    {
        $sum = 0;

        $dataPrice = [];
        $dataPriceAdd = [];
        $categories = [];

        foreach ($portfolio->portfolioStocks as $stock) {
            $date = Yii::$app->formatter->asDate($stock->date, 'LLL YY');
            $sum += $stock->price * $stock->quantity;

            $dataPrice[$date] = $sum;
            $dataPriceAdd[$date] = ($dataPriceAdd[$date] ?? 0) + $stock->price * $stock->quantity;

            if (!in_array($date, $categories)) {
                $categories[] = $date;
            }
        }

        return [
            'series' => [
                [
                    'name' => 'Стоимость',
                    'data' => array_values($dataPrice),
                ],
                [
                    'name' => 'Куплено',
                    'data' => array_values($dataPriceAdd),
                ],
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
        ];
    }

    /**
     * @param Portfolio $portfolio
     *
     * @return array
     *
     * @throws InvalidConfigException
     */
    #[ArrayShape(['series' => "array[]", 'xAxis' => "array[]|null[]"])]
    public static function getChangePercent(Portfolio $portfolio): array
    {
		$dataPrice = [];
        $categories = [];

        foreach ($portfolio->portfolioHistories as $history) {
            $date = Yii::$app->formatter->asDate($history->date, 'dd.MM.YY');

            $categories[] = $date;
            $dataPrice[$date] =  $history->price_current * 100 / $history->price_start - 100;
        }

        return [
            'series' => [
                [
                    'name' => 'Стоимость',
                    'data' => array_values($dataPrice),
                ],
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
        ];
    }

    #[ArrayShape(['series' => "array[]", 'xAxis' => "array[]|null[]"])]
    public static function getChangeYear(Portfolio $portfolio): array
    {
        $data = [];
        $categories = [];

        $years = [];
        $ytd = [];

        foreach ($portfolio->portfolioStocks as $stock) {
            $ticker = $stock->stock->ticker;

            if ($stock->stock->canSign(StockEntity::SING_SKIP_CHART)) {
                continue;
            }

            if (($data[$ticker] ?? null) === null) {
                $data[$ticker] = [
                    'year' => $stock->stock->percent_year,
                    'ytd' => $stock->stock->percent_ytd,
                ];
            }
        }

        uasort($data, function($a, $b) {
            return $a['year'] <=> $b['year'];
        });

        foreach ($data as $key => $item) {
            $categories[] = $key;
            $years[] = $item['year'];
            $ytd[] = $item['ytd'];
        }

        return [
            'series' => [
                [
                    'name' => 'TTM',
                    'data' => $years,
                ],
                [
                    'name' => 'YTD',
                    'data' => $ytd,
                ],
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
        ];
    }

    /**
     * @param Portfolio $portfolio
     *
     * @return array
     *
     * @throws yii\base\InvalidConfigException
     */
    #[ArrayShape(['series' => "array"])]
    public static function getChartCurrency(Portfolio $portfolio): array
    {
        $totalPrice = $portfolio->totalPriceCurrent();

        $series = [];
        $currencies = [];

        foreach ($portfolio->portfolioStocks as $portfolioStock) {
            $currency = $portfolioStock->stock->currency;

            $currencies[$currency] = ($currencies[$currency] ?? 0)
                + $portfolioStock->totalPriceCurrent(StockHelper::CURRENCY_RUB);
        }

        foreach ($currencies as $currency => $value) {
            $series[] = [
                'name' => $currency,
                'price' => Yii::$app->formatter->asCurrency($value),
                'y' => round($value / $totalPrice * 100, 2),
            ];
        }

        return [
            'series' => [
                [
                    'name' => 'Доля',
                    'data' => $series,
                ],
            ],
        ];
    }

    /**
     * @param Portfolio $portfolio
     *
     * @return array
     *
     * @throws yii\base\InvalidConfigException
     */
    #[ArrayShape(['series' => "array"])]
    public static function getChartSector(Portfolio $portfolio): array
    {
        $totalPrice = $portfolio->totalPriceCurrent();

        $series = [];
        $currencies = [];

        foreach ($portfolio->portfolioStocks as $portfolioStock) {
            $sector = $portfolioStock->stock->getSectorName();

            $currencies[$sector] = ($currencies[$sector] ?? 0)
                + $portfolioStock->totalPriceCurrent(StockHelper::CURRENCY_RUB);
        }

        foreach ($currencies as $sector => $value) {
            $series[] = [
                'name' => $sector,
                'price' => Yii::$app->formatter->asCurrency($value),
                'y' => round($value / $totalPrice * 100, 2),
            ];
        }

        return [
            'series' => [
                [
                    'name' => 'Доля',
                    'data' => $series,
                ],
            ],
        ];
    }

    /**
     * @param Portfolio $portfolio
     *
     * @return array
     *
     * @throws yii\base\InvalidConfigException
     */
    #[ArrayShape(['series' => "array"])]
    public static function getChartType(Portfolio $portfolio): array
    {
        $totalPrice = $portfolio->totalPriceCurrent();

        $series = [];
        $currencies = [];

        foreach ($portfolio->portfolioStocks as $portfolioStock) {
            $type = $portfolioStock->stock->getTypeName();

            $currencies[$type] = ($currencies[$type] ?? 0)
                + $portfolioStock->totalPriceCurrent(StockHelper::CURRENCY_RUB);
        }

        foreach ($currencies as $type => $value) {
            $series[] = [
                'name' => $type,
                'price' => Yii::$app->formatter->asCurrency($value),
                'y' => round($value / $totalPrice * 100, 2),
            ];
        }

        return [
            'series' => [
                [
                    'name' => 'Доля',
                    'data' => $series,
                ],
            ],
        ];
    }
}
