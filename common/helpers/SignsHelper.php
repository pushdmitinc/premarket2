<?php

namespace common\helpers;

use common\models\interfaces\SignsInterface;
use yii\db\ActiveRecord;
use yii\widgets\ActiveField;
use yii\widgets\ActiveForm;

class SignsHelper
{
    const INPUT_NAME = 'inputSigns';

    /**
     * @param ActiveForm $form
     * @param SignsInterface|ActiveRecord $model
     * @param int $index
     *
     * @return ActiveField
     */
    public static function checkbox(ActiveForm $form, SignsInterface $model, int $index = 0): ActiveField
    {
        return $form->field($model, 'inputSigns[' . $index . ']')->checkbox([
            'checked' => $model->canSign($index),
            'label' => self::labelName($model, $index)
        ]);
    }

    /**
     * @param SignsInterface $model
     * @param int $index
     *
     * @return string
     */
    public static function labelName(SignsInterface $model, int $index = 0): string
    {
        $labels = $model::signsLabels();

        if (isset($labels[$index])) {
            return $labels[$index];
        }

        return $model->getAttributeLabel(self::INPUT_NAME);
    }

    public static function getIntFromSigns(array $signs): int
    {
        $result = 0;

        foreach ($signs as $sign) {
            $result += 0b1 << $sign;
        }

        return $result;
    }

    public static function canSigns(int $signs, int $index = 0):  bool
    {
        return (bool) ($signs >> $index & 0b1);
    }
}
