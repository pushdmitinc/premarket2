<?php

namespace common\helpers;

class ArrayHelper extends \yii\helpers\ArrayHelper
{
    public static function avg(array $array): float
    {
        if (count($array) === 0) {
            return 0.00;
        }

        return (float) array_sum($array) / count($array);
    }
}
