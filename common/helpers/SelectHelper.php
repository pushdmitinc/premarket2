<?php

namespace common\helpers;

class SelectHelper
{
    public static function year(int $pref = 10, int $next = 1): array
    {
        $result = [];
        $start = (int) date('Y');

        for ($year = $start + $next; $year >= $start - $pref; --$year) {
            $result[$year] = $year;
        }

        return $result;
    }

    public static function period(): array
    {
        return [
            'год' => 'год',
            '1 кв' => '1 кв',
            '2 кв' => '2 кв',
            '3 кв' => '3 кв',
            '4 кв' => '4 кв',
        ];
    }
}
