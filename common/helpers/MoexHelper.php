<?php

namespace common\helpers;

use common\components\exception\ValidationModelException;
use common\models\stock\StockEntity;
use common\models\stock\StockHistory;

class MoexHelper
{
    public static function changePercent(float $price, float $oldPrice, int $precision = 2): float
    {
        if ($oldPrice > 0) {
            return round($price / ($oldPrice / 100) - 100, $precision);
        }

        return 0.0;
    }

    public static function labelColor(StockEntity $model, ?string $prefix = 'badge'): string
    {
        $class = 'bg-light-blue';

        if ($model->list_level === 3) {
            $class = 'bg-red';
        }

        if ($model->list_level === 2) {
            $class = 'bg-aqua';
        }

        if ($prefix !== null) {
            $class = $prefix . ' ' . $class;
        }

        return $class;
    }

    public static function maxColor(StockEntity $model, ?string $prefix = null): string
    {
        $class = '';

        if ($model->percent_ytd_max === null) {
            return 'bg-gray';
        }

        if ($model->percent_ytd_max < -28) {
            $class = 'bg-red';
        }

        if ($model->percent_ytd_max < -30) {
            $class = 'bg-green';
        }

        if ($prefix !== null && $class !== '') {
            $class = $prefix . ' ' . $class;
        }

        return $class;
    }



    public static function loadMoex($data): void
    {
        $ticker = $data[0];

        $stock = StockEntity::find()->where(['ticker' => $ticker])->one();

        if ($stock === null) {
            $stock = new StockEntity();
        }

        $stock->ticker = $ticker;
        $stock->class_code = $data[1];
        $stock->name = $data[9];

        $stock->price_prev = $data[3];
        $stock->date_prev = $data[17];

        $stock->sector = $data[13];
        $stock->min_step = $data[14];
        $stock->face_unit = $data[16];
        $stock->issue_size = $data[18];
        $stock->isin = $data[19];
        $stock->currency = $data[24];

        if (!$stock->save()) {
            throw new ValidationModelException($stock);
        }
    }

    public static function historyLoad(StockEntity $stock, array $data): bool
    {
        $ticker = $data[3];
        $date = $data[1];

        $history = StockHistory::find()
            ->where(['ticker' => $ticker, 'date' => $date])
            ->one();

        if ($history instanceof StockHistory) {
            if ($history->num_trades >= $data[4]) {
                return true;
            }
        } else {
            $history = new StockHistory();
        }

        $history->stock_id = $stock->id;
        $history->ticker = $ticker;
        $history->date = $date;

        $history->board = $data[0];
        $history->num_trades = $data[4];
        $history->value = $data[5];
        $history->price_open = $data[6];
        $history->price_low = $data[7];
        $history->price_high = $data[8];
        $history->price_close_legal = $data[9];
        $history->price_wa = $data[10];
        $history->price_close = $data[11];
        $history->volume = $data[12];

        return $history->save();
    }

    public static function marketDataLoad(StockEntity $stock, array $data, string $date): bool
    {
        $ticker = $data[0];
        $numTrades = $data[26];

        $history = StockHistory::find()
            ->where(['ticker' => $ticker, 'date' => $date])
            ->one();

        if ($history instanceof StockHistory) {
            if ($history->num_trades >= $numTrades) {
                return true;
            }
        } else {
            $history = new StockHistory();
        }

        $history->stock_id = $stock->id;
        $history->ticker = $ticker;
        $history->date = $date;
        $history->board = $data[1];
        $history->num_trades = $numTrades;

        $history->value = $data[16];
        $history->price_open = $data[9];
        $history->price_low = $data[10];
        $history->price_high = $data[11];
        $history->price_close = $data[12];

        return $history->save();
    }
}
