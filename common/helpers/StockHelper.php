<?php

declare(strict_types=1);

namespace common\helpers;

use common\models\PortfolioStock;
use common\models\stock\StockEntity;
use common\models\stock\StockHistory;
use DateInterval;
use DateTime;

class StockHelper
{
    public const CURRENCY_SYSTEM = 'rub';

    public const CURRENCY_RUB = 'rub';
    public const CURRENCY_USD = 'usd';
    public const CURRENCY_EUR = 'eur';
    public const CURRENCY_HKD = 'hkd';
    public const CURRENCY_CNY = 'cny';

    public static function getCurrencySymbol($currency): string
    {
        $list = [
            self::CURRENCY_RUB => '₽',
            self::CURRENCY_USD => '$',
            self::CURRENCY_EUR => '€',
            self::CURRENCY_CNY => '¥',
            self::CURRENCY_HKD => 'HK$',
        ];

        return $list[$currency] ?? $currency;
    }

    public static function getCurrencySystem(): string
    {
        return self::CURRENCY_SYSTEM;
    }

    public static function getCurrencyModel(string $currency): ?StockEntity
    {
        static $currencies;

        $tickers = [
            self::CURRENCY_USD => 'USD000UTSTOM',
            self::CURRENCY_EUR => 'EUR_RUB__TOM',
            self::CURRENCY_HKD => 'HKDRUB_TOM',
        ];

        if (($tickers[$currency] ?? null) === null) {
            return null;
        }

        if ($currencies === null) {
            /** @var StockEntity[] $currencies */
            $currencies = StockEntity::find()
                ->where(['ticker' => $tickers])
                ->indexBy('ticker')
                ->all();
        }

        return $currencies[$tickers[$currency]] ?? null;
    }

    /**
     * Цена позиции в другой валюте
     *
     * @param float $price
     * @param string $from
     * @param string $to
     *
     * @return float
     */
    public static function priceToCurrency(float $price, string $from, string $to = self::CURRENCY_SYSTEM): float
    {
        if ($from === $to) {
            return $price;
        }

        $currencyModel = StockHelper::getCurrencyModel($from);

        if ($currencyModel) {
            if ($to === self::CURRENCY_SYSTEM) {
                return $price * $currencyModel->price_prev;
            }
        }

        $currencyModel = StockHelper::getCurrencyModel($to);

        if ($currencyModel) {
            return $price / $currencyModel->price_prev;
        }

        return $price;
    }

    /**
     * @param float $number
     *
     * @return float
     */
    public static function round(float $number): float
    {
        $precision = 2;

        if ($number > 1000) {
            $precision = 0;
        }

        if ($number < 1) {
            $precision = 3;
        }

        if ($number < 0.1) {
            $precision = 4;
        }

        return round($number, $precision);
    }

    public static function changeParams(StockEntity $stock): void
    {
        $maxPrice = 0;
        $minPrice = 0;

        $startBeforePrice = 0;
        $monthBeforePrice = 0;
        $weekBeforePrice = 0;

        $ytdPrice = 0;
        $mtdPrice = 0;
        $wtdPrice = 0;

        $dateStart = new DateTime();
        $dateStart->sub(new DateInterval('P1Y'));

        $dateMonth = new DateTime();
        $dateMonth->sub(new DateInterval('P1M'));

        $dateWeek = new DateTime();
        $dateWeek->sub(new DateInterval('P8D'));

        $dateWeek = DateHelper::weekStart();

        $lastHistory = null;
        $prevHistory = null;

        $query = StockHistory::find()
            ->where(['stock_id' => $stock->id])
            ->andWhere(['>=', 'date',  $dateStart->format('Y-m-d')])
            ->andWhere(['not', ['price_close' => null]])
            ->orderBy(['date' => SORT_ASC]);

        /** @var StockHistory $model */
        foreach ($query->all() as $model) {
            if ($maxPrice < $model->price_close) {
                $maxPrice = $model->price_close;
            }

            if ($minPrice === 0 || $minPrice > $model->price_close) {
                $minPrice = $model->price_open;
            }

            if ($startBeforePrice === 0) {
                $startBeforePrice = $model->price_open;
            }

            if ($ytdPrice === 0 && $model->date >= date('Y-01-01')) {
                $ytdPrice = $model->price_open;
            }

            if ($mtdPrice === 0 && $model->date >= date('Y-m-01')) {
                $mtdPrice = $model->price_open;
            }

            if ($wtdPrice === 0 && $model->date >= $dateWeek->format('Y-m-d')) {
                $wtdPrice = $model->price_open;
            }

            if ($monthBeforePrice === 0 && $model->date >= $dateMonth->format('Y-m-d')) {
                $monthBeforePrice = $model->price_open;
            }

            if ($weekBeforePrice === 0 && $model->date >= $dateWeek->format('Y-m-d')) {
                $weekBeforePrice = $model->price_open;
            }

            $prevHistory = $lastHistory;
            $lastHistory = $model;
        }

        $currentPrice = $lastHistory->price_close ?? 0;
        $openPrice = $prevHistory->price_close ?? 0;

        $max = MoexHelper::changePercent($currentPrice, $maxPrice);
        $min = MoexHelper::changePercent($currentPrice, $minPrice);

        $year = MoexHelper::changePercent($currentPrice, $startBeforePrice);
        $month = MoexHelper::changePercent($currentPrice, $monthBeforePrice);
        $week = MoexHelper::changePercent($currentPrice, $weekBeforePrice);
        $day = MoexHelper::changePercent($currentPrice, $openPrice);

        $stock->price_ytd_max = $maxPrice;
        $stock->price_ytd_min = $minPrice;

        $stock->percent_ytd_max = $max;
        $stock->percent_ytd_min = $min;

        $stock->percent_year = $year;
        $stock->percent_month = $month;
        $stock->percent_week = $week;
        $stock->percent_day = $day;

        $stock->percent_ytd = MoexHelper::changePercent($currentPrice, $ytdPrice);
        $stock->percent_mtd = MoexHelper::changePercent($currentPrice, $mtdPrice);
        $stock->percent_wtd = MoexHelper::changePercent($currentPrice, $wtdPrice);

        if ($lastHistory instanceof StockHistory) {
            $stock->price_prev = $lastHistory->price_close;
            $stock->date_prev = $lastHistory->date;
        }

        $stock->save();
    }

    public static function priceForDate(PortfolioStock $portfolioStock, string $date): ?float
    {
        /** @var StockHistory $stockHistory */
        $stockHistory = StockHistory::find()
            ->where(['stock_id' => $portfolioStock->stock_id])
            ->andWhere(['<=', 'date',  $date])
            ->orderBy(['date' => SORT_DESC])
            ->one();

        if ($stockHistory === null) {
            return null;
        }

        if ($portfolioStock->price > $stockHistory->price_close) {
            $priceHistory = $stockHistory->price_high ?: $stockHistory->price_close;
        } else {
            $priceHistory = $stockHistory->price_low ?: $stockHistory->price_close;
        }

        if ($portfolioStock->stock->type === StockEntity::TYPE_BOND) {
            $priceHistory =  $portfolioStock->stock->nominal * $priceHistory / 100;
        }

        return $priceHistory;

    }

    public static function getPercentOrEmpty(float $percent): ?string
    {
        return $percent ? sprintf('%.2f%%', $percent) : null;
    }
}
