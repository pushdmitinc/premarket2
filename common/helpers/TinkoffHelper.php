<?php

declare(strict_types=1);

namespace common\helpers;

use common\components\exception\ValidationModelException;
use common\models\Dividend;
use common\models\PortfolioStock;
use common\models\stock\StockEntity;
use common\models\stock\StockFuture;
use common\models\stock\StockHistory;
use DateInterval;
use DateTime;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;

class TinkoffHelper
{
    public static function queryForMarketData(int $signs = 0, ?int $portfolioId = null): ActiveQuery
    {
        $query = StockEntity::find()->alias('s')
            ->select(['s.id', 's.figi', 's.ticker', 's.price_prev'])
            ->andWhere(['is_upload' => false])
            ->indexBy('figi');

        if (SignsHelper::canSigns($signs)) {
            $query->andWhere(['is_portfolio' => true]);
        }

        if (SignsHelper::canSigns($signs, 1) || $portfolioId !== null) {
            $query->innerJoin(['ps' => PortfolioStock::tableName()], 'ps.stock_id = s.id');

            if ($portfolioId !== null) {
                $query->andWhere(['ps.portfolio_id' => $portfolioId]);
            }

            if (SignsHelper::canSigns($signs, 1)) {
                $query->andWhere(['ps.date_close' => null]);
            }
        }

        if (SignsHelper::canSigns($signs, 2)) {
            $query->leftJoin(['sff' => StockFuture::tableName()], 'sff.future_id = s.id');
            $query->leftJoin(['sfs' => StockFuture::tableName()], 'sfs.stock_id = s.id');

            $query->andWhere(['or',
                ['not', ['sff.future_id' => null]],
                ['not', ['sfs.stock_id' => null]],
            ]);
        }

        return $query;
    }

    public static function afterMarketDataStock(StockEntity $stock, int $signs = 0): bool
    {
        if (SignsHelper::canSigns($signs, 2)) {
            if ($stock->futureStock !== null) {
                $futureStock = $stock->futureStock;
                $percent = $futureStock->percentDiff(2);
                $percentAbs = $percent ? abs($percent) : 0;

                if ($percentAbs > 5) {
                    if ($percentAbs * 0.98 > abs($futureStock->percent_diff)) {
                        $text = $futureStock->future->ticker . ' <> ' . $futureStock->stock->ticker;
                        $text .= ' ' . $futureStock->percent_diff . ' -> ' . $percent;
                        Yii::$app->telegram->queueMessage($text);
                    }
                }

                $futureStock->percent_diff = $percent;
                $futureStock->save();
            }
        }

        return true;
    }

    public static function afterMarketData(int $signs = 0): bool
    {
        if (SignsHelper::canSigns($signs, 2)) {
            Yii::$app->telegram->queueSend();
        }

        return true;
    }

        /**
     * @throws Exception
     */
    public static function dateFromTo(int $year, ?string $interval = null): array
    {
        $dateFrom = $year . '-01-01T00:00:00Z';

        if ($year === (int) date('Y')) {
            $dateTo = date('Y-m-d') . 'T00:00:00Z';
        } else {
            $dateTo = $year . '-12-31T00:00:00Z';
        }

        if ($interval) {
            $dateFrom = (new DateTime($dateTo))
                ->sub(new DateInterval($interval))
                ->format('Y-m-d') . 'T00:00:00Z';
        }

        return [$dateFrom, $dateTo];
    }

    public static function parseQuotation(array $value): float
    {
        return $value['units'] + ($value['nano'] * pow(10, -9));
    }

    /**
     * @throws Exception
     */
    #[ArrayShape(['date' => "string", 'price_open' => "float", 'price_low' => "float", 'price_high' => "float", 'price_close' => "float"])]
    public static function prepareDataHistory(array $data): array
    {
        $date = new DateTime($data['time']);

        return [
            'date' => $date->format('Y-m-d'),
            'price_open' => self::parseQuotation($data['open']),
            'price_low' => self::parseQuotation($data['low']),
            'price_high' => self::parseQuotation($data['high']),
            'price_close' => self::parseQuotation($data['close']),
        ];
    }

    /**
     * @param array $data
     * @param int $type
     *
     * @return void
     *
     * @throws ValidationModelException
     */
    public static function loadStock(array $data, int $type = StockEntity::TYPE_STOCK): void
    {
        $stock = StockEntity::find()
            ->where(['figi' => $data['figi']])
            ->one();

        if ($stock === null) {
            $stock = new StockEntity();
        }

        if ($data['nominal'] ?? null) {
            $data['nominal'] = self::parseQuotation($data['nominal']);
        }

        $data['class_code'] = $data['classCode'];
        $data['real_exchange'] = $data['realExchange'];

        if ($data['minPriceIncrement'] ?? null) {
            $data['min_price_increment'] = self::parseQuotation($data['minPriceIncrement']);
        }

        $data['iso_currency_name'] = $data['isoCurrencyName'] ?? null;
        $data['issue_size'] = $data['issueSize'] ?? null;
        $data['date_ipo'] = $data['dateIpo'] ?? null;

        $stock->load($data, '');

        $stock->type = $type;

        if (!$stock->save()) {
            throw new ValidationModelException($stock);
        }
    }

    /**
     * @param StockEntity $stock
     * @param array $data
     *
     * @return void
     *
     * @throws ValidationModelException
     * @throws Exception
     */
    public static function loadDividend(StockEntity $stock, array $data): void
    {
        $dateT2 = null;
        [$date] = explode('T', $data['recordDate']);
        $dividendNet = self::parseQuotation($data['dividendNet']);

        if (($data['lastBuyDate'] ?? null) !== null) {
            [$dateT2] = explode('T', $data['lastBuyDate']);
        }

        $dividend= Dividend::findOne([
            'stock_id' => $stock->id,
            'date' => $date,
            'dividend' => $dividendNet,
        ]);

        if ($dividend === null) {
            $dividend = new Dividend();
        }

        $dividend->dividend = self::parseQuotation($data['dividendNet']);
        $dividend->stock_price = self::parseQuotation($data['closePrice']);

        $dividend->currency = $data['dividendNet']['currency'];

        $dividend->stock_id = $stock->id;
        $dividend->date = $date;
        $dividend->date_t2 = $dateT2;
        $dividend->period = $data['dividendType'];

        if (!$dividend->save()) {
            throw new ValidationModelException($stock);
        }
    }

    /**
     * @param StockEntity $stock
     * @param array $data
     *
     * @return void
     *
     * @throws ValidationModelException
     * @throws Exception
     */
    public static function loadCoupon(StockEntity $stock, array $data): void
    {
        [$date] = explode('T', $data['payDate']);
        $coupon = self::parseQuotation($data['payOneBond']);

        $dividend= Dividend::findOne([
            'stock_id' => $stock->id,
            'date' => $date,
            'dividend' => $coupon,
        ]);

        if ($dividend === null) {
            $dividend = new Dividend();
        }

        $dividend->stock_id = $stock->id;
        $dividend->dividend = $coupon;
        $dividend->currency = $data['payOneBond']['currency'];
        $dividend->date = $date;
        $dividend->date_t2 = explode('T', $data['fixDate'])[0];

        if (!$dividend->save()) {
            throw new ValidationModelException($stock);
        }
    }

    /**
     * @throws ValidationModelException
     * @throws Exception
     */
    public static function loadHistory(StockEntity $stock, array $data): void
    {
        $date = explode('T', $data['time'])[0];

        $history = StockHistory::find()
            ->where(['stock_id' => $stock->id, 'date' => $date])
            ->one();

        if (!$history instanceof StockHistory) {
            $history = new StockHistory();
        }

        $history->stock_id = $stock->id;
        $history->date = $date;

        $history->price_open = self::parseQuotation($data['open']);
        $history->price_low = self::parseQuotation($data['low']);
        $history->price_high = self::parseQuotation($data['high']);
        $history->price_close = self::parseQuotation($data['close']);

        $history->volume = $data['volume'];

        if (!$history->save()) {
            throw new ValidationModelException($history);
        }
    }

    /**
     * @param array $figiList
     *
     * @return void
     *
     * @throws Exception
     */
    public static function updateStockPrice(array $figiList): void
    {
        $stocks = StockEntity::find()
            ->where(['figi' => $figiList])
            ->indexBy('figi')
            ->all();

        $data = Yii::$app->tinkoff->marketData($figiList);

        foreach ($data as $item) {
            $stock = $stocks[$item['figi']];

            if (($item['time'] ?? null) === null) {
                continue;
            }

            TinkoffHelper::loadMarketData($stock, $item);
        }
    }

    /**
     * @param StockEntity $stock
     * @param array $data
     *
     * @return void
     *
     * @throws Exception
     */
    public static function loadMarketData(StockEntity $stock, array $data): void
    {
        $date = (new DateTime($data['time']))->format('Y-m-d');
        $price = self::parseQuotation($data['price']);

        $history = StockHistory::find()
            ->where(['stock_id' => $stock->id, 'date' => $date])
            ->one();

        if (!$history instanceof StockHistory) {
            $history = new StockHistory();
        }

        $history->stock_id = $stock->id;
        $history->date = $date;

        if ($history->price_open === null) {
            $history->price_open = $price;
        }

        if (!$history->price_low === null || $history->price_low > $price) {
            $history->price_low = $price;
        }

        if ($history->price_high === null || $history->price_high < $price) {
            $history->price_high = $price;
        }

        $history->price_close = $price;

        if (!$history->save()) {
            throw new ValidationModelException($history);
        }

        $stock->date_prev = $date;
        $stock->price_prev = $price;

        if (!$stock->save(false)) {
            throw new ValidationModelException($stock);
        }
    }

    /**
     * @return void
     */
    public static function updateStockForUpload(): void
    {
        StockEntity::updateAll(
            ['is_upload' => false],
            ['>=', 'date_prev', new Expression("NOW() - INTERVAL '10 DAY'")]
        );
    }
}
