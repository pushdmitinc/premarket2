<?php
namespace common\helpers;

use Yii;

class AdminLteHelper extends \dmstr\helpers\AdminLteHelper
{
    /**
     * It allows you to get the name of the css class.
     * You can add the appropriate class to the body tag for dynamic change the template's appearance.
     * Note: Use this fucntion only if you override the skin through configuration. 
     * Otherwise you will not get the correct css class of body.
     * 
     * @return string
     */
    public static function skinClass()
    {
        /** @var \dmstr\web\AdminLteAsset $bundle */
        $bundle = Yii::$app->assetManager->getBundle('dmstr\web\AdminLteAsset');

        return $bundle->skin;
    }

    /**
     * @return string
     */
    public static function directoryAsset(): string
    {
        return Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    }

    /**
     * @return string
     */
    public static function filePath($file): string
    {
        return self::directoryAsset() . $file;
    }

    /**
     * @param float $percent Процент отклонения
     * @param int $deviation Допустимое отклонение
     *
     * @return string
     */
    public static function colorDeviation(float $percent, int $deviation = 20): string
    {
        $color = $percent > 0 ? 'aqua' : 'yellow';

        if ($percent > $deviation) {
            $color = 'green';
        }

        if ($percent < $deviation * -1) {
            $color = 'red';
        }

        return $color;
    }

    /**
     * @param float $percent Процент отклонения
     * @param int $deviation Допустимое отклонение
     *
     * @return string
     */
    public static function labelDeviation(float $percent, int $deviation = 20): string
    {
        $color = $percent > 0 ? 'info' : 'warning';

        if ($percent > $deviation) {
            $color = 'success';
        }

        if ($percent < $deviation * -1) {
            $color = 'danger';
        }

        return $color;
    }
}
