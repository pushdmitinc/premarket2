<?php

namespace common\helpers;

class CalendarHelper
{
    const TYPE_DANGER = 'danger';

    public static $styleClassList = [
        '' => 'Нет стиля',
        'primary' => 'Главный',
        'secondary' => 'Вторичный',
        'success' => 'Успех',
        'danger' => 'Опасность',
        'warning' => 'Предупреждение',
        'info' => 'Инфо',
        'light' => 'Светлый',
        'dark' => 'Темный',
    ];

    public static $statusList = [
        1 => 'Нет стиля',
        2 => 'Зачеркнутый',
        3 => 'Подчеркнутый',
        4 => 'Мигающий',
        5 => 'Курсив',
    ];

    public static $statusAliasList = [
        1 => 'none',
        2 => 'line-through',
        3 => 'underline',
        4 => 'blink',
        5 => 'italic',
    ];

    public static function timeList(): array
    {
        $timeList = ['' => 'Весь день'];

        for ($i = 0; $i < 24; ++$i) {
            for ($j = 0; $j < 60; $j += 15) {
                $time = ($i < 10 ? '0' . $i : $i) . ':' . ($j < 10 ? '0' . $j : $j);
                $timeList[$time . ':00'] = $time;
            }
        }

        return $timeList;
    }

    public static function repeatSelect($count = 10)
    {
        $result = [1 => 'Без повторений'];

        for ($i = 2; $i <= $count; ++$i) {
            $result[$i] = $i;
        }

        return $result;
    }

    public static function getWeekend(\DateTime $date): ?string
    {
        $holidays = SettingsHelper::getSetting('CALENDAR_DATE_HOLIDAYS', []);

        if (isset($holidays[$date->format('m-d')])) {
            return $holidays[$date->format('m-d')];
        }

        if (in_array($date->format('w'), [0, 6])) {
            return 'Выходной день';
        }

        return null;
    }

    public static function getIntervalText(\DateTime $date): ?string
    {
        $current = new \DateTime();
        $interval = (int) $current->diff($date, true)->format('%a');

        $pattern = '{n,plural,=1{один день} one{# день} few{# дня} many{# дней} other{# дней}}';

        if ($current > $date) {
            $format = \Yii::t('app', $pattern . ' назад', ['n' => $interval]);
        } else {
            $format = \Yii::t('app', 'через ' . $pattern, ['n' => $interval]);
        }

        $format = strtr($format, [
            'один день назад' => 'вчера',
            'через 0 дней' => 'завтра',
        ]);

        return $format;
    }

    public static function getStyleList(): array
    {
        return self::$styleClassList;
    }

    public static function getStatusAlias($statusId): ?string
    {
        if (isset(self::$statusAliasList[$statusId])) {
            return self::$statusAliasList[$statusId];
        }

        return null;
    }
}
