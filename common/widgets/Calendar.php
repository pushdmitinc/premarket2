<?php

namespace common\widgets;

use common\helpers\CalendarHelper;
use common\helpers\DateHelper;
use common\models\interfaces\EventInterface;
use common\widgets\Modal\ModalWidget;
use DateTime;
use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;

class Calendar extends Widget
{
    /**
     * @var string|DateTime
     */
    public $dateStart;

    /**
     * @var string|null
     */
    public $dateMinStart;

    /**
     * @var string|null
     */
    public $dateMaxStart;

    /**
     * @var int
     */
    public $countMonth = 12;

    /**
     * @var bool
     */
    public $viewMonthName = true;

    /**
     * @var string
     */
    public $classCol = 'col-md-3';

    /**
     * @var string
     */
    public $classCurrent = 'calendar-day-current';

    /**
     * @var string
     */
    public $classSelect = 'calendar-day-select';

    /**
     * @var string
     */
    public $currentDate;

    /**
     * @var bool
     */
    public $modalHidden = 1;

    /**
     * @var string
     */
    public $classAll = 'danger';

    /**
     * @var bool
     */
    public $urlIsAjax;

    /**
     * @var bool
     */
    public $urlIsEmpty = false;

    /**
     * @var string|array
     */
    public $url;

    /**
     * @var bool
     */
    public $popover = true;

    /**
     * @var string
     */
    public $popoverTitle; // 'События <small>{date}</small>';

    /**
     * @var bool
     */
    public $popoverDiff;

    /**
     * @var EventInterface
     */
    public $searchModel;

    /**
     * @var array
     */
    public $searchParams = [
        'signParams' => [0],
    ];

    /**
     * @var array
     */
    public $data = [];

    /**
     * @var array
     */
    public $meta = [];

    /**
     * @var bool
     */
    public $isMetaData = false;

    /**
     * @var bool
     */
    public $isMonthOne = false;

    /**
     * @inheritdoc
     *
     * @throws \Exception
     */
    public function init(): void
    {
        if (!$this->dateStart) {
            $this->dateStart = new DateTime(date('Y-m-01'));

            if ($this->dateMinStart && $this->dateStart->format('Y-m-d') < $this->dateMinStart) {
                $this->dateStart = new DateTime($this->dateMinStart);
            }

            if ($this->dateMaxStart && $this->dateStart->format('Y-m-d') > $this->dateMaxStart) {
                $this->dateStart = new DateTime($this->dateMaxStart);
            }
        }

        if (!$this->dateStart instanceof DateTime) {
            $this->dateStart = new DateTime($this->dateStart);
        }

        if ($this->searchModel) {
            $date = clone $this->dateStart;
            $date->add(new \DateInterval('P' . $this->countMonth . 'M'));
            $date->add(new \DateInterval('P1D'));

            $this->data = $this->searchModel->dataEvent([
                'dateStart' => $this->dateStart->format('Y-m-d'),
                'dateFinish' => $date->format('Y-m-d'),
                'isMetaData' => $this->isMetaData,
            ] + $this->searchParams);

            if (isset($this->data['meta'])) {
                $this->meta = $this->data['meta'];
                unset($this->data['meta']);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function run(): string
    {
        $view = $this->getView();
        $view->registerJs('$(\'[data-toggle="popover"]\').popover({html:true,trigger:\'hover\'})');

        $year = $this->dateStart->format('Y');
        $month = $this->dateStart->format('m');

        $calendar = '';

        if ($this->isMonthOne) {
            $calendar .= $this->renderMonthName($month, $year);
            $calendar .= $this->renderMonth($month, $year);

            return $calendar;
        }

        for ($i = 0; $i < $this->countMonth; $i++) {
            $monthNumber = ($month % 12) ?: 12;
            $class = $this->viewMonthName ? ' month-' . $monthNumber : '';

            $calendar .= '<div class="' . $this->classCol . ' ' . $class . '">';

            if ($this->viewMonthName) {
                $calendar .= $this->renderMonthName($month, $year);
            }

            $calendar .= $this->renderMonth($month++, $year);
            $calendar .= '</div>';
        }

        if ($this->isMetaData && $this->meta) {
            $calendar .= $this->getMeta();
        }

        return $calendar;
    }

    public function renderMonthName($month, $year): string
    {
        $monthNumber = ($month % 12) ?: 12;

        $calendar = '<div class="col-md-12 calendar-month">';
        $calendar .= DateHelper::$monthName[$monthNumber];
        $calendar .= ' ' . ($year + ($month > 12 ? 1 : 0));
        $calendar .= '</div>';

        return $calendar;
    }

    public function renderMonth(int $month, int $year): string
    {
        /* Начало таблицы */
        $calendar = '<table cellpadding="0" cellspacing="0" class="calendar">';
        $calendar .= '<tr class="calendar-row"><td class="calendar-day-head">'
            . implode('</td><td class="calendar-day-head">', DateHelper::$dayNameSort)
            . '</td></tr>';

        $thisWeek = 1;
        $dayCounter = 0;
        $runningDay = DateHelper::runningDay($month, $year);
        $daysInMonth = date('t', mktime(0, 0, 0, $month, 1, $year));

        /* первая строка календаря */
        $calendar .= '<tr class="calendar-row">';

        /* вывод пустых ячеек в сетке календаря */
        for ($x = 0; $x < $runningDay; $x++) {
            $calendar .= '<td class="calendar-day-np"></td>';
            $thisWeek++;
        }

        /* дошли до чисел, будем их писать в первую строку */
        for ($listDay = 1; $listDay <= $daysInMonth; $listDay++) {
            $date = date('Y-m-d', mktime(0, 0, 0, $month, $listDay, $year));
            $date = new DateTime($date);

            $popover = $this->getPopover($date);

            $calendar .= '<td class="calendar-day">';
            $calendar .= '<div class="' . $this->classDay($date) . '" ' . $popover . '>';
            $calendar .= $this->getContentDay($date);
            $calendar .= '</div>';
            $calendar .= '</td>';

            if ($runningDay == 6){
                $calendar .= '</tr>';

                if (($dayCounter + 1) != $daysInMonth){
                    $calendar .= '<tr class="calendar-row">';
                }

                $runningDay = -1;
                $thisWeek = 0;
            }

            $thisWeek++;
            $runningDay++;
            $dayCounter++;
        }

        /* Выводим пустые ячейки в конце последней недели */
        if ($thisWeek < 8) {
            for ($x = 1; $x <= (8 - $thisWeek); $x++) {
                $calendar .= '<td class="calendar-day-np"></td>';
            }
        }

        $calendar .= '</tr>';
        $calendar .= '</table>';

        return $calendar;
    }

    private function dataDay(DateTime $date, string $param): ?string
    {
        $day = $date->format('Y-m-d');

        if (isset($this->data[$day]) && isset($this->data[$day][$param])) {
            if (is_array($this->data[$day][$param])) {
                return implode(',', $this->data[$day][$param]);
            }

            return $this->data[$day][$param];
        }

        $dayM = $date->format('m-d');

        if (isset($this->data[$dayM]) && isset($this->data[$dayM][$param])) {
            if (is_array($this->data[$dayM][$param])) {
                return implode(',', $this->data[$dayM][$param]);
            }

            return $this->data[$dayM][$param];
        }

        return null;
    }

    private function classDay(DateTime $date): string
    {
        $class = 'day-number';
        $day = $date->format('Y-m-d');
        $dayMD = $date->format('m-d');

        if (isset($this->data[$day]) || isset($this->data[$dayMD])) {
            if (isset($this->data[$day]['class']) && $this->data[$day]['class']) {
                $class .= ' badge-' . $this->data[$day]['class'];
            } else {
                if (isset($this->data[$dayMD]['class']) && $this->data[$dayMD]['class']) {
                    $class .= ' badge-' . $this->data[$dayMD]['class'];
                } else {
                    $class .= ' badge-' . $this->classAll;
                }
            }
        }

        if ($this->isCurrentDate($day)) {
            $class .= ' ' . $this->classCurrent;
        }

        if ($this->isSelectDate($day)) {
            $class .= ' ' . $this->classSelect;
        }

        return $class;
    }

    public function isCurrentDate(string $day): bool
    {
        return $this->classCurrent && $day === date('Y-m-d');
    }

    public function isSelectDate(string $day): bool
    {
        return $this->currentDate && $this->classSelect && $day === $this->currentDate;
    }

    private function getContentDay(DateTime $date): string
    {
        $day = $date->format('d');
        $dayYMD = $date->format('Y-m-d');
        $dayMD = $date->format('m-d');

        if ($this->url) {
            if ($this->urlIsEmpty || (isset($this->data[$dayYMD]) || isset($this->data[$dayMD]))) {
                $url = Url::to($this->url + ['date' => $dayYMD]);

                if ($this->urlIsAjax) {
                    return ModalWidget::widget([
                        'content' => $day,
                        'url' => $url,
                        'options' => ['data' => [
                            'hidden' => $this->modalHidden,
                            'date' => $dayYMD,
                        ]],
                    ]);
                }

                return \yii\bootstrap4\Html::a($day, $url);
            }
        }

        return $day;
    }

    private function getPopover(DateTime $date): string
    {
        if (!$this->popover) {
            return '';
        }

        $popover = '';
        $popoverContent = $this->dataDay($date, 'popover');

        if ($popoverContent) {
            $popover = '';

            if ($this->popoverTitle) {
                $popover .= ' title="' . $this->popoverTitle . '"';
            }

            if ($this->popoverDiff && !$this->isCurrentDate($date->format('Y-m-d'))) {
                $popoverContent .= '<hr>' . CalendarHelper::getIntervalText($date);
            }

            $popover .= ' data-toggle="popover"';
            $popover .= ' data-content="' . Html::encode($popoverContent)  . '"';
            $popover = strtr($popover, ['{date}' => \Yii::$app->formatter->asDate($date)]);
        }

        return $popover;
    }

    private function getMeta(): string
    {
        $content = '';
        $content .= '<div class="calendar-mata">';

        if (isset($this->meta['items'])) {
            $content .= '<div class="col-md-12">';
            foreach ($this->meta['items'] as $key => $value) {
                $contentTag = $value['name'] . ' - ' . $value['count'];
                if (isset($value['sum']) && $value['sum']) {
                    $contentTag .= ' (сумма: ' . $value['sum'] . ')';
                }
                $content .= Html::tag('span', $contentTag, ['class' => 'badge badge-' . $key]);
            }
            $content .= '</div>';
        }

        if (isset($this->meta['total'])) {
            $content .= '<div class="col-md-12">';
            foreach ($this->meta['total'] as $item) {
                $contentTag = $item['name'] . ' - ' . $item['value'];
                $content .= Html::tag('span', $contentTag, ['class' => 'badge badge-light']);
            }
            $content .= '</div>';
        }

        if (isset($this->meta['months'])) {
            $content .= '<div class="col-md-12">';
            foreach ($this->meta['months'] as $key => $months) {
                $content .= Html::tag('span', $key . ' - ', ['class' => 'badge badge-light']);

                foreach ($months as $key => $item) {
                    $name = $item['count'];
                    if (isset($item['name'])) $name = $item['name'] . ' - ' . $item['count'];
                    if ($item['value']) $name .= ' (' . $item['value'] . ')';
                    $content .= Html::tag('span', $name, ['class' => 'badge badge-' . $key]);
                }

                $content .= '<br>';
            }
            $content .= '</div>';
        }

        $content .= '</div>';

        return $content;
    }
}
