<?php

declare(strict_types=1);

namespace common\widgets\Modal;

use yii\helpers\Url;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class ModalWidget
 *
 * Виджет для вызова модального окна.
 */
class ModalWidget extends Widget
{
    public const MODAL_ID_DEFAULT = 'modal-remote';

    /**
     * @var string Ссылка
     */
    public $url;

    /**
     * @var string Селектор модального окна
     */
    public $modal;

    /**
     * @var string Текст кнопки
     */
    public $content;

    /**
     * @var string Класс
     */
    public $cssClass = '';

    /**
     * @var string Класс
     */
    public $sendClass = 'modal-remote-view';

    /**
     * @var array
     */
    public $options = [];

    /**
     * @inheritdoc
     */
    public function init(): void
    {
        if (!$this->modal) {
            $this->modal = self::MODAL_ID_DEFAULT;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function run(): string
    {
        $view = $this->getView();
        ModalAsset::register($view);

        return Html::a($this->content, Url::to($this->url), [
            'class' =>  $this->sendClass . ' ' . $this->cssClass,
            'data-modal' => $this->modal,
        ] + $this->options);
    }
}
