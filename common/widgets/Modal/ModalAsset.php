<?php

declare(strict_types=1);

namespace common\widgets\Modal;

use yii\web\AssetBundle;

/**
 * Class ModalAsset
 *
 * Ассет виджета модальных окон.
 */
class ModalAsset extends AssetBundle
{
    /**
     * {@inheritDoc}
     */
    public $publishOptions = [
        'forceCopy' => true
    ];

    /**
     * {@inheritDoc}
     */
    public $sourcePath = __DIR__ . '/assets';

    /**
     * {@inheritDoc}
     */
    public $js = [
        'modal.js',
    ];

    /**
     * {@inheritDoc}
     */
    public $css = [];

    /**
     * {@inheritDoc}
     */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
