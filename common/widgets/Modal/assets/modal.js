$(function() {
    $('[data-toggle="tooltip"]').tooltip();

    $(document).on('click', '.modal-remote-view', function (event) {
        event.preventDefault();

        let modal = modalHtml($(this).data('modal'));
        let dataType = $(this).data('type') || 'html';

        $.ajax({
            url: $(this).attr('href'),
            dataType: dataType,
            type: $(this).data('method') || 'get',
            data: {modal: $(this).data('modal')},
            success: function (response) {
                if (dataType === 'html') {
                    modal.html(response);
                    modal.modal();
                }
                if (dataType === 'json') {
                    actionRemote(response);
                }
            },
            error: function (response) {}
        });
    });

    $(document).on('click', '.modal-send-form', function (event) {
        event.preventDefault();

        let modal = modalHtml($(this).data('modal'));
        let form = $(this).closest('.modal-content').find('form');

        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                if (typeof response === 'object') {
                    actionRemote(response);
                } else {
                    modal.html(response);
                    modal.modal();
                }
            },
            error: function (response) {}
        });
    });

    function modalHtml(selector) {
        let modal = $('#' + selector);

        if (modal.length === 0) {
            modal = $('<div id="' + selector + '" class="modal" tabindex="" role="dialog" aria-hidden="true"></div>');
        }

        $('.modal').modal('hide');

        return modal;
    }

    function actionRemote(response){
        var url = location.href;
        var blockBuild = undefined;

        if(response.url !== undefined && response.url){
            url = response.url;
        }

        if (response.pageReload !== undefined && response.pageReload){
            console.log(response.pageReload);
            if(response.pageReload === true){
                location.reload();
            } else {
                location.href = response.pageReload;
            }
        }

        if (response.forceReload !== undefined && response.forceReload)
        {
            if(response.replace === undefined){
                response.replace = false;
            }

            if (response.forceReload === true) {
                response.forceReload = '#pjax-index';
            }

            if ($(response.forceReload).length > 0) {
                $.pjax.reload({
                    container:response.forceReload, url:url, replace:response.replace,timeout:1500
                });

                blockBuild = response.forceReload;
            }
        }

        // hideBlock
        if (response.hideBlock !== undefined && response.hideBlock) {
            $(response.hideBlock).slideUp(300, function(){});
        }

        // showBlock
        if (response.showBlock !== undefined && response.showBlock) {
            $(response.showBlock).slideDown(300, function(){
                $(response.showBlock).show();
            });
        }

        // deleteBlock
        if (response.deleteBlock !== undefined && response.deleteBlock) {
            $(response.deleteBlock).attr('id', '').slideUp(300, function(){
                $(this).remove();
            });
        }

        // reloadBlock
        if (response.reloadBlock !== undefined && response.reloadBlock) {
            response.reloadBlock.forEach(function(item) {
                $(item.selector).html(item.content);
                blockBuild = item.selector;
            });
        }

        // appendTo
        if (response.appendTo !== undefined && response.appendTo) {
            if(response.content !== undefined){
                $(response.content).hide().appendTo(response.appendTo).slideDown(300, function(){
                    $(response.appendTo).resize();
                });
            }
        }

        // prependTo
        if (response.prependTo !== undefined && response.prependTo) {
            if (response.content !== undefined){
                var content = $(response.content).hide();
                content.prependTo(response.prependTo).slideDown(300, function(){});
            }
        }

        // insertBefore
        if (response.insertBefore !== undefined && response.insertBefore) {
            if (response.content !== undefined){
                $(response.content).hide().insertBefore(response.insertBefore).slideDown(300, function(){});
            }
        }

        // insertAfter
        if (response.insertAfter !== undefined && response.insertAfter) {
            if (response.content !== undefined){
                var content = $(response.content).hide();
                $(response.insertAfter).after(content);
                content.slideDown(300, function(){})
            }
        }

        if (response.inputSetValue !== undefined) {
            $(response.inputSetValue).val(response.value).change();
        }

        if(response.click !== undefined){
            $(response.click).click();
        }

        if(response.loading !== undefined){
            $('body').append('<iframe src="' + response.loading + '" class="hiddenFrame"></iframe>');
        }

        if(response.clickTimeout !== undefined){
            setTimeout(function () {
                $(response.clickTimeout.selector).click();
            }, response.clickTimeout.delay)
        }

        // clear form
        if (response.clearForm !== undefined) {
            var form = $(response.clearForm);
            form.find(':input').val('');
        }

        if (undefined !== blockBuild) {
            setInterval(function () {
                $(blockBuild).find('[data-toggle=tooltip]').tooltip();
            }, 1000);
        }
    }
});
