<?php

return [
    [
        'name' => 'Currency 1',
        'code' => 'CR1',
        'rate' => 11,
        'date' => date('Y-m-d'),
    ],
    [
        'name' => 'Currency 3',
        'code' => 'CR2',
        'rate' => 11,
        'date' => date('Y-m-d'),
    ],
];
