<?php

namespace frontend\tests\models;

use Codeception\Test\Unit;
use common\fixtures\CurrencyFixture;
use common\models\Currency;

class CurrencyTest extends Unit
{
    /**
     * @var \frontend\tests\UnitTester
     */
    protected $tester;
    
    protected function _before(): void
    {
        $this->tester->haveFixtures([
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php',
            ],
        ]);
    }

    public function testCreate(): void
    {
        $model = new Currency();

        $model->name = 'Currency 3';
        $model->code = 'CR3';
        $model->rate = 13;
        $model->date = date('Y-m-d');

        expect_that($model->save());
    }

    public function testCreateError(): void
    {
        $model = new Currency();

        expect_not($model->validate());
        expect_not($model->save());
    }

    public function testUpdate(): void
    {
        $model = Currency::findOne(['code' => 'CR2']);

        $model->rate = 14;
        $model->date = date('Y-m-d');

        expect_that($model->validate());
        expect_that($model->save());
    }

    public function testDelete(): void
    {
        $model = Currency::findOne(['code' => 'CR1']);

        expect_that($model !==  null);
        expect_that($model->delete());
    }
}