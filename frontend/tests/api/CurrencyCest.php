<?php

namespace frontend\tests\api;

use Codeception\Util\HttpCode;
use common\fixtures\CurrencyFixture;
use frontend\tests\FunctionalTester;

class CurrencyCest
{
    public function _fixtures(): array
    {
        return [
            'currency' => [
                'class' => CurrencyFixture::class,
                'dataFile' => codecept_data_dir() . 'currency.php',
            ],
        ];
    }

    public function checkCurrencies(FunctionalTester $I): void
    {
        $I->sendAjaxGetRequest('/api/v1/currency');
        $I->seeResponseCodeIs(HttpCode::OK);
    }

    public function checkCurrency(FunctionalTester $I): void
    {
        $I->amOnPage('/api/v1/currency/1');
        $I->see('rate');
    }

    public function checkNotCurrency(FunctionalTester $I): void
    {
        $I->amOnPage('/api/v1/currency/99999');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seePageNotFound();
    }

    public function checkNotUpdate(FunctionalTester $I): void
    {
        $I->amOnPage('/api/v1/currency/update?id=1');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
        $I->seePageNotFound();
    }
}
