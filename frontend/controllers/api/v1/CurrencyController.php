<?php

declare(strict_types=1);

namespace frontend\controllers\api\v1;

use common\models\Currency;
use common\models\CurrencySearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class CurrencyController
 * @package frontend\controllers\api\v1
 */
class CurrencyController extends ActiveController
{
    /**
     * {@inheritdoc}
     */
    public $modelClass = 'common\models\Currency';

    /**
     * {@inheritdoc}
     */
    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * {@inheritdoc}
     */
    public function actions(): array
    {
        $actions = parent::actions();

        unset($actions['delete'], $actions['create'], $actions['update']);

        return $actions;
    }

    /**
     * {@inheritdoc}
     */
    protected function verbs(): array
    {
        $verbs = parent::verbs();

        $verbs['currency'] = ['GET', 'HEAD'];

        return $verbs;
    }

    /**
     * Пример реализации через DataProvider
     *
     * @return ActiveDataProvider
     */
    public function actionCurrencies(): ActiveDataProvider
    {
        $searchModel = new CurrencySearch();
        return $searchModel->search(Yii::$app->request->queryParams);
    }

    /**
     * @param int $id
     *
     * @return Currency
     * @throws NotFoundHttpException
     */
    public function actionCurrency(int $id): Currency
    {
        return $this->findModel($id);
    }

    protected function findModel(int $id): Currency
    {
        $model = Currency::findOne($id);

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Валюта не найдена.');
        }
    }
}
