<?php

namespace backend\controllers;

use aki\telegram\base\Command;
use Yii;
use common\models\Portfolio;
use yii\web\Controller;
use yii\filters\VerbFilter;

/**
 * TelegramController
 */
class TelegramController extends Controller
{
    public ?Portfolio $portfolio = null;

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex(): string
    {
        $telegram = Yii::$app->telegram;

//        var_dump($telegram->setWebhook()); die;

//        $telegram->sendMessage([
//            'chat_id' => '@PreMarketChannel',
//            'text' => 'test',
//        ]);
//
//        $telegram->sendMessage([
//            'chat_id' => '796199071',
//            'text' => 'test',
//        ]);

        Command::run("/start", function($telegram){
            $result = $telegram->sendMessage([
                'chat_id' => $telegram->input->message->chat->id,
                "text" => "hello"
            ]);
        });
    }
}
