<?php

namespace backend\controllers;

use common\components\AccessControlCalendar;
use common\models\Calendar;
use common\models\CalendarSubscribe;
use common\models\CalendarUser;
use common\models\User;
use Yii;
use common\models\CalendarEvent;
use common\models\search\CalendarEventSearch;
use common\components\BaseCalendarController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CalendarEventController
 */
class CalendarEventController extends BaseCalendarController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlCalendar::class,
                'calendar' => $this->calendar,
                'calendarEvent' => $this->calendarEvent,
                'rules' => [
                    [
                        'actions' => ['view', 'subscribe'],
                        'allow' => true,
                        'roles' => [CalendarUser::SING_VIEW],
                    ],
                    [
                        'actions' => ['update', 'copy',  'clear', 'create', 'delete'],
                        'allow' => true,
                        'roles' => [CalendarUser::SING_UPDATE],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeAction($action)
    {
        if(Yii::$app->request->get('id')){
            $this->calendarEvent = $this->findEventModel(Yii::$app->request->get('id'));
            $this->calendar = $this->calendarEvent->calendar;
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all CalendarEvent models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CalendarEventSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CalendarEvent model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionCreate()
    {
        $model = new CalendarEvent();

        $model->calendar_id = Yii::$app->request->get('calendar_id');
        $model->date = Yii::$app->request->get('date');
        $model->user_id = Yii::$app->user->getId();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->dateTo && $model->interval) {
                $model->createChild();
            }

            if ($model->repeat) {
                for ($i = 1; $i < $model->repeat; ++$i) {
                    $child = new CalendarEvent();
                    $child->setAttributes($model->attributes);
                    $child->save();
                }
            }

            $calendar = $this->findCalendarModel($model->calendar_id);
            $calendar->updated_at = date('Y-m-d H:i:s');
            $calendar->save();

            if (Yii::$app->request->isAjax) {
                $calendar = new CalendarController($this->id, $this->module);
                return $calendar->actionEvent($model->calendar_id, $model->date);
            }

            return $this->redirect(['/calendar/view', 'id' => $model->calendar_id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create', [
                'model' => $model,
            ]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CalendarEvent model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if ($model->dateTo && $model->interval) {
                $model->createChild();
            }

            if (Yii::$app->request->isAjax) {
                $calendar = new CalendarController($this->id, $this->module);
                return $calendar->actionEvent($model->calendar_id, $model->date);
            }

            return $this->redirect(['/calendar/view', 'id' => $model->calendar_id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $calendar_id
     * @param string $date
     * @param string $dateTo
     */
    public function actionCopy(int $calendar_id, string $date, string $dateTo = null)
    {
        $model = new CalendarEvent();
        $model->calendar_id = $calendar_id;
        $model->date = $date;
        $model->dateTo = $dateTo;

        $model->load(Yii::$app->request->post());

        if ($model->dateTo) {
            $events = CalendarEvent::findAll([
                'calendar_id' => $model->calendar_id,
                'date' => $model->date,
            ]);

            foreach ($events as $event) {
                $clone = ((int) $model->type === 1) ? $event : new CalendarEvent();
                $clone->load($event->attributes, '');
                $clone->date = $model->dateTo;
                $clone->save();
            }

            $calendar = $this->findCalendarModel($calendar_id);
            $calendar->updated_at = date('Y-m-d H:i:s');
            $calendar->save();

            if (Yii::$app->request->isAjax) {
                $calendar = new CalendarController($this->id, $this->module);
                return $calendar->actionEvent($calendar_id, $model->dateTo);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('copy', ['model' => $model]);
        }

        return $this->render('copy', ['model' => $model]);
    }

    /**
     * @param integer $calendar_id
     * @param string $date
     */
    public function actionClear($calendar_id, $date)
    {
        CalendarEvent::deleteAll([
            'calendar_id' => $calendar_id,
            'date' => $date,
        ]);

        if (Yii::$app->request->isAjax) {
            $calendar = new CalendarController($this->id, $this->module);
            return $calendar->actionEvent($calendar_id, $date);
        }

        return $this->redirect(['/calendar/view', 'id' => $calendar_id]);
    }

    /**
     * @param integer $id
     * @param string $token
     */
    public function actionSubscribe($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $event = $this->findEventModel($id);

        $content = '<i class="fas fa-bullhorn text-success pull-right"></i>';

        $subscribe = CalendarSubscribe::findOne([
            'event_id' => $id,
            'user_id' => Yii::$app->getUser()->getId(),
        ]);

        if ($subscribe === null) {
            $subscribe = new CalendarSubscribe();

            $subscribe->event_id = $event->id;
            $subscribe->user_id = Yii::$app->getUser()->getId();
            $subscribe->calendar_id = $event->calendar_id;
            $subscribe->date = $this->calendarEvent->dateTime();

            $subscribe->save();

            $content = '<i class="fas fa-deaf text-secondary pull-right"></i>';
        } else {
            $subscribe->delete();
        }

        return ['reloadBlock' => [['selector' => '[data-id=' . $id . ']', 'content' => $content]]];
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if ($model->calendar->canUser(CalendarUser::SING_UPDATE)) {
            CalendarEvent::deleteAll(['parent_id' => $model->id]);
            $model->delete();
        }

        if (Yii::$app->request->isAjax) {
            $calendar = new CalendarController($this->id, $this->module);
            return $calendar->actionEvent($model->calendar_id, $model->date);
        }

        return $this->redirect(['/calendar/view', 'id' => $model->calendar_id]);
    }

    /**
     * @param integer $id
     * @return CalendarEvent the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CalendarEvent::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
