<?php

namespace backend\controllers;

use common\components\AccessControlCalendar;
use common\helpers\SettingsHelper;
use common\models\CalendarUser;
use common\models\search\CalendarEventSearch;
use common\models\Calendar;
use common\models\search\CalendarSearch;
use common\components\BaseCalendarController;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CalendarController implements the CRUD actions for Calendar model.
 */
class CalendarController extends BaseCalendarController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControlCalendar::class,
                'calendar' => $this->calendar,
                'rules' => [
                    [
                        'actions' => ['view', 'event', 'month'],
                        'allow' => true,
                        'roles' => [CalendarUser::SING_VIEW],
                    ],
                    [
                        'actions' => ['update', 'join', 'user'],
                        'allow' => true,
                        'roles' => [CalendarUser::SING_UPDATE],
                    ],
                    [
                        'actions' => ['create', 'alias'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @param $action
     *
     * @return bool
     *
     * @throws NotFoundHttpException
     */
    public function beforeAction($action): bool
    {
        if(Yii::$app->request->get('id')){
            $this->calendar = $this->findModel(Yii::$app->request->get('id'));
        }

        if(Yii::$app->request->get('alias')){
            $this->calendar = $this->findByAlias(Yii::$app->request->get('alias'));
        }

        return parent::beforeAction($action);
    }

    /**
     * Lists all Calendar models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CalendarSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findCalendarModel($id);

        $searchModel = new CalendarEventSearch();
        $searchModel->load(Yii::$app->request->get());
        $searchModel->calendar_id = $model->id;

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUser($id)
    {
        $model = $this->findCalendarModel($id);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('user', ['model' => $model]);
        }

        return $this->render('user', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @param integer $model_id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException
     */
    public function actionJoin($id, $model_id = null)
    {
        $calendar = $this->findCalendarModel($id);

        if ($model_id) {
            $model = CalendarUser::findOne($model_id);
        }

        if (!isset($model) || !$model instanceof CalendarUser) {
            $model = new CalendarUser();
        }

        $model->calendar_id = $calendar->id;

        if (Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->post());
            $model->save();

            if (Yii::$app->request->isAjax) {
                return $this->renderAjax('user', ['model' => $calendar]);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('join', ['model' => $model]);
        }

        return $this->render('join', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $calendar_id
     * @param string $date
     *
     * @return mixed
     */
    public function actionEvent($calendar_id, $date)
    {
        $model = $this->findModel($calendar_id);

        $searchModel = new CalendarEventSearch();
        $searchModel->calendar_id = $model->id;

        $date = new \DateTime($date);

        $searchModel->date = $date->format('Y-m-d');
        $searchModel->startYear = $date->format('Y');
        $searchModel->startMonth = $date->format('m');

        $dataProvider = $searchModel->search([]);
        $dataProvider->pagination = false;

        if (Yii::$app->request->isAjax) {
            if (Yii::$app->request->get('type') === 'month') {
                Yii::$app->response->format = Response::FORMAT_JSON;

                $content = $this->renderPartial('view-month', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                ]);

                return ['reloadBlock' => [
                    [
                        'selector' => '.month-' . $date->format('n'),
                        'content' => $content,
                    ],
                ]];
            }

            return $this->renderAjax('/calendar-event/index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('/calendar-event/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Calendar();
        $model->user_id = Yii::$app->getUser()->getId();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->remove(SettingsHelper::KEY_EXISTS_CALENDAR);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findCalendarModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->session->remove(SettingsHelper::KEY_EXISTS_CALENDAR);

            return $this->redirect(['view', 'id' => $model->id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel(int $id): Calendar
    {
        return $this->findCalendarModel($id);
    }

    protected function findByAlias(string $alias): Calendar
    {
        if ($this->calendar instanceof Calendar) {
            if ($this->calendar->alias === $alias) {
                return $this->calendar;
            }
        }

        $this->calendar = Calendar::findOne(['alias' => $alias]);

        if ($this->calendar !== null) {
            return $this->calendar;
        }

        throw new NotFoundHttpException('Календарь не найден');
    }
}
