<?php

namespace backend\controllers;

use common\helpers\ArrayHelper;
use common\models\stock\StockEntity;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SearchController implements the CRUD actions for Portfolio model.
 */
class SearchController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionStock(string $q): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $select = ['id', 'name', 'ticker'];

        $stocks = StockEntity::find()
            ->select($select)
            ->where(['ticker' => strtoupper($q)])
            ->indexBy('ticker')
            ->limit(3)
            ->all();

        if (count($stocks) < 10) {
            $subStocks = StockEntity::find()
                ->select($select)
                ->where(['ilike', 'ticker', $q . '%', false])
                ->indexBy('ticker')
                ->limit(10 - count($stocks))
                ->all();

            $stocks += ArrayHelper::merge($subStocks, $stocks);
        }

        if (count($stocks) < 10) {
            $subStocks = StockEntity::find()
                ->select($select)
                ->where(['ilike', 'ticker', $q])
                ->indexBy('ticker')
                ->limit(10 - count($stocks))
                ->all();

            $stocks += ArrayHelper::merge($subStocks, $stocks);
        }

        if (count($stocks) < 10) {
            $subStocks = StockEntity::find()
                ->select($select)
                ->where(['ilike', 'name', $q . '%', false])
                ->indexBy('ticker')
                ->limit(10 - count($stocks))
                ->all();

            $stocks += ArrayHelper::merge($subStocks, $stocks);
        }

        if (count($stocks) < 10) {
            $subStocks = StockEntity::find()
                ->select($select)
                ->where(['ilike', 'name', $q])
                ->indexBy('ticker')
                ->limit(10 - count($stocks))
                ->all();

            $stocks += ArrayHelper::merge($subStocks, $stocks);
        }

        $result = ['results' => []];

        foreach ($stocks as $stock) {
            $result['results'][] = [
                'id' => $stock->id,
                'text' => $stock->name . ' (' . $stock->ticker . ')',
            ];
        }

        return $result;
    }
}
