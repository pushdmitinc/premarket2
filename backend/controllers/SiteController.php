<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['auth', 'login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $this->layout = 'blank';

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout(): Response
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * @return mixed
     */
    public function actionAuth()
    {
        $appId = Yii::$app->vk->appId;
        $appSecret = Yii::$app->vk->appSecret;
        $code = Yii::$app->request->get('code');
        $redirect = 'http://' . $_SERVER['SERVER_NAME'] . '/site/auth';

        if (!$code) {
            $url = 'http://oauth.vk.com/authorize?client_id=' . $appId;
            $url .= '&display=popup&redirect_uri=' . $redirect . '&response_type=code';

            return $this->redirect($url);
        }

        $url = 'https://oauth.vk.com/access_token?client_id=' . $appId;
        $url .= '&client_secret=' . $appSecret . '&code=' . $code . '&redirect_uri=' . $redirect;

        $result = file_get_contents($url);
        $result = json_decode($result, true);

        var_dump($result); die;
    }
}
