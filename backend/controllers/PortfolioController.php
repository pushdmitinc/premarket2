<?php

namespace backend\controllers;

use common\models\portfolio\PortfolioStockCollection;
use common\models\PortfolioStock;
use JetBrains\PhpStorm\ArrayShape;
use Yii;
use common\models\Portfolio;
use common\models\PortfolioSearch;
use yii\base\InvalidConfigException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * PortfolioController implements the CRUD actions for Portfolio model.
 */
class PortfolioController extends BaseController
{
    public ?Portfolio $portfolio = null;

    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Portfolio models.
     * @return mixed
     */
    public function actionIndex(): string
    {
        $searchModel = new PortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException
     */
    public function actionView(int $id): string
    {
        $model = $this->findModel($id);

        $model->stock_type = Yii::$app->request->get('type');
        $stockGroup = PortfolioStockCollection::getStockGroup($model);

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('_view-pjax', [
                'model' => $model,
                'stockGroup' => $stockGroup,
            ]);
        }

        return $this->render('view', [
            'model' => $model,
            'stockGroup' => $stockGroup,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException
     */
    public function actionViewClose(int $id): string
    {
        $model = $this->findModel($id);

        $stockGroupClose = PortfolioStockCollection::getStockGroupClose($model);
        usort($stockGroupClose, fn($a, $b) => strcmp($a->name, $b->name));

        return $this->render('view-close', [
            'model' => $model,
            'stockGroupClose' => $stockGroupClose,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     *
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    #[ArrayShape(['meta' => "array", 'items' => "array"])]
    public function actionViewUpdate(int $id): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = $this->findModel($id);

        $formatter = Yii::$app->formatter;
        $model->stock_type = Yii::$app->request->get('type');
        $stockGroup = PortfolioStockCollection::getStockGroup($model);

        $result = [
            'meta' => [
                'portfolioProfit' => $formatter->asCurrency($model->profitType()),
                'portfolioCurrentPrice' => $formatter->asCurrency($model->totalPriceCurrentType()),
                'changePercent' => $model->changePercentType() . '%',
                'changeDay' => $formatter->asCurrency($model->getChangeDay()),
                'changePercentDay' => $model->getChangePercentDay() . '%',
            ],
            'items' => [],
        ];

        foreach ($stockGroup as $collection) {
            $result['items'][$collection->getTicker()] = [
                'percentDay' => $collection->getPercentDay() . '%',
                'currentPercent' => $collection->getPercentCurrent() . '%',
                'currentPrice' => $formatter->stockCurrency($collection->getPriceCurrent(), $collection->getCurrency()),
                'totalPriceCurrent' => $formatter->asCurrency($collection->totalPriceCurrent(), $collection->getCurrency()),
                'profit' => $formatter->asCurrency($collection->profit(), $collection->getCurrency()),
                'percentToPurpose' => $collection->percentToPurpose() . '%',
                'percentToStopLoss' => $collection->percentToStopLoss() . '%',
            ];
        }

        return $result;
    }

    /**
     * @param $stock_id
     *
     * @return string
     */
    public function actionStockView($stock_id): string
    {
        $model = PortfolioStock::findOne($stock_id);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('stock/_view', [
                'model' => $model,
            ]);
        }

        return $this->render('stock/_view', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionCreate(): Response|string
    {
        $model = new Portfolio();

        $model->user_id = Yii::$app->getUser()->getId();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $searchModel = new PortfolioSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->asJson([
                'reloadBlock' => [[
                    'selector' => '.portfolio-grid',
                    'content' => $this->renderAjax('_index', [
                        'dataProvider' => $dataProvider,
                    ]),
                ]],
            ]);
        }

        return $this->renderPartial('create', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate(int $id): mixed
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $searchModel = new PortfolioSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->asJson([
                'reloadBlock' => [[
                    'selector' => '.portfolio-grid',
                    'content' => $this->renderAjax('_index', [
                        'dataProvider' => $dataProvider,
                    ]),
                ]],
            ]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('update', [
                'model' => $model,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|Response
     *
     * @throws NotFoundHttpException
     */
    public function actionStockCreate($id): Response|string
    {
        $portfolio = $this->findModel($id);

        $model = new PortfolioStock();
        $model->portfolio_id = $portfolio->id;
        $model->date = date('Y-m-d');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['portfolio/view', 'id' => $portfolio->id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('stock/create', [
                'model' => $model,
            ]);
        }

        return $this->render('stock/create', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @param $stock_id
     *
     * @return string|Response
     */
    public function actionStockClose($id, $stock_id): Response|string
    {
        $model = new PortfolioStock();
        $model->portfolio_id = $id;
        $model->stock_id = $stock_id;
        $model->date_close = date('Y-m-d');

        if (Yii::$app->request->isPost) {
            if ($model->load(Yii::$app->request->post()) && $model->closePosition()) {
                return $this->redirect(['portfolio/view', 'id' => $id]);
            }
        }

        return $this->renderWithAjax('stock/close', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $stock_id
     *
     * @return string|Response
     *
     * @throws NotFoundHttpException
     */
    public function actionStockUpdate(int $stock_id): string|Response
    {
        $model = $this->findModelStock($stock_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['portfolio/view', 'id' => $model->portfolio_id]);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('stock/update', [
                'model' => $model,
            ]);
        }

        return $this->render('stock/update', [
            'model' => $model,
        ]);
    }

    /**
     * @param integer $id
     * @return mixed
     *
     * @throws NotFoundHttpException
     * @throws yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param integer $id
     *
     * @return Portfolio
     * @throws NotFoundHttpException
     */
    protected function findModel(int $id): Portfolio
    {
        if ($this->portfolio && $this->portfolio->id === $id) {
            return $this->portfolio;
        }

        if (($model = Portfolio::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param integer $id
     *
     * @return PortfolioStock
     * @throws NotFoundHttpException
     */
    protected function findModelStock($id): PortfolioStock
    {
        if (($model = PortfolioStock::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
