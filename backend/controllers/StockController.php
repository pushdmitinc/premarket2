<?php

namespace backend\controllers;

use common\models\stock\StockFuture;
use Yii;
use common\models\stock\StockEntity;
use common\models\stock\StockSearch;
use yii\db\StaleObjectException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * StockController implements the CRUD actions for StockEntity model.
 */
class StockController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StockEntity models.
     * @return string
     */
    public function actionIndex(): string
    {
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->setPageSize(100);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionFuture(): string
    {
        $searchModel = new StockSearch();

        $searchModel->type = StockEntity::TYPE_FUTURE;
        $searchModel->dateStart = date('Y-m-d', time() - 86400 * 3);

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->pagination->setPageSize(100);

        return $this->render('future', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param int|null $id
     *
     * @return string|Response
     *
     * @throws yii\db\Exception
     */
    public function actionFutureUpdate(int $id = null): Response|string
    {
        $model = $id ? StockFuture::findOne(['future_id' => $id]) : null;

        if ($model === null) {
            $model = new StockFuture();
            $model->future_id = $id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['stock/future']);
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('_future-update', [
                'model' => $model,
            ]);
        }

        return $this->render('_future-update', [
            'model' => $model,
        ]);
    }

    /**
     * Displays a single StockEntity model.
     * @param integer $id
     * @return string
     */
    public function actionView($id): string
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StockEntity model.
     *
     * @return string|Response
     */
    public function actionCreate(): string|Response
    {
        $model = new StockEntity();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing StockEntity model.
     *
     * @param integer $id
     *
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id): string|Response
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return Response
     *
     * @throws NotFoundHttpException
     * @throws StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the StockEntity model based on its primary key value.
     *
     * @param integer $id
     *
     * @return StockEntity the loaded model
     *
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StockEntity::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
