<?php

namespace backend\assets;

class AdminLteAsset extends \dmstr\web\AdminLteAsset
{
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
