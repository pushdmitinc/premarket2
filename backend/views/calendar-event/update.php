<?php

use common\models\Calendar;
use common\widgets\Modal\ModalWidget;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $model \common\models\CalendarEvent */

$this->title = $model->name;

if ($model->calendar instanceof Calendar) {
    $this->params['breadcrumbs'][] = [
        'label' => $model->calendar->name,
        'url' => ['/calendar/view', 'id' => $model->calendar_id]
    ];
}

$this->params['breadcrumbs'][] = $this->title;

$formId = 'form-calendar-event';
?>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Событие</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?=$this->render('_form', ['model' => $model, 'formId' => $formId])?>
        </div>
        <div class="modal-footer">
            <?= ModalWidget::widget([
                'url' => ['/calendar-event/update', 'id' => $model->id],
                'content' => 'Сохранить',
                'cssClass' => 'btn btn-success',
                'options' => ['data' => ['request-form' => $formId]],
            ]) ?>
            <?= Html::button('Отмена', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
