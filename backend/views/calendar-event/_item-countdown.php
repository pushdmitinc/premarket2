<?php
/* @var $this yii\web\View */

use common\widgets\Countdown;
use common\widgets\Modal\ModalWidget;
use yii\helpers\Html;

/* @var $model common\models\CalendarEvent */

$countdownId = 'countdown-event-' . $model->id;
$eventDelete = "function(){ $('#" . $countdownId . "').text('прошло'); }";
?>

<div class="col-md-6 item-countdown">
    <div class="card mb-4">
        <div class="row no-gutters">
            <div class="col-md-12">
                <div class="card-body center">
                    <h5 class="card-title">
                        <?= ModalWidget::widget([
                            'url' => [
                                '/calendar/event',
                                'calendar_id' => $model->calendar_id,
                                'date' => $model->date,
                            ],
                            'content' => $model->name,
                        ]) ?>
                        <?= Html::a(
                            '<i class="fas fa-calendar-alt"></i>',
                            ['/calendar/view', 'id' => $model->calendar_id]
                        ) ?>
                    </h5>
                    <p class="card-text card-text-24">
                        <?= Countdown::widget([
                            'id' => $countdownId,
                            'datetime' => $model->dateTime(),
                            'format' => $model->countdownFormat(),
                            'tagName' => 'span',
                            'events' => ['finish' => $eventDelete],
                        ]) ?>
                    </p>
                    <p class="card-text">
                        <small class="text-muted">
                            Дата: <?= Yii::$app->formatter->asDate($model->date) . ' ' . $model->getTime() ?>
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
