<?php

use common\models\Calendar;
use common\widgets\Modal\ModalWidget;
use kartik\date\DatePicker;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\CalendarEvent */

$this->title = $model->name;

if ($model->calendar instanceof Calendar) {
    $this->params['breadcrumbs'][] = [
        'label' => $model->calendar->name,
        'url' => ['/calendar/view', 'id' => $model->calendar_id]
    ];
}

if (!$model->dateTo) {
    $model->dateTo = date('Y-m-d', time() + 60*60*24);
}

$this->params['breadcrumbs'][] = $this->title;

$formId = 'form-calendar-event';
?>
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Событие</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="calendar-event-form">
                <?php $form = ActiveForm::begin(['id' => $formId]); ?>

                <div class="row">
                    <div class="col-md-6">
                        <?= $form->field($model, 'type') ->dropDownList([2 => 'Копия', 1 => 'Перенос']) ?>
                    </div>
                    <div class="col-md-6">
                        <?= $form->field($model, 'dateTo')->widget(DatePicker::class, [
                            'options' => ['placeholder' => 'Выберите дату...'],
                            'value' => $model->date,
                            'type' => DatePicker::TYPE_COMPONENT_APPEND,
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]);?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= ModalWidget::widget([
                'url' => ['/calendar-event/copy', 'calendar_id' => $model->calendar_id, 'date' => $model->date],
                'content' => 'Сохранить',
                'cssClass' => 'btn btn-success',
                'options' => ['data' => ['request-form' => $formId]],
            ]) ?>
            <?= Html::button('Отмена', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
