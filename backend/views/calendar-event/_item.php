<?php
/* @var $this yii\web\View */

use common\models\CalendarUser;
use common\widgets\Countdown;
use common\widgets\Modal\ModalWidget;

/* @var $model common\models\CalendarEvent */

$format = $model->isTimerDay();
?>

<li class="item">
    <div class="product-time">
        <div><?= $model->getTime() ?></div>
        <?php if ($format): ?>
            <div class="timer">
                <?= Countdown::widget([
                    'id' => 'countdown-' . $model->id,
                    'datetime' => $model->date . ' ' . $model->getTime(),
                    'format' => $format,
                    'tagName' => 'span',
                ])?>
            </div>
        <?php endif; ?>
    </div>
    <div class="product-info">
        <span href="" class="product-title">
            <?= $model->nameHtml() ?>

            <?php if ($model->calendar->canUserSing(CalendarUser::SING_UPDATE)): ?>
                <?= ModalWidget::widget([
                    'content' => '<i class="fas fa-times pull-right"></i>',
                    'url' => ['/calendar-event/delete', 'id' => $model->id],
                    'cssClass' => 'text-danger',
                    'options' => ['data-request-method' => 'POST'],
                ]) ?>
                <?= ModalWidget::widget([
                    'content' => '<i class="fas fa-edit pull-right"></i>',
                    'url' => ['/calendar-event/update', 'id' => $model->id],
                ]) ?>
                <?php if ($model->isSubscribe()): ?>
                    <?= ModalWidget::widget([
                        'content' => $model->iconSubscribe(),
                        'url' => ['/calendar-event/subscribe', 'id' => $model->id],
                        'cssClass' => 'text-success subscribe-event',
                        'options' => ['data' => ['type' => 'json', 'id' => $model->id]],
                    ]) ?>
                <?php endif; ?>
            <?php endif; ?>
        </span>
        <span class="product-description">
              <?= $model->htmlDescription() ?>
        </span>
    </div>
</li>
