<?php

use common\helpers\CalendarHelper;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\CalendarEvent */
/* @var $form yii\widgets\ActiveForm */
/* @var $formId yii\widgets\ActiveForm */
?>

<div class="calendar-event-form">

    <?php $form = ActiveForm::begin(['id' => $formId]); ?>

    <div class="row">
        <div class="col-md-7">
            <?= $form->field($model, 'name')->textInput() ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'time') ->dropDownList(CalendarHelper::timeList()) ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'value')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'style_class')->dropDownList(CalendarHelper::$styleClassList) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status_id')->dropDownList(CalendarHelper::$statusList) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'repeat')->dropDownList(CalendarHelper::repeatSelect()) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'date')->widget(
                DatePicker::class, [
                'options' => ['placeholder' => 'Выберите дату...'],
                'value' => $model->date,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'interval')->dropDownList([
                '' => 'Не повторять',
                'P1D' => 'Каждый день',
                'P2D' => 'Через день',
                'P3D' => 'Через два день',
                'P4D' => 'Через три день',
                'P7D' => 'Каждую неделю',
                'P1М' => 'Каждую месяц',
            ]) ?>
        </div>

        <div class="col-md-4">
            <?= $form->field($model, 'dateTo')->widget(
                DatePicker::class, [
                'options' => ['placeholder' => 'Выберите дату по...'],
                'value' => $model->date,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd'
                ]
            ]);?>
        </div>
    </div>

    <?= $form->field($model, 'description')->textarea(['rows' => 1]) ?>

    <?php ActiveForm::end(); ?>

</div>
