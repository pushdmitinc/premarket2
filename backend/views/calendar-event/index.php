<?php

use common\models\Calendar;
use common\models\CalendarUser;
use common\widgets\Modal\ModalWidget;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\search\CalendarEventSearch */


$this->title = 'События календаря';

$search = clone $searchModel;
$search->date = null;

$date = new DateTime($searchModel->date);

$date->add(new DateInterval('P1M'));
$dateMonthFollowing = $date->format('Y-m-d');

$date->sub(new DateInterval('P2M'));
$dateMonthPrevious = $date->format('Y-m-d');

$date->add(new DateInterval('P1M'));

$date->sub(new DateInterval('P1D'));
$datePrevious = $date->format('Y-m-d');

$date->add(new DateInterval('P2D'));
$dateFollowing = $date->format('Y-m-d');

if ($searchModel->calendar_id) {
    $this->title .= ': ' . $searchModel->calendar->name;
    $this->params['breadcrumbs'][] = [
        'label' => $searchModel->calendar->name,
        'url' => ['/calendar/view', 'id' => $searchModel->calendar_id]
    ];
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">
                События <?= Yii::$app->formatter->asDate($searchModel->date) ?>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row list-calendar">
                        <?= \common\widgets\Calendar::widget([
                            'dateStart' => $search->dateStart(),
                            'countMonth' => 1,
                            'classCol' => 'col-md-12',
                            'currentDate' => $searchModel->date,
                            'searchModel' => $search,
                            'url' => ['/calendar/event', 'calendar_id' => $search->calendar_id],
                            'urlIsAjax' => true,
                            'urlIsEmpty' => true,
                            'viewMonthName' => false,
                            'modalHidden' => 0,
                            'popover' => false,
                        ]) ?>
                    </div>

                    <ul class="products-list product-list-in-box" style="margin-top:20px">
                        <?php foreach ($dataProvider->getModels() as $model): ?>
                            <?= $this->render('_item', ['model' => $model]) ?>
                        <?php endforeach; ?>
                    </ul>

                    <?php if ($search->calendar->canSign(Calendar::SIGN_VIEW_DIFF)): ?>
                        <div class="product-diff" style="text-align:center">
                            <?= $searchModel->dateDiff() ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                <i class="fas fa-times"></i>
            </button>

            <div style="display:none">
                <?= ModalWidget::widget([
                    'url' => [
                        '/calendar/event',
                        'calendar_id' => $searchModel->calendar_id,
                        'date' => $searchModel->date,
                        'type' => 'month',
                    ],
                    'content' => '<i class="fas fa-sync"></i>',
                    'cssClass' => 'btn btn-default',
                    'options' => ['data' => ['type' => 'json'], 'id' => 'reloadMonth'],
                ]) ?>
                <?= ModalWidget::widget([
                    'url' => [
                        '/calendar/event',
                        'calendar_id' => $searchModel->calendar_id,
                        'date' => $datePrevious,
                    ],
                    'content' => '<i class="fas fa-chevron-left"></i>',
                    'cssClass' => 'btn btn-primary',
                ]) ?>
                <?= ModalWidget::widget([
                    'url' => [
                        '/calendar/event',
                        'calendar_id' => $searchModel->calendar_id,
                        'date' => $dateFollowing,
                    ],
                    'content' => '<i class="fas fa-chevron-right"></i>',
                    'cssClass' => 'btn btn-primary',
                ]) ?>
            </div>

            <?= ModalWidget::widget([
                'url' => [
                    '/calendar/event',
                    'calendar_id' => $searchModel->calendar_id,
                    'date' => $dateMonthPrevious,
                ],
                'content' => '<i class="fas fa-angle-double-left"></i>',
                'cssClass' => 'btn btn-primary',
            ]) ?>
            <?= ModalWidget::widget([
                'url' => [
                    '/calendar/event',
                    'calendar_id' => $searchModel->calendar_id,
                    'date' => $dateMonthFollowing,
                ],
                'content' => '<i class="fas fa-angle-double-right"></i>',
                'cssClass' => 'btn btn-primary',
            ]) ?>

            <?php if ($searchModel->calendar->canUserSing(CalendarUser::SING_UPDATE)): ?>
                <?php if ($dataProvider->totalCount): ?>
                    <?= ModalWidget::widget([
                        'url' => [
                            '/calendar-event/copy',
                            'calendar_id' => $searchModel->calendar_id,
                            'date' => $searchModel->date,
                        ],
                        'content' => '<i class="fas fa-share"></i>',
                        'cssClass' => 'btn btn-info',
                    ]) ?>
                    <?= ModalWidget::widget([
                        'url' => [
                            '/calendar-event/clear',
                            'calendar_id' => $searchModel->calendar_id,
                            'date' => $searchModel->date,
                        ],
                        'content' => '<i class="far fa-times-circle"></i>',
                        'cssClass' => 'btn btn-danger',
                        'options' => ['data' => ['confirm' => 'Уверены что хотите очистить события?']],
                    ]) ?>
                <?php endif; ?>

                <?= ModalWidget::widget([
                    'url' => [
                        '/calendar-event/create',
                        'calendar_id' => $searchModel->calendar_id,
                        'date' => $searchModel->date,
                    ],
                    'content' => '<i class="fas fa-plus"></i>',
                    'cssClass' => 'btn btn-success',
                ]) ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php if (Yii::$app->request->isPost): ?>
    <script>
        $('#reloadMonth').click();
    </script>
<?php endif; ?>
