<?php

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */

use common\helpers\AdminLteHelper;

$formatter = Yii::$app->formatter;
?>

<div class="row">
    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-aqua">
            <div class="inner">
                <h3><?= $formatter->asCurrency($model->totalPriceType()) ?></h3>
                <p>Начальная стоимость</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-<?= AdminLteHelper::colorDeviation($model->profit()) ?>">
            <div class="inner">
                <h3 class="portfolioCurrentPrice">
                    <?= $formatter->asCurrency($model->totalPriceCurrentType()) ?>
                </h3>
                <p>
                    Стоимость
                    (<span class="changeDay" title="изменение за день"><?= $formatter->asCurrency($model->getChangeDay()) ?></span>)
                    [<span class="changePercentDay" title="изменение за день %"><?= $model->getChangePercentDay() ?>%</span>]
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-<?= AdminLteHelper::colorDeviation($model->changePercentType()) ?>">
            <div class="inner">
                <h3 class="portfolioProfit">
                    <?= $formatter->asCurrency($model->profitType()) ?>
                </h3>
                <p>
                    Прибыль
                    [<span class="changePercent"><?= $model->changePercentType()?>%</span>]
                    [<span class="changeTotalPercent" title="с учетом дивидендов"><?= $model->changeTotalPercentType()?>%</span>]
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= $formatter->asCurrency($model->dividendType()) ?></h3>
                <p>
                    Дивиденды
                    [<span class="changeDividendPercent"><?= $model->changePercentDividendType()?>%</span>]
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
        </div>
    </div>
</div>
