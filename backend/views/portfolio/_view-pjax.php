<?php

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $stockGroup common\models\portfolio\PortfolioStockCollection[] */

use common\helpers\AdminLteHelper;

$formatter = Yii::$app->formatter;
?>

<?= $this->render('_view-cart', ['model' => $model]) ?>

<div style="text-align:center; padding-bottom:10px">
    <strong>Сортировать по:</strong>
    <span data-sort=".stockName">
        названию
        <span class="arrow"></span>
    </span>
    &#8661;
    <span data-sort=".percentDay" data-sort-value="float">
        изм. день
        <span class="arrow"></span>
    </span>
    &#8661;
    <span data-sort=".currentPercent" data-sort-value="float">
        изменению
        <span class="arrow"></span>
    </span>
    &#8661;
    <span data-sort=".currentPrice" data-sort-value="float">
        цене
        <span class="arrow"></span>
    </span>
    &#8661;
    <span data-sort=".stockProportion" data-sort-type="desc" data-sort-value="float">
        доля
        <span class="arrow"></span>
    </span>
    &#8661;
    <span data-sort=".percentToPurpose" data-sort-type="desc" data-sort-value="float">
        тейк
        <span class="arrow"></span>
    </span>
    &#8661;
    <span data-sort=".percentToStopLoss" data-sort-type="desc" data-sort-value="float">
        стоп
        <span class="arrow"></span>
    </span>
    &#8661;
    <span data-sort=".stockDate" data-sort-value="int">
        дата
        <span class="arrow">&#9660;</span>
    </span>
</div>

<?= $this->render('_view-collection', ['model' => $model, 'stockGroup' => $stockGroup]) ?>
