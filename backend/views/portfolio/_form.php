<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portfolio-form">
    <?php $form = ActiveForm::begin(['id' => $model->formId()]); ?>

    <div class="box-body table-responsive">
        <?= $form->field($model, 'user_id')->textInput() ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
