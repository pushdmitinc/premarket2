<?php

use common\models\Portfolio;
use common\widgets\Modal\ModalWidget;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
//    'filterModel' => $searchModel,
    'layout' => "{items}\n{summary}\n{pager}",
    'columns' => [
        'name',
        'count_stock' => [
            'class' => 'yii\grid\DataColumn',
            'contentOptions' => ['class' => 'text-right'],
            'label' => 'Кол. акций',
            'attribute' => 'count_stock',
            'content' => function(Portfolio $model) {
                return count($model->portfolioStocks);
            },
        ],
        'total_price' => [
            'class' => 'yii\grid\DataColumn',
            'contentOptions' => ['class' => 'text-right'],
            'label' => 'Нач. стоим.',
            'attribute' => 'total_price',
            'format' => 'decimal',
            'content' => function(Portfolio $model) {
                return Yii::$app->formatter->asDecimal($model->totalPrice(), 2);
            },
            'footer' => 123,
        ],
        'total_price_current' => [
            'contentOptions' => ['class' => 'text-right'],
            'label' => 'Teк. стоим.',
            'attribute' => 'total_price_current',
            'content' => function(Portfolio $model) {
                return Yii::$app->formatter->asDecimal($model->totalPriceCurrent(), 2);
            },
        ],
        'profit' => [
            'contentOptions' => ['class' => 'text-right'],
            'label' => 'Прибыль',
            'attribute' => 'profit',
            'content' => function(Portfolio $model) {
                return Yii::$app->formatter->asDecimal($model->profit(), 2);
            },
        ],
        'change' => [
            'contentOptions' => ['class' => 'text-right'],
            'label' => 'Изм %',
            'attribute' => 'change',
            'content' => function(Portfolio $model) {
                return $model->changePercent();
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'view' => function ($url) {
                    return Html::a('<i class="glyphicon glyphicon-eye-open"></i>', $url);
                },
                'update' => function ($url) {
                    return ModalWidget::widget([
                        'url' => $url,
                        'content' => '<i class="glyphicon glyphicon-pencil"></i>',
                    ]);
                },
            ],
        ],
    ],
]); ?>
