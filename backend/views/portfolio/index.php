<?php

use common\widgets\Modal\ModalWidget;
use kartik\select2\Select2Asset;
use kartik\select2\Select2KrajeeAsset;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PortfolioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Портфели';
$this->params['breadcrumbs'][] = $this->title;

Select2Asset::register($this);
Select2KrajeeAsset::register($this);
?>
<div class="portfolio-index box box-primary">
    <div class="box-header with-border">
        <?= ModalWidget::widget([
            'content' => 'Добавить портфель',
            'url' => ['create'],
            'cssClass' => 'btn btn-success btn-flat',
        ]) ?>
    </div>
    <div class="portfolio-grid box-body table-responsive no-padding">
        <?= $this->render('_index', ['dataProvider' => $dataProvider, 'searchModel' => $searchModel]) ?>
    </div>
</div>
