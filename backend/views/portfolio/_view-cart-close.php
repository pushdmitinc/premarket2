<?php

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */

use common\helpers\AdminLteHelper;

$formatter = Yii::$app->formatter;
?>

<div class="row">
    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-<?= AdminLteHelper::colorDeviation($model->changePercentWithClose()) ?>">
            <div class="inner">
                <h3><?= $formatter->asCurrency($model->profitWithClose()) ?></h3>
                <p>
                    Прибыль с закр.поз.
                    [<span class=""><?= $model->changePercentWithClose() ?>%</span>]
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-<?= AdminLteHelper::colorDeviation($model->changePercentFull()) ?>">
            <div class="inner">
                <h3 class="">
                    <?= $formatter->asCurrency($model->profitFull()) ?>
                </h3>
                <p>
                    Прибыль полная
                    [<span class=""><?= $model->changePercentFull() ?>%</span>]
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-<?= AdminLteHelper::colorDeviation($model->changePercentClose(), 0) ?>">
            <div class="inner">
                <h3 class="">
                    <?= $formatter->asCurrency($model->profitClose()) ?>
                </h3>
                <p>
                    Прибыль
                    [<span class=""><?= $model->changePercentClose() ?>%</span>]
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-12">
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?= $formatter->asCurrency($model->dividendClose()) ?></h3>
                <p>
                    Дивиденды закр.поз.
                    [<span class=""><?= $model->changePercentDividendClose() ?>%</span>]
                </p>
            </div>
            <div class="icon">
                <i class="ion ion-pie-graph"></i>
            </div>
        </div>
    </div>
</div>
