<?php

use common\helpers\StockHelper;
use common\widgets\Modal\ModalWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $stockGroup common\models\portfolio\PortfolioStockCollection[] */

$formatter = Yii::$app->formatter;
?>

<?php foreach ($stockGroup as $collection) : ?>
    <div class="box box-default box-collection collapsed-box ticker-<?= $collection->getTicker() ?>">
        <div class="box-header with-border">
            <div class="row">
                <div class="col-sm-2 col-xs-6 name">
                    <h3 class="box-title stockName" data-widget="collapse">
                        <?= $collection->getName() ?>
                    </h3>
                    <div class="description">
                        <?= $collection->getTicker() ?>
                        &middot;
                        <?= $collection->totalQuantity() ?>
                        &middot;
                        <?= $collection->getPrice() ?>
                        &middot;
                        <span class="stockProportion">
                            <?= $model->getStockProportion($collection->totalPriceCurrent(StockHelper::CURRENCY_SYSTEM)) ?>
                        </span>
                        &hellip;
                    </div>
                </div>

                <div class="col-sm-2 col-xs-3">
                    <div class="description-block  border-right">
                        <span class="description-percentage text-<?= $collection->typeClassDay() ?>">
                            <i class="fa fa-caret-<?= $collection->typeDeviationDay() ?>"></i>
                            <span class="percentDay">
                                <?= $collection->getPercentDay() ?>%
                            </span>
                        </span>
                        (<span class="description-percentage text-<?= $collection->typeClass() ?>"
                               title="<?= StockHelper::getPercentOrEmpty($collection->getPercentCurrencyCurrent()) ?>">
                            <i class="fa fa-caret-<?= $collection->typeDeviation() ?>"></i>
                            <span class="currentPercent">
                                <?= $collection->getPercentCurrent() ?>%
                            </span>
                        </span>)
                        <h5 class="bg-i description-header currentPrice">
                            <?= $formatter->stockCurrency($collection->getPriceCurrent(), $collection->getCurrency()) ?>
                        </h5>
                    </div>
                </div>

                <div class="col-sm-2 col-768-0 col-xs-0">
                    <div class="description-block border-right">
                        <span class="description-percentage text-<?= $collection->typeClassDividend() ?>">
                            <i class="fa fa-caret-<?= $collection->typeDeviationDividend() ?>"></i>
                            <?= $collection->getDividendPercent() ?>%
                        </span>
                        <h5 class="description-header">
                            <?= $formatter->asCurrency($collection->dividend()) ?>
                        </h5>
                    </div>
                </div>

                <div class="col-sm-2 col-768-0 col-xs-0">
                    <div class="description-block border-right">
                        <span class="description-percentage text-<?= $collection->typeClass() ?>">
                            <i class="fa fa-caret-<?= $collection->typeDeviation() ?>"></i>
                            <span>
                                <?= $formatter->asCurrency($collection->profit(), $collection->getCurrency()) ?>
                            </span>
                        </span>
                        <h5 class="bg-i description-header totalPriceCurrent">
                            <?= $formatter->asCurrency($collection->totalPriceCurrent(), $collection->getCurrency()) ?>
                        </h5>
                    </div>
                </div>

                <div class="col-sm-2 col-768-0 col-xs-0">
                    <div class="description-block border-right purpose-block">
                        <span class="description-percentage text-<?= $collection->typeClassPurpose() ?>">
                            <?= $collection->purpose() ?>
                            [<?= $collection->purposePercent() ?>%]
                            [<span class="percentToPurpose"><?= $collection->percentToPurpose() ?>%</span>]
                        </span>
                        <div class="description-percentage text-<?= $collection->typeClassStopLoss() ?>">
                            <?= $collection->stopLoss() ?>
                            [<?= $collection->stopLossPercent() ?>%]
                            [<span class="percentToStopLoss"><?= $collection->percentToStopLoss() ?>%</span>]
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 col-768-3 col-680-0">
                    <div class="description-block">
                        <span class="description-percentage text-<?= $collection->typeClass() ?>">
                            <i class="fa fa-caret-up"></i>
                            <span class="stockDate">
                                <?= $collection->getDateDiffDays() ?> дней
                            </span>
                        </span>
                        <h5 class="description-header">
                            <?= $formatter->asDate($collection->getDate()) ?>
                        </h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="box-body">
            <div class="row">
                <div class="col-lg-3 col-xs-12">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?= $formatter->asCurrency($collection->totalPrice(), $collection->getCurrency()) ?></h3>
                            <p>Начальная стоимость</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-card"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?= $formatter->asCurrency($collection->getCurrentPriceFull(), $collection->getCurrency()) ?></h3>
                            <p>Текущая стоимость + дивиденды</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-card"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-xs-12">
                    <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3><?= $formatter->asCurrency($collection->getProfitTotal(), $collection->getCurrency()) ?></h3>
                            <p>Прибыль/убыток + дивиденды</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-card"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-xs-12">
                    <div class="small-box bg-<?= $collection->typePercentTotal() ?>">
                        <div class="inner">
                            <h3><?= $collection->getPercentTotal() ?> %</h3>
                            <p>Изменение с дивидендами %</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-card"></i>
                        </div>
                    </div>
                </div>
            </div>

            <table class="table table-condensed table-portfolio-stock">
                <tbody>
                <tr>
                    <th>Цена пок.</th>
                    <th>Кол-во</th>
                    <th>Стоим.пок.</th>
                    <th>Стоим.тек.</th>
                    <th>Дивы</th>
                    <th>Прибыль</th>
                    <th>Изм, %</th>
                    <th>Изм++, %</th>
                    <th>Дата</th>
                    <th></th>
                </tr>
                <?php foreach ($collection->collections as $portfolioStock): ?>
                    <tr>
                        <td>
                            <?= $formatter->stockCurrency($portfolioStock->getPrice(), $collection->getCurrency()) ?>
                        </td>
                        <td>
                            <?= $portfolioStock->quantity ?>
                        </td>
                        <td>
                            <?= $formatter->asCurrency($portfolioStock->totalPrice(), $collection->getCurrency()) ?>
                        </td>
                        <td>
                            <?= $formatter->asCurrency($portfolioStock->totalPriceCurrent(), $collection->getCurrency()) ?>
                        </td>
                        <td>
                            <?= $formatter->asCurrency($portfolioStock->dividend(), $collection->getCurrency()) ?>
                        </td>
                        <td>
                            <?= $formatter->asCurrency($portfolioStock->profit(), $collection->getCurrency()) ?>
                        </td>
                        <td>
                            <?= $portfolioStock->percent() ?>%
                        </td>
                        <td>
                            <?= $portfolioStock->percentTotal() ?>%
                        </td>
                        <td>
                            <?= $formatter->asDate($portfolioStock->date) ?>
                        </td>
                        <td class="action">
                            <?= Html::a('<i class="fa fa-eye"></i>',
                                ['portfolio/stock-view', 'stock_id' => $portfolioStock->id]
                            ) ?>
                            <?= ModalWidget::widget([
                                'url' => ['portfolio/stock-update', 'stock_id' => $portfolioStock->id],
                                'content' => '<i class="fa fa-edit"></i>',
                            ]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <div style="text-align: center">
                <?= ModalWidget::widget([
                    'url' => ['/portfolio/stock-close', 'id' => $model->id, 'stock_id' => $portfolioStock->stock_id],
                    'content' => 'Продать акции',
                    'cssClass' => 'btn btn-primary',
                ]) ?>
            </div>
        </div>
    </div>
<?php endforeach; ?>
