<?php

use common\helpers\AdminLteHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $searchModel common\models\PortfolioStockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $stockGroupClose common\models\portfolio\PortfolioStockCollection[] */

$formatter = Yii::$app->formatter;

$this->title = 'Закрытые позиции';
$this->params['breadcrumbs'][] = ['label' => 'Портфели', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio box box-<?= AdminLteHelper::labelDeviation($model->changePercentFull()) ?>">
    <div class="box-header with-border" style="padding:10px">
        <h3 class="box-title">Закрытые позиции</h3>
    </div>

    <div class="box-body">
        <?= $this->render('_view-cart', ['model' => $model]) ?>

        <?= $this->render('_view-cart-close', ['model' => $model]) ?>

        <?= $this->render('_view-collection-close', ['stockGroup' => $stockGroupClose]) ?>
    </div>
</div>
