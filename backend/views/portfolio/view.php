<?php

use common\models\stock\StockEntity;
use common\widgets\Modal\ModalWidget;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $searchModel common\models\PortfolioStockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $stockGroup common\models\portfolio\PortfolioStockCollection[] */
/* @var $stockGroupClose common\models\portfolio\PortfolioStockCollection[] */

$formatter = Yii::$app->formatter;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Портфели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="portfolio" role="document">
    <div id="portfolio-view-pjax">
        <?= $this->render('_view-pjax', ['model' => $model, 'stockGroup' => $stockGroup]) ?>
    </div>

    <div style="text-align:center;padding:10px 0 15px">
        <?= ModalWidget::widget([
            'url' => ['/portfolio/stock-create', 'id' => $model->id],
            'content' => 'Добавить акцию',
            'cssClass' => 'btn btn-danger',
        ]) ?>
        <?php if ($model->stock_type !== StockEntity::TYPE_BOND) : ?>
            <?= Html::a('Облигации',
                ['/portfolio/view', 'id' => $model->id, 'type' => StockEntity::TYPE_BOND],
                ['class' => 'btn btn-success']
            ) ?>
        <?php endif; ?>
        <?php if ($model->stock_type !== StockEntity::TYPE_STOCK) : ?>
            <?= Html::a('Акции',
                ['/portfolio/view', 'id' => $model->id, 'type' => StockEntity::TYPE_STOCK],
                ['class' => 'btn btn-primary']
            ) ?>
        <?php endif; ?>
        <?php if ($model->stock_type) : ?>
            <?= Html::a('Все',
                ['/portfolio/view', 'id' => $model->id],
                ['class' => 'btn btn-success']
            ) ?>
        <?php endif; ?>
        <?= Html::a('Закрытые позиции', ['/portfolio/view-close', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>
    </div>
</div>

<div class="charts">
    <?= $this->render('_charts', ['model' => $model]) ?>
</div>

<script>
    var data = {id: <?= $model->id ?>};

    <?php if ($model->stock_type): ?>
        data = {id: <?= $model->id ?>, type: <?= $model->stock_type ?>};
    <?php endif; ?>

    var interval = setInterval(function () {
        $.get('/portfolio/view-update', data, function (data) {
            for (var meta in data.meta) {
                $('.' + meta).html(data.meta[meta]);
            }

            for (var key in data.items) {
                var block = $('.ticker-' + key);

                for (var keyClass in data.items[key]) {
                    var element = block.find('.' + keyClass);

                    if (element.hasClass('bg-i')) {
                        var newVal = parseFloat(data.items[key][keyClass].replace(' ', '').replace(',', '.'));
                        var currentVal = element.text().replace(/^\s+/, '').replace(' ', '').replace(',', '.');
                        currentVal = parseFloat(currentVal);

                        if (currentVal > newVal) {
                            element.addClass('bg-red').addClass('interval').removeClass('bg-green');
                        }

                        if (currentVal < newVal) {
                            element.addClass('bg-green').addClass('interval').removeClass('bg-red');
                        }
                    }

                    element.html(data.items[key][keyClass]);
                }
            }
        });
    }, 3000);

    setInterval(function () {
        $('.interval.disabled')
            .removeClass('interval')
            .removeClass('disabled')
            .removeClass('bg-red')
            .removeClass('bg-green');

        $('.interval').addClass('disabled');
    }, 1000);

    setTimeout(function () {
        $('[data-sort]').click(function () {
            var _this = $(this);
            var sorted = $('.box-collection');
            var sortClass = _this.data('sort');
            var sortType = _this.data('sort-type');
            var sortValue = _this.data('sort-value');

            $('[data-sort]').find('.arrow').text('');

            sorted.sort(function (a, b) {
                var val1 = $(a).find(sortClass).text()
                    .replace(/^\s+/, '').replace(' ', '').replace(',', '.');
                var val2 = $(b).find(sortClass).text()
                    .replace(/^\s+/, '').replace(' ', '').replace(',', '.');

                if (sortValue === 'float') {
                    val1 = parseFloat(val1);
                    val2 = parseFloat(val2);
                }

                if (sortValue === 'int') {
                    val1 = parseFloat(val1);
                    val2 = parseFloat(val2);
                }

                if (sortType === 'desc') {
                    _this.data('sort-type', 'asc');
                    _this.find('.arrow').html("&#9660;");

                    return val1 < val2;
                }

                _this.data('sort-type', 'desc');
                _this.find('.arrow').html("&#9650;");

                return val1 > val2;
            }).appendTo(sorted.parent());
        });
    }, 2000);

</script>
