<?php

use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PortfolioStock */
/* @var $form yii\widgets\ActiveForm */

$stockData = [];

if ($model->stock) {
    $stockData = [$model->stock->id => $model->stock->name];
}
?>

<div class="portfolio-stock-form">
    <?php $form = ActiveForm::begin(['id' => $model->formId()]); ?>

    <div class="box-body table-responsive">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'portfolio_id')->textInput() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'stock_id')->widget(Select2::class, [
                    'data' => $stockData,
                    'options' => ['placeholder' => 'Select a state ...'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'ajax' => [
                            'url' => Url::to(['search/stock']),
                            'dataType' => 'json',
                            'delay' => 250,
                            'cache' => true,
                        ],
                    ],
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'quantity')->textInput() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'price')->textInput() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'take_profit')->textInput() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'purposePercent')->textInput() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'stop_loss')->textInput() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'stopLossPercent')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'date')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]); ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'date_purpose')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'date_close')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]); ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'price_close')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'currency_price')->textInput() ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<script>
    // $('#portfoliostock-purposepercent, #portfoliostock-price').change(function () {
    //     var price = parseInt($('#portfoliostock-price').val());
    //     var percent = parseInt($('#portfoliostock-purposepercent').val()) / 100 + 1;
    //
    //     $('#portfoliostock-take_profit').val(price * percent);
    // });
    //
    // $('#portfoliostock-stoplosspercent, #portfoliostock-price').change(function () {
    //     var price = parseInt($('#portfoliostock-price').val());
    //     var percent = 1 - parseInt($('#portfoliostock-stoplosspercent').val()) / 100;
    //
    //     $('#portfoliostock-stop_loss').val(price * percent);
    // });
</script>