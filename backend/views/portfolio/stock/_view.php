<?php

use common\helpers\PortfolioHelper;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $model common\models\PortfolioStock */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->stock->name . ' (' . $model->stock->ticker . ')';

$this->params['breadcrumbs'][] = ['label' => 'Портфели', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->portfolio->name, 'url' => ['view', 'id' => $model->portfolio_id]];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="modal-dialog modal-lg" role="document">
    <div class="box box-primary" role="document">
        <div class="modal-content box-body table-responsive">
            <div class="modal-header">
                <h5 class="modal-title">
                    <?= $this->title ?>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-lg-6 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?= $model->profitTotal() ?></h3>
                                <p>Прибыль/убыток</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-card"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?= $model->percentTotal() ?><sup style="font-size: 20px">%</sup></h3>
                                <p>Изменение %</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-card"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-aqua">
                            <div class="inner">
                                <h3><?= $model->dividend() ?></h3>
                                <p>Дивиденды</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-bag"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-green">
                            <div class="inner">
                                <h3><?= $model->dividendPercent() ?><sup style="font-size: 20px">%</sup></h3>
                                <p>Дивиденды %</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-pie-graph"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-yellow">
                            <div class="inner">
                                <h3><?= $model->profit() ?></h3>
                                <p>Прибыль/убыток</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-card"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>

                    <div class="col-lg-3 col-xs-6">
                        <div class="small-box bg-red">
                            <div class="inner">
                                <h3><?= $model->percent() ?><sup style="font-size: 20px">%</sup></h3>
                                <p>Изменение %</p>
                            </div>
                            <div class="icon">
                                <i class="ion ion-stats-bars"></i>
                            </div>
                            <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="info-box bg-aqua box-purpose">
                            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Цель</span>
                                <span class="info-box-number purpose-price">
                                    <?= $model->price ?>
                                    <?php if ($model->percentToPurpose() > 10 && $model->percentToPurpose() < 90) : ?>
                                        <small class="label pull-right bg-green price-current"
                                               style="left:<?= $model->percentToPurpose() - 6 ?>%">
                                            <?= $model->stock->price_prev ?>
                                            <i class="fa fa-caret-down down"
                                               style="left:<?= $model->percentToPurpose() - 5 ?>%"></i>
                                        </small>
                                    <?php endif; ?>
                                    <span style="float: right"><?= $model->purpose() ?></span>
                                </span>
                                <div class="progress">
                                    <div class="progress-bar" style="width:<?= $model->percentToPurpose() ?>%"></div>
                                </div>
                                <span class="progress-description">
                                    <?= $model->percentToPurpose() ?>% прошло <?= $model->daysBefore() ?> дней
                                </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="info-box bg-green box-purpose">
                            <span class="info-box-icon"><i class="ion ion-ios-pricetag-outline"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Цель с дивидендами</span>
                                <span class="info-box-number purpose-price">
                                    <?= $model->price ?>
                                    <?php if ($model->percentTotalToPurpose() > 10 && $model->percentTotalToPurpose() < 90) : ?>
                                        <small class="label pull-right bg-green-active price-current"
                                               style="left:<?= $model->percentTotalToPurpose() - 6 ?>%">
                                            <?= $model->stock->price_prev ?>
                                            <i class="fa fa-caret-down down"
                                               style="left:<?= $model->percentTotalToPurpose() - 5 ?>%"></i>
                                        </small>
                                    <?php endif; ?>
                                    <span style="float: right"><?= $model->purpose() ?></span>
                                </span>
                                <div class="progress">
                                    <div class="progress-bar" style="width:<?= $model->percentTotalToPurpose() ?>%"></div>
                                </div>
                                <span class="progress-description">
                                    <?= $model->percentTotalToPurpose() ?>% прошло <?= $model->daysBefore() ?> дней
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row stock">
                    <div class="col-md-7">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">График доходности</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="chart">
                                    <?= Highcharts::widget(
                                        [
                                            'htmlOptions' => [
                                                'id' => 'chart-profit',
                                                'class' => 'item-charts-chart',
                                            ],
                                            'options' => array_merge(
                                                [
                                                    'gradient' => ['enabled' => true],
                                                    'credits' => ['enabled' => false],
                                                    'exporting' => ['enabled' => false],
                                                    'chart' => [
                                                        'type' => 'spline',
                                                    ],
                                                    'title' => [
                                                        'text' => '',
                                                        'margin' => 0
                                                    ],
                                                    'colors' => ['#1E6393', '#f39c12', '#dd4b39', '#0094FF'],
                                                    'tooltip' => [
                                                        'split' => true,
                                                    ],
                                                    'plotOptions' => [
                                                        'series' => [
                                                            'lineWidth' => 2,
                                                            'label' => [
                                                                'connectorAllowed' => false,
                                                            ],
                                                            'marker' => [
                                                                'enabled' => false,
                                                            ],
                                                        ],
                                                    ],
                                                    'legend' => [
                                                        'useHTML' => true,
                                                        'symbolWidth' => 0,
                                                    ],
                                                    'yAxis' => [
                                                        'title' => ['text' => false],
                                                    ],
                                                ],
                                                PortfolioHelper::getChartProfit($model)
                                            ),
                                        ],
                                    ) ?>
                                </div>
                            </div>
                        </div>

                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">График доходности</h3>
                                <div class="box-tools pull-right">
                                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                        <i class="fa fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </div>

                            <div class="box-body">
                                <div class="chart">
                                    <?= Highcharts::widget(
                                        [
                                            'htmlOptions' => [
                                                'id' => 'chart-percent',
                                                'class' => 'item-charts-chart',
                                            ],
                                            'options' => array_merge(
                                                [
                                                    'gradient' => ['enabled' => true],
                                                    'credits' => ['enabled' => false],
                                                    'exporting' => ['enabled' => false],
                                                    'chart' => [
                                                        'type' => 'spline',
                                                    ],
                                                    'title' => [
                                                        'text' => '',
                                                        'margin' => 0
                                                    ],
                                                    'colors' => ['#1E6393', '#f39c12', '#dd4b39', '#0094FF'],
                                                    'tooltip' => [
                                                        'split' => true,
                                                    ],
                                                    'plotOptions' => [
                                                        'series' => [
                                                            'lineWidth' => 2,
                                                            'label' => [
                                                                'connectorAllowed' => false,
                                                            ],
                                                            'marker' => [
                                                                'enabled' => false,
                                                            ],
                                                        ],
                                                    ],
                                                    'legend' => [
                                                        'useHTML' => true,
                                                        'symbolWidth' => 0,
                                                    ],
                                                    'yAxis' => [
                                                        'title' => ['text' => false],
                                                    ],
                                                ],
                                                PortfolioHelper::getChartPercent($model)
                                            ),
                                        ],
                                    ) ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Параметр</h3>
                            </div>

                            <div class="box-body no-padding">
                                <table class="table table-condensed">
                                    <tbody>
                                    <tr>
                                        <th>Название</th>
                                        <th style="text-align: right">Значение</th>
                                    </tr>
                                    <?php foreach (PortfolioHelper::getParams($model) as $key => $value): ?>
                                        <tr>
                                            <td>
                                                <?= $key ?>
                                            </td>
                                            <td style="text-align: right">
                                                <?= $value ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Параметр</h3>
                            </div>

                            <div class="box-body no-padding">
                                <table class="table table-condensed">
                                    <tbody>
                                    <tr>
                                        <th>Название</th>
                                        <th style="text-align: right">Значение</th>
                                    </tr>
                                    <?php foreach ($model->stock->getParams() as $key => $value): ?>
                                        <tr>
                                            <td>
                                                <?= $key ?>
                                            </td>
                                            <td style="text-align: right">
                                                <?= $value ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Дивиденды</h3>
                            </div>

                            <div class="box-body no-padding">
                                <table class="table table-condensed">
                                    <tbody>
                                    <tr>
                                        <th>Дата</th>
                                        <th>Год</th>
                                        <th>Период</th>
                                        <th>Сумма</th>
                                        <th>%</th>
                                    </tr>
                                    <?php foreach ($model->dividends as $dividend): ?>
                                        <tr>
                                            <td>
                                                <?= Yii::$app->formatter->asDate($dividend->date) ?>
                                            </td>
                                            <td>
                                                <?= $dividend->period ?>
                                            </td>
                                            <td>
                                                <span class="badge bg-green">
                                                    <?= Yii::$app->formatter->asCurrency($dividend->dividend * $model->quantity) ?>
                                                </span>
                                            </td>
                                            <td>
                                                <?= round($dividend->dividend / $model->price * 100, 2) ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-7">
                    </div>

                    <div class="col-md-5">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
