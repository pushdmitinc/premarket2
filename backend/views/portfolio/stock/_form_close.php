<?php

use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PortfolioStock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="portfolio-stock-form">
    <?php $form = ActiveForm::begin(['id' => $model->formId()]); ?>

    <div class="box-body table-responsive">
        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'quantity')->textInput() ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'stock_id')->hiddenInput()->label(false) ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'date_close')->widget(DatePicker::class, [
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'pluginOptions' => [
                        'autoclose' => true,
                        'format' => 'yyyy-mm-dd',
                    ]
                ]); ?>
            </div>

            <div class="col-md-6">
                <?= $form->field($model, 'price_close')->textInput() ?>
            </div>
        </div>

        <?php if (!Yii::$app->request->isAjax) : ?>
            <div class="form-group">
                <?= Html::submitButton('Продать', ['class' => 'btn btn-success']) ?>
            </div>
        <?php endif; ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>