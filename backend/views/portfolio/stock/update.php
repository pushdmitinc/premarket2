<?php

/* @var $this yii\web\View */
/* @var $model common\models\PortfolioStock */

use common\widgets\Modal\ModalWidget;
use yii\helpers\Html;

$this->title = 'Добавить портфель';
$this->params['breadcrumbs'][] = ['label' => 'Портфели', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-dialog" role="document">
    <div class="box box-primary" role="document">
        <div class="modal-content box-body table-responsive">
            <div class="modal-header">
                <h5 class="modal-title"><?= $this->title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->render('_form', ['model' => $model]) ?>
            </div>
            <div class="modal-footer">
                <?= ModalWidget::widget([
                    'url' => ['/portfolio/stock-update', 'id' => $model->id],
                    'content' => 'Сохранить',
                    'cssClass' => 'btn btn-success',
                    'sendClass' => 'modal-send-form',
                ]) ?>
                <?= Html::button('Отмена', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
            </div>
        </div>
    </div>
</div>
