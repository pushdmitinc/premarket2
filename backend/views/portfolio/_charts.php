<?php

use common\helpers\PortfolioHelper;
use miloschuman\highcharts\Highcharts;

/* @var $this yii\web\View */
/* @var $model common\models\Portfolio */
/* @var $stockGroup common\models\portfolio\PortfolioStockCollection[] */
?>

<div class="row">
    <div class="col-md-4">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Валюты</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="box-body">
                <div class="chart">
                    <?= Highcharts::widget(
                        [
                            'htmlOptions' => [
                                'id' => 'chart-currency',
                                'class' => 'item-charts-pie',
                            ],
                            'options' => array_merge(
                                [
                                    'gradient' => ['enabled' => true],
                                    'credits' => ['enabled' => false],
                                    'exporting' => ['enabled' => false],
                                    'chart' => [
                                        'type' => 'pie',
                                    ],
                                    'title' => [
                                        'text' => '',
                                        'margin' => 0
                                    ],
                                    'tooltip' => [
                                        'pointFormat' => '<b>{point.percentage:.1f}%</b> ({point.price})',
                                    ],
                                    'plotOptions' => [
                                        'pie' => [
                                            'allowPointSelect' => true,
                                            'cursor' => 'pointer',
                                            'dataLabels' => [
                                                'enabled' => false,
                                            ],
                                            'showInLegend' => true,
                                        ],
                                    ],
                                ],
                                PortfolioHelper::getChartCurrency($model)
                            ),
                        ],
                    ) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Сектора</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="box-body">
                <div class="chart">
                    <?= Highcharts::widget(
                        [
                            'htmlOptions' => [
                                'id' => 'chart-sector',
                                'class' => 'item-charts-pie',
                            ],
                            'options' => array_merge(
                                [
                                    'gradient' => ['enabled' => true],
                                    'credits' => ['enabled' => false],
                                    'exporting' => ['enabled' => false],
                                    'chart' => [
                                        'type' => 'pie',
                                    ],
                                    'title' => [
                                        'text' => '',
                                        'margin' => 0
                                    ],
                                    'tooltip' => [
                                        'pointFormat' => '<b>{point.percentage:.1f}%</b> ({point.price})',
                                    ],
                                    'plotOptions' => [
                                        'pie' => [
                                            'allowPointSelect' => true,
                                            'cursor' => 'pointer',
                                            'dataLabels' => [
                                                'enabled' => false,
                                            ],
                                            'showInLegend' => true,
                                        ],
                                    ],
                                ],
                                PortfolioHelper::getChartSector($model)
                            ),
                        ],
                    ) ?>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Тип</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove">
                        <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>

            <div class="box-body">
                <div class="chart">
                    <?= Highcharts::widget(
                        [
                            'htmlOptions' => [
                                'id' => 'chart-type',
                                'class' => 'item-charts-pie',
                            ],
                            'options' => array_merge(
                                [
                                    'gradient' => ['enabled' => true],
                                    'credits' => ['enabled' => false],
                                    'exporting' => ['enabled' => false],
                                    'chart' => [
                                        'type' => 'pie',
                                    ],
                                    'title' => [
                                        'text' => '',
                                        'margin' => 0
                                    ],
                                    'tooltip' => [
                                        'pointFormat' => '<b>{point.percentage:.1f}%</b> ({point.price})',
                                    ],
                                    'plotOptions' => [
                                        'pie' => [
                                            'allowPointSelect' => true,
                                            'cursor' => 'pointer',
                                            'dataLabels' => [
                                                'enabled' => false,
                                            ],
                                            'showInLegend' => true,
                                        ],
                                    ],
                                ],
                                PortfolioHelper::getChartType($model)
                            ),
                        ],
                    ) ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">График изменения за год</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <div class="chart">
            <?= Highcharts::widget(
                [
                    'htmlOptions' => [
                        'id' => 'chart-change-year',
                        'class' => 'item-charts-chart',
                    ],
                    'options' => array_merge(
                        [
                            'gradient' => ['enabled' => true],
                            'credits' => ['enabled' => false],
                            'exporting' => ['enabled' => false],
                            'legend' => ['enabled' => false],
                            'chart' => [
                                'type' => 'column',
                            ],
                            'title' => [
                                'text' => '',
                                'margin' => 0
                            ],
                            'tooltip' => [
                                'pointFormat' => '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} %</b><br/>',
                                'valueDecimals' => 2,
                                'split' => true,
                            ],
                            'yAxis' => [
                                'title' => ['text' => false],
                            ],
                        ],
                        PortfolioHelper::getChangeYear($model)
                    ),
                ],
            ) ?>
        </div>
    </div>
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Динамика портфеля</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <div class="chart">
            <?= Highcharts::widget(
                [
                    'htmlOptions' => [
                        'id' => 'chart-change-year-percent',
                        'class' => 'item-charts-chart',
                    ],
                    'options' => array_merge(
                        [
                            'gradient' => ['enabled' => true],
                            'credits' => ['enabled' => false],
                            'exporting' => ['enabled' => false],
                            'legend' => ['enabled' => false],
                            'chart' => [
                                'type' => 'line',
                            ],
                            'title' => [
                                'text' => '',
                                'margin' => 0
                            ],
                            'tooltip' => [
                                'pointFormat' => '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} %</b><br/>',
                                'valueDecimals' => 2,
                                'split' => true,
                            ],
                            'yAxis' => [
                                'title' => ['text' => false],
                            ],
                        ],
                        PortfolioHelper::getChangePercent($model)
                    ),
                ],
            ) ?>
        </div>
    </div>
</div>

<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">График стоимости</h3>
        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
        </div>
    </div>

    <div class="box-body">
        <div class="chart">
            <?= Highcharts::widget(
                [
                    'htmlOptions' => [
                        'id' => 'chart-profit',
                        'class' => 'item-charts-chart',
                    ],
                    'options' => array_merge(
                        [
                            'gradient' => ['enabled' => true],
                            'credits' => ['enabled' => false],
                            'exporting' => ['enabled' => false],
                            'chart' => [
                                'type' => 'column',
                            ],
                            'title' => [
                                'text' => '',
                                'margin' => 0
                            ],
                            'tooltip' => [
                                'pointFormat' => '<span style="color:{series.color}">{series.name}</span>: <b>{point.y} ₽</b><br/>',
                                'valueDecimals' => 2,
                                'split' => true,
                            ],
                            'yAxis' => [
                                'title' => ['text' => false],
                            ],
                        ],
                        PortfolioHelper::getChartPrice($model)
                    ),
                ],
            ) ?>
        </div>
    </div>
</div>
