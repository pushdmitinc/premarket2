<?php

/** @var $content string */

use yii\helpers\Inflector;
use yii\widgets\Breadcrumbs;
use dmstr\widgets\Alert;

if ($this->title === null && isset($this->blocks['content-header'])) {
    $this->title = $this->blocks['content-header'];
}

if ($this->title === null) {
    $this->title = Inflector::camel2words(
        Inflector::id2camel($this->context->module->id)
    );

    if ($this->context->module->id !== \Yii::$app->id) {
        $this->title .= '<small>Module</small>';
    }
}
?>
<div class="content-wrapper">
    <section class="content-header">
        <h1><?= $this->title ?></h1>

        <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs'] ?? []]) ?>
    </section>

    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
    </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.0
    </div>
    <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
</footer>