<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $content string */

backend\assets\AppAsset::register($this);

if (Yii::$app->controller->action->id === 'login') {
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
} else { ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?= Url::to('/favicon.svg', true) ?>" type=" image/svg+xml">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="skin-blue sidebar-mini sidebar-collapse">
        <?php $this->beginBody() ?>

        <div class="wrapper">
            <?= $this->render('header.php') ?>

            <?= $this->render('left.php') ?>

            <?= $this->render('content.php', ['content' => $content]) ?>
        </div>

        <?php $this->endBody() ?>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
