<aside class="main-sidebar">

    <section class="sidebar">
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= common\helpers\AdminLteHelper::filePath('/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>

        <?= dmstr\widgets\Menu::widget([
            'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
            'items' => [
                ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                ['label' => 'Главная', 'icon' => 'university', 'url' => ['/']],
                [
                    'label' => 'Активы',
                    'icon' => 'chart-line',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Акции', 'icon' => 'chart-line', 'url' => ['/stock']],
                        ['label' => 'Акции TQBR', 'icon' => 'chart-line', 'url' => ['/stock?class_code=TQBR']],
                        ['label' => 'Фьючерсы', 'icon' => 'chart-line', 'url' => ['/stock/future']],
                    ],
                ],
                ['label' => 'Портфели', 'icon' => 'suitcase', 'url' => ['/portfolio']],
                ['label' => 'Дивиденты', 'icon' => 'hand-holding-usd', 'url' => ['/dividend']],
                ['label' => 'Календари', 'icon' => 'calendar', 'url' => ['/calendar']],
                ['label' => 'Уведомления', 'icon' => 'paper-plane', 'url' => ['/notifications']],
                [
                    'label' => 'Some tools',
                    'icon' => 'share',
                    'url' => '#',
                    'items' => [
                        ['label' => 'Gii', 'icon' => 'terminal', 'url' => ['/gii'],],
                        ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                        [
                            'label' => 'Level One',
                            'icon' => 'circle-o',
                            'url' => '#',
                            'items' => [
                                ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                [
                                    'label' => 'Level Two',
                                    'icon' => 'circle-o',
                                    'url' => '#',
                                    'items' => [
                                        ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ]) ?>
    </section>
</aside>
