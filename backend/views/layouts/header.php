<?php

use common\helpers\AdminLteHelper;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$directoryAsset = AdminLteHelper::directoryAsset();
?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">PM</span><span class="logo-lg">' . Yii::$app->name . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="fa sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

    </nav>
</header>
