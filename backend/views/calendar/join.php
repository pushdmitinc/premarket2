<?php

use common\helpers\SignsHelper;
use common\widgets\Modal\ModalWidget;
use kartik\select2\Select2;
use yii\bootstrap\Html;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \common\models\CalendarUser */

$this->title = 'Добавить пользователя';
$this->params['breadcrumbs'][] = $this->title;

$formId = 'form-calendar-event';
?>
<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Добавить пользователя</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="calendar-event-form">
                <?php $form = ActiveForm::begin(['id' => $formId]); ?>

                <div class="row">
                    <div class="col-md-12">
                        <?= $form->field($model, 'user_id')->widget(Select2::class, [
                            'options' => ['placeholder' => 'Поиск по email ...'],
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 5,
                                'ajax' => [
                                    'url' => '/site/user',
                                    'dataType' => 'json',
                                    'delay' => 250,
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                            ],
                        ]) ?>
                    </div>
                </div>

                <div class="row">
                    <?php foreach ($model::signsLabels() as $key => $label): ?>
                        <div class="col-md-4">
                            <?= SignsHelper::checkbox($form, $model, $key) ?>
                        </div>
                    <?php endforeach; ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= ModalWidget::widget([
                'url' => ['/calendar/join', 'id' => $model->calendar_id, 'model_id' => $model->id],
                'content' => 'Сохранить',
                'cssClass' => 'btn btn-success',
                'options' => ['data' => ['request-form' => $formId]],
            ]) ?>
            <?= Html::button('Отмена', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
        </div>
    </div>
</div>
