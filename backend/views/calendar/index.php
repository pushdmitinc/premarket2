<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CalendarSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \common\models\Calendar */

$this->title = 'Календари пользователей';

if (!Yii::$app->user->isGuest) {
    if ($searchModel->isMy) {
        $this->title = 'Мои календари';
        $this->params['breadcrumbs'][] = ['label' => 'Все календари', 'url' => ['/calendar']];
    } else {
        $this->params['breadcrumbs'][] = [
            'label' => 'Мои календари',
            'url' => ['/calendar', 'isMy' => 1]
        ];
    }
}

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calendar-index">

    <?php $form = ActiveForm::begin(['method' => 'get']); ?>

        <div style="text-align: center; padding-bottom: 10px">
            <div class="search-calendar">
                <?= $form->field($searchModel, 'name')->textInput()->label(false) ?>
            </div>

            <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-primary']) ?>

            <?= Html::a('<i class="fas fa-plus"></i>', ['create'], ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

    <?= $this->render('/site/_content-header', ['title' => $this->title])?>

    <ul class="products-list cards product-list-in-box">
        <?php foreach ($dataProvider->getModels() as $model): ?>
            <?= $this->render('_item', ['model' => $model])?>
        <?php endforeach; ?>
    </ul>

    <div class="clearfix" style="padding-bottom: 15px"></div>

    <?php if ($searchModel->isMy && !Yii::$app->user->isGuest): ?>
        <?= $this->render('/site/_content-header', [
            'title' => 'Календари',
            'subTitle' => 'в которых состою',
        ])?>

        <ul class="products-list cards product-list-in-box">
            <?php foreach ($searchModel->joinCalendar()->getModels() as $model): ?>
                <?= $this->render('_item', ['model' => $model])?>
            <?php endforeach; ?>
        </ul>

        <div class="clearfix" style="padding-bottom: 15px"></div>
    <?php endif; ?>

</div>
