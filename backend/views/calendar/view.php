<?php

use common\helpers\DateHelper;
use common\helpers\SettingsHelper;
use common\models\CalendarUser;
use common\widgets\Calendar;
use common\widgets\Modal\ModalWidget;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\YiiAsset;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */
/* @var $searchModel common\models\search\CalendarEventSearch */

$this->title = $model->name;

if ($searchModel->startYear) {
    $this->title .= ' ' . $searchModel->startYear . ' год';
}

$this->params['breadcrumbs'][] = ['label' => 'Календари', 'url' => ['/calendar']];
$this->params['breadcrumbs'][] = $this->title;

YiiAsset::register($this);

$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->keywords ?: SettingsHelper::getKeywords(),
], 'keywords');
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->description_short ?: SettingsHelper::getDescription(),
], 'description');

if ($searchModel->startYear === null) {
    $searchModel->startYear = (new DateTime($model->date_start ?: 'now'))->format('Y');
}

//$this->registerJs("$('.modal-remote-view').last().click();");
?>
<div class="calendar-view">
    <?php if ($model->canSign(\common\models\Calendar::SIGN_SELECT_YEAR)): ?>
        <div>
            <?php $form = ActiveForm::begin([
                'action' => ['view', 'id' => $model->id],
                'method' => 'get',
            ]); ?>

            <div class="row" style="text-align:center">
                <div class="col-md-3"></div>

                <div class="col-md-2">
                    <?= $form->field($searchModel, 'startYear')
                        ->dropDownList(DateHelper::yearsList())
                        ->label(false)
                    ?>
                </div>

                <div class="col-md-2">
                    <?= $form->field($searchModel, 'startMonth')
                        ->dropDownList(DateHelper::$monthName)
                        ->label(false)
                    ?>
                </div>

                <div class="col-md-1" style="text-align:left">
                    <?= Html::submitButton('Показать', ['class' => 'btn btn-primary']) ?>
                </div>

                <div class="col-md-2"></div>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    <?php endif; ?>

    <div class="row list-calendar">
        <?= Calendar::widget([
            'dateStart' => $searchModel->dateStart(new DateTime($model->date_start)),
            'searchModel' => $searchModel,
            'popover' => SettingsHelper::isNotMobile(),
            'popoverTitle' => false,
            'url' => ['/calendar/event', 'calendar_id' => $model->id],
            'urlIsAjax' => true,
            'urlIsEmpty' => $model->canUserSing(CalendarUser::SING_UPDATE),
            'isMetaData' => true,
            'popoverDiff' => $model->canSign($model::SIGN_VIEW_DIFF),
        ]) ?>
    </div>

    <?php if ($model->canUserSing(CalendarUser::SING_UPDATE)): ?>
        <div class="row" style="padding-bottom:15px">
            <div class="col-md-12" style="text-align: center">
                <?= ModalWidget::widget([
                    'url' => ['user', 'id' => $model->id],
                    'content' => 'Пользователи',
                    'cssClass' => 'btn btn-success',
                ]) ?>
                <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>

        <?php if ($model->canSign(\common\models\Calendar::SIGN_VIEW_LINK)): ?>
            <div class="calendar-link" style="text-align:center;padding-top:10px">
                Поделится: <?= Html::input('input', 'link', $model->getLink()) ?>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <div class="calendar-description">
        <?= $model->description ?>
    </div>
</div>

<div style="display:none">
    <?= ModalWidget::widget([
        'url' => ['event', 'calendar_id' => $model->id, 'date' => $model->date_start],
        'content' => 'Пользователи',
        'cssClass' => 'btn btn-success',
        'options' => ['data-hide' => 'true'],
    ]) ?>
</div>

<?php if (Yii::$app->request->get('date')): ?>
    <script>
        setTimeout(function () {
            $('[data-date=<?= Yii::$app->request->get('date') ?>]').click();
        }, 500);
    </script>
<?php endif; ?>
