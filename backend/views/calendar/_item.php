<?php

use common\models\CalendarUser;
use common\widgets\Modal\ModalWidget;
use yii\bootstrap\Html;

/* @var $model \common\models\Calendar */
?>

<li class="item">
    <div class="product-img">
        <img src="/images/default-50x50.gif" alt="Product Image">
    </div>
    <div class="product-info">
    <span href="" class="product-title">
        <?= Html::a($model->name, ['/calendar/view', 'id' => $model->id]) ?>
        <?php if ($model->canUserSing(CalendarUser::SING_UPDATE)): ?>
            <?= ModalWidget::widget([
                'content' => '<i class="fas fa-edit pull-right"></i>',
                'url' => ['/calendar/update', 'id' => $model->id],
                'cssClass' => 'right',
                'modal' => 'calendar-create',
            ]) ?>
        <?php endif; ?>
    </span>
    <span class="product-description">
        <?= $model->description_short ?: 'нет описания' ?>
    </span>
    </div>
</li>
