<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */

$this->title = 'Добавить календарь';
$this->params['breadcrumbs'][] = ['label' => 'Календари', 'url' => ['/calendar']];
$this->params['breadcrumbs'][] = $this->title;

$formId = 'form-calendar';
?>

<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">
                <?= $this->title ?>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <?= $this->render('_form', ['model' => $model, 'formId' => $formId]) ?>
        </div>
        <div class="modal-footer">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'form' => $formId]) ?>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="z-index:9999">
                Закрыть
            </button>
        </div>
    </div>
</div>
