<?php

use common\widgets\Modal\ModalWidget;

/* @var $model \common\models\Calendar */

$this->title = 'События календаря';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">
                Пользователи <?= $model->name ?>
            </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="box box-primary">
                <div class="box-body">
                    <ul class="products-list product-list-in-box" style="margin-top:20px">
                        <?php foreach ($model->calendarUsers as $user): ?>
                            <li class="item">
                                <div class="product-time">
                                    <div>12 34</div>
                                </div>
                                <div class="product-info">
                                    <span class="product-title">
                                        <?= $user->user->firstname . ' ' . $user->user->lastname ?>

                                        <?= ModalWidget::widget([
                                            'content' => '<i class="fas fa-times pull-right"></i>',
                                            'url' => ['/calendar/delete', 'id' => $model->id],
                                            'cssClass' => 'text-danger',
                                            'options' => ['data-request-method' => 'POST'],
                                        ]) ?>
                                        <?= ModalWidget::widget([
                                            'content' => '<i class="fas fa-edit pull-right"></i>',
                                            'url' => ['/calendar/join', 'id' => $model->id, 'model_id' => $user->id],
                                        ]) ?>
                                    </span>
                                    <span class="product-description">
                                          <?= 'тут будут роли' ?>
                                    </span>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="text-align:center">
            <button type="button" class="btn btn-secondary" data-dismiss="modal" style="float:left">
                <i class="fas fa-times"></i>
            </button>

            <?= ModalWidget::widget([
                'url' => ['/calendar/join', 'id' => $model->id],
                'content' => '<i class="fas fa-plus"></i>',
                'cssClass' => 'btn btn-primary',
            ]) ?>
        </div>
    </div>
</div>

