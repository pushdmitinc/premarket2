<?php

use common\helpers\CalendarHelper;
use common\helpers\SettingsHelper;
use common\helpers\SignsHelper;
use common\models\Calendar;
use vova07\imperavi\Widget;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */
/* @var $form yii\widgets\ActiveForm */
/* @var $formId string */

if (!isset($formId)) {
    $formId = 'form-calendar';
}
?>

<div class="calendar-form">

    <?php $form = ActiveForm::begin(['id' => $formId]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description_short')->textInput(['maxlength' => true]) ?>

    <div>
        <?= $form->field($model, 'date_start')->widget(
            DatePicker::class, [
            'options' => ['placeholder' => 'Выберите дату...'],
            'value' => $model->date_start,
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
                'orientation' => 'top right',
            ]
        ]); ?>
    </div>

    <?= $form->field($model, 'keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'allowedTags' => SettingsHelper::$allowedTags,
            'replaceDivs' => false,
            'minHeight' => 200,
            'plugins' => [
                'clips',
                'fullscreen',
                'table',
            ],
            'clips' => [
                ['Lorem ipsum...', 'Lorem...'],
                ['red', '<span class="label-red">red</span>'],
                ['green', '<span class="label-green">green</span>'],
                ['blue', '<span class="label-blue">blue</span>'],
            ],
        ],
    ]) ?>

    <div class="collapse-title">
        <div class="collapsed" data-toggle="collapse" data-target="#collapseParam" aria-expanded="false" aria-controls="collapseParam">
            Параметры
            <span  class="collapse-icon">
                <i class="fas fa-angle-up"></i>
            </span>
        </div>
    </div>
    <div class="collapse" id="collapseParam">
        <div class="row">
            <?php foreach (CalendarHelper::getStyleList() as $key => $name): ?>
                <?php if ($key === '') continue; ?>
                <?php $attribute = 'params[n][' . $key . ']'; ?>
                <?php $label = 'Название "' . $name . '"'; ?>
                <div class="col-md-3">
                    <?= $form->field($model, $attribute)->textInput()->label($label) ?>
                </div>
            <?php endforeach; ?>
            <div class="col-md-3">
                <?= $form->field($model, 'params[totalCountName]')->textInput()->label('Общее') ?>
            </div>
            <div class="col-md-3">
                <?= $form->field($model, 'params[totalSumName]')->textInput()->label('Сумма') ?>
            </div>
        </div>
    </div>

    <div class="row" style="padding-top:15px">
        <?php foreach (Calendar::signsLabels() as $key => $label): ?>
            <div class="col-md-3">
                <?= SignsHelper::checkbox($form, $model, $key) ?>
            </div>
        <?php endforeach; ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
