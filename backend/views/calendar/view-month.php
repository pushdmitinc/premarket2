<?php

use common\widgets\Calendar;

/* @var $this yii\web\View */
/* @var $model common\models\Calendar */
/* @var $searchModel common\models\search\CalendarEventSearch */

$search = clone $searchModel;
$search->date = null;
?>

<?= Calendar::widget([
    'dateStart' => $searchModel->dateStart(),
    'searchModel' => $search,
    'popoverTitle' => false,
    'url' => ['/calendar/event', 'calendar_id' => $model->id],
    'urlIsAjax' => true,
    'urlIsEmpty' => true,
    'isMetaData' => true,
    'isMonthOne' => true,
    'popoverDiff' => $model->canSign($model::SIGN_VIEW_DIFF),
]) ?>
