<?php
/* @var $this yii\web\View */
/* @var $title string */
/* @var $subTitle string */
/* @var $right string */

if (!isset($subTitle)) $subTitle = '';
if (!isset($right)) $right = '';
?>

<div class="content-header">
    <h3><?= $title ?> <small><?= $subTitle ?></small></h3>
    <div class="pull-right">
        <?= $right ?>
    </div>
</div>