<?php

use common\helpers\SelectHelper;
use common\models\stock\StockEntity;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Dividend */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dividend-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>

    <div class="box-body table-responsive">

        <?= $form->field($model, 'stock_id')->widget(Select2::class, [
            'data' => StockEntity::getMapList(),
            'options' => ['placeholder' => 'Select a state ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]); ?>

        <?= $form->field($model, 'dividend')->textInput() ?>

        <?= $form->field($model, 'year')->dropDownList(SelectHelper::year()) ?>

        <?= $form->field($model, 'period')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'stock_price')->textInput() ?>

        <?= $form->field($model, 'date')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ]
        ]); ?>

        <?= $form->field($model, 'date_t2')->widget(DatePicker::class, [
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
            ]
        ]); ?>

        <?= $form->field($model, 'signs')->textInput() ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
