<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Dividend */

$this->title = 'Create Dividend';
$this->params['breadcrumbs'][] = ['label' => 'Dividends', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dividend-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
