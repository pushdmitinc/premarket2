<?php

use common\models\Dividend;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DividendSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dividends';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dividend-index box box-primary">
    <div class="box-header with-border">
        <?= Html::a('Create Dividend', ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= $this->render('_index', ['searchModel' => $searchModel, 'dataProvider' => $dataProvider]) ?>
    </div>
</div>
