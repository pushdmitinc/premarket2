<?php

use common\models\Dividend;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\DividendSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'layout' => "{items}\n{summary}\n{pager}",
    'columns' => [
        'ticker',
        'period',
        'date',
        'date_t2',
        'stock_price',
        'dividend',
        'profitability' => [
            'class' => 'yii\grid\DataColumn',
            'label' => 'Доходность',
            'attribute' => 'profitability',
            'value' => function(Dividend $model) {
                return $model->getPercent();
            },
        ],
        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>
