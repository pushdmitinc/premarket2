<?php

use common\helpers\MoexHelper;
use common\models\stock\StockEntity;
use common\widgets\Modal\ModalWidget;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\stock\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Фьючерсы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moex-stock-index box box-primary">
    <div class="box-body table-responsive no-padding">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'id',
                    'headerOptions' => ['style' => 'width:80px;'],
                ],
                'name' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'name',
                    'value' => function (StockEntity $model) {
                        return mb_substr($model->name, 0, 24);
                    },
                ],
                'ticker' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'ticker',
                    'headerOptions' => ['style' => 'width:120px;'],
                    'contentOptions' => function (StockEntity $model) {
                        return ['title' => $model->name];
                    },
                ],
                'stock' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'stock',
                    'value' => function (StockEntity $model) {
                        return $model->futureStock->stock->ticker ?? '-';
                    },
                ],
                'date_prev' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'date_prev',
                    'format' => 'date',
                    'headerOptions' => ['style' => 'width:140px;'],
                    'contentOptions' => function () {
                        return ['style' => 'text-align:center;'];
                    },
                ],
                'price_prev' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'price_prev',
                    'format' => 'currency',
                    'contentOptions' => function () {
                        return ['style' => 'text-align:right;'];
                    },
                ],
                'price_stock' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'price_stock',
                    'label' => 'Цена актива',
                    'contentOptions' => function () {
                        return ['style' => 'text-align:right;'];
                    },
                    'value' => function (StockEntity $model) {
                        $price = $model->futureStock->stock->price_prev ?? null;

                        if ($price === null) {
                            return '-';
                        }

                        return Yii::$app->formatter->asCurrency($price);
                    },
                ],
                'percent_diff' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'percent_diff',
                    'label' => 'Разница',
                    'contentOptions' => function () {
                        return ['style' => 'text-align:right;'];
                    },
                    'value' => function (StockEntity $model) {
                        $percent = $model->futureStock->percent_diff ?? null;

                        if ($percent === null) {
                            return '-';
                        }

                        return Yii::$app->formatter->asPercent($percent / 100, 2);
                    },
                ],
                'position' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'position',
                    'label' => 'Позиция % ₽',
                    'format' => 'raw',
                    'contentOptions' => function () {
                        return ['style' => 'text-align:center;white-space:nowrap'];
                    },
                    'value' => function (StockEntity $model) {
                        $futureStock = $model->futureStock;

                        if ($futureStock === null || $futureStock->price_future === null || $futureStock->price_stock === null ) {
                            return '-';
                        }

                        $priceStock = $futureStock->stock->price_prev ?? null;

                        if ($model->price_prev === null || $priceStock === null) {
                            return '-';
                        }

                        $priceStock = $priceStock * $futureStock->quantity;
                        $priceStockFuture = $futureStock->price_stock * $futureStock->quantity;

                        if ($futureStock->price_future > $priceStockFuture) {
                            $priceDiff = $futureStock->price_future - $model->price_prev;
                            $priceDiff += $priceStock - $priceStockFuture;

                            $percent = ($model->price_prev / $futureStock->price_future - 1) * -1;
                            $percent += $priceStock / $priceStockFuture - 1;
                        } else {
                            $priceDiff = $model->price_prev - $futureStock->price_future;
                            $priceDiff += $priceStockFuture - $priceStock;

                            $percent = $model->price_prev / $futureStock->price_future - 1;
                            $percent += $priceStockFuture / $priceStock  - 1;
                        }

                        $priceDiff = $priceDiff * ($futureStock->quantity_position ?: 1);

                        $value = Yii::$app->formatter->asPercent($percent, 2);
                        $value .= ' ' . Yii::$app->formatter->asCurrency($priceDiff);

                        return Html::tag('span', $value);
                    },
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {future-update}',
                    'buttons' => [
                        'future-update' => function ($url) {
                            return ModalWidget::widget([
                                'url' => $url,
                                'content' => '<i class="fa fa-edit"></i>',
                            ]);
                        },
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
