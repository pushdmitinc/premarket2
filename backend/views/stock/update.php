<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StockEntity */

$this->title = 'Update Moex Stock: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Moex Securities', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="moex-stock-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
