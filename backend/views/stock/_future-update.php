<?php

use common\widgets\Modal\ModalWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\stock\StockFuture */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modal-dialog" role="document">
    <div class="box box-primary" role="document">
        <div class="modal-content box-body table-responsive">
            <div class="modal-header">
                <h5 class="modal-title">Сравнение</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="form-new">
                    <?php $form = ActiveForm::begin(); ?>

                        <div class="row" style="margin-top:5px;">
                            <div class="col-md-12">
                                <?= $form->field($model, 'future_id')->textInput() ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'stock_id')->textInput() ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'price_future')->textInput() ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'price_stock')->textInput() ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'quantity')->textInput() ?>
                            </div>
                            <div class="col-md-12">
                                <?= $form->field($model, 'quantity_position')->textInput() ?>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>

            <div class="modal-footer">
                <?= ModalWidget::widget([
                    'url' => ['/stock/future-update'],
                    'content' => 'Сохранить',
                    'cssClass' => 'btn btn-success',
                    'sendClass' => 'modal-send-form',
                ]) ?>
                <?= Html::button('Отмена', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
            </div>
        </div>
    </div>
</div>
