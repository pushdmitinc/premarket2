<?php

use common\helpers\MoexHelper;
use common\models\stock\StockEntity;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\stock\StockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Акции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moex-stock-index box box-primary">
    <div class="box-body table-responsive no-padding">

        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                'id',
                'ticker' => [
                    'class' => 'yii\grid\DataColumn',
                    'attribute' => 'ticker',
                    'contentOptions' => function (StockEntity $model) {
                        return ['title' => $model->name];
                    },
                ],
                'class_code',
                'name',
                'date_prev',
                'price_prev',
                'price_ytd_max',
                'price_ytd_min',
                'percent_ytd_max' => [
                    'attribute' => 'percent_ytd_max',
                    'contentOptions' => function ($model) {
                        return ['class' => 'text-right ' . MoexHelper::maxColor($model)];
                    },
                ],
                'percent_ytd_min',
                'percent_day',
                'percent_ytd',
                'percent_year',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                ],
            ],
        ]); ?>
    </div>
</div>
