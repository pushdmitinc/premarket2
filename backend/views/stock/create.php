<?php

/* @var $this yii\web\View */
/* @var $model common\models\StockEntity */

$this->title = 'Create Stock';
$this->params['breadcrumbs'][] = ['label' => 'Moex Securities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moex-stock-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
