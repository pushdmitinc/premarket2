<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\stock\StockEntity */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Moex Securities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="moex-stock-view box box-primary">
    <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-flat']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger btn-flat',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'ticker',
                'board',
                'name',
                'price_prev',
                'lot_size',
                'face_value',
                'status',
                'decimals',
                'remarks',
                'market_code',
                'instr',
                'sector',
                'min_step',
                'face_unit',
                'currency',
                'date_prev',
                'issue_size',
                'isin',
                'type',
            ],
        ]) ?>
    </div>
</div>
