<?php

namespace backend\modules\api\modules\v1\controllers;

use backend\modules\api\controllers\BaseController;
use Yii;

class TestController extends BaseController
{
    public $modelClass = 'backend\modules\api\models\Stock';

    /**
     * @return array
     */
    public function actionIndex(): array
    {
        return [
            [
                'id' => 'bitcoin',
                'amount' => 0.025,
                'price' => 26244,
                'date' => '2025-01-01',
            ],
            [
                'id' => 'ethereum',
                'amount' => 4,
                'price' => 2520,
                'date' => '2025-01-01',
            ],
            [
                'id' => 'dogecoin',
                'amount' => 10000,
                'price' => 0.072,
                'date' => '2025-01-01',
            ],
        ];
    }
}
