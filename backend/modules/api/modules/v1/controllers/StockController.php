<?php

namespace backend\modules\api\modules\v1\controllers;

use backend\modules\api\models\Stock;
use backend\modules\api\models\StockSearch;
use backend\modules\api\controllers\BaseController;
use common\helpers\ArrayHelper;
use Yii;

/**
 * StockController implements the CRUD actions for StockEntity model.
 */
class StockController extends BaseController
{
    public $modelClass = 'backend\modules\api\models\Stock';

    /**
     * @param string|null $q
     *
     * @return array
     */
    public function actionIndex(string $q = null): array
    {
        $searchModel = new StockSearch();

        if ($q) {
            $searchModel->ticker = $q;
        } else {
            $searchModel->isPopular = true;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $dataProvider->getModels();
    }

    /**
     * @param string|null $q
     *
     * @return array
     */
    public function actionSearch(string $q = null): array
    {
        $select = ['id', 'name', 'ticker', 'price_prev'];

        if (!$q) {
            return Stock::find()
                ->select($select)
                ->andWhere(['is_portfolio' => true])
                ->andWhere(['not', ['price_prev' => null]])
                ->limit(10)
                ->all();
        }

        $stocks = Stock::find()
            ->select($select)
            ->andWhere(['ticker' => strtoupper($q)])
            ->andWhere(['not', ['price_prev' => null]])
            ->limit(1)
            ->all();

        if (count($stocks) < 10) {
            $query = Stock::find()
                ->select($select)
                ->andWhere(['not', ['price_prev' => null]])
                ->andWhere(['ilike', 'ticker', $q . '%', false])
                ->limit(10 - count($stocks));

            $notTicker = ArrayHelper::getColumn($stocks, 'ticker');
            if ($notTicker) {
                $query->andWhere(['not', ['ticker' => $notTicker]]);
            }

            $stocks = ArrayHelper::merge($stocks, $query->all());
        }

        if (count($stocks) < 10) {
            $query = Stock::find()
                ->select($select)
                ->andWhere(['ilike', 'ticker', $q])
                ->andWhere(['not', ['price_prev' => null]])
                ->limit(10 - count($stocks));

            $notTicker = ArrayHelper::getColumn($stocks, 'ticker');
            if ($notTicker) {
                $query->andWhere(['not', ['ticker' => $notTicker]]);
            }

            $stocks = ArrayHelper::merge($stocks, $query->all());
        }

        if (count($stocks) < 10) {
            $query = Stock::find()
                ->select($select)
                ->andWhere(['ilike', 'name', $q . '%', false])
                ->andWhere(['not', ['price_prev' => null]])
                ->limit(10 - count($stocks));

            $notTicker = ArrayHelper::getColumn($stocks, 'ticker');
            if ($notTicker) {
                $query->andWhere(['not', ['ticker' => $notTicker]]);
            }

            $stocks = ArrayHelper::merge($stocks, $query->all());
        }

        return $stocks;
    }
}
