<?php

namespace backend\modules\api\modules\v1\controllers;

use backend\modules\api\controllers\BaseController;
use backend\modules\api\models\Note;
use common\models\NoteGroup;

class NoteGroupController extends BaseController
{
    public $modelClass = 'backend\modules\api\models\NoteGroup';
}
