<?php

namespace backend\modules\api\modules\v1\controllers;

use backend\modules\api\controllers\BaseController;
use backend\modules\api\models\NoteGroup;

class NoteController extends BaseController
{
    public $modelClass = 'backend\modules\api\models\Note';

    /**
     * @return NoteGroup[]
     */
    public function actionGroup(): array
    {
        NoteGroup::$isRelation = true;

        return NoteGroup::find()
            ->orderBy(['sort' => SORT_ASC])
            ->with(['notes'])
            ->all();
    }
}
