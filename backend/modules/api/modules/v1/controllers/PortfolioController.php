<?php

namespace backend\modules\api\modules\v1\controllers;

use backend\modules\api\controllers\BaseController;
use backend\modules\api\models\Portfolio;
use backend\modules\api\models\PortfolioSearch;
use Yii;
use yii\web\NotFoundHttpException;

class PortfolioController extends BaseController
{
    public $modelClass = 'backend\modules\api\models\Portfolio';

    /**
     * @return array
     */
    public function actionIndex(): array
    {
        $searchModel = new PortfolioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $dataProvider->getModels();
    }

    /**
     * @param int $id
     * @param int|null $group
     *
     * @return Portfolio
     *
     * @throws NotFoundHttpException
     */
    public function actionViewFull(int $id, ?int $group = null): Portfolio
    {
        $portfolio = Portfolio::findOne($id);

        if (!$portfolio) {
            throw new NotFoundHttpException();
        }

        $portfolio->isFullData = true;

        if ($group) {
            $portfolio->groupType = $group;
        }

        return $portfolio;
    }
}
