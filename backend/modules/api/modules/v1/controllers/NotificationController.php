<?php

namespace backend\modules\api\modules\v1\controllers;

use backend\modules\api\controllers\BaseController;
use backend\modules\api\models\StockNotification;
use Yii;

/**
 * NotificationController implements the CRUD actions for StockEntity model.
 */
class NotificationController extends BaseController
{
    public $modelClass = 'backend\modules\api\models\StockNotification';

    /**
     * @return StockNotification[]
     */
    public function actionIndex(): array
    {
        $query = StockNotification::find();

        $query->with(['stock']);

        return $query->all();
    }

    /**
     * @return StockNotification
     */
    public function actionCreateJson(): StockNotification
    {
        $request = Yii::$app->request;

        $model = new StockNotification();
        $model->load($request->getBodyParams(), '');

        $model->save();

        return $model;
    }
}
