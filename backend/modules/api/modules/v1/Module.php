<?php

namespace backend\modules\api\modules\v1;

/**
 * api module definition class
 */
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\api\modules\v1\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
    }
}
