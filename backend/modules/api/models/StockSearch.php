<?php

namespace backend\modules\api\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class StockSearch extends Stock
{
    public bool $isPopular = false;

    /**
     * @inheritdoc
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function rules(): array
    {
        return [
            [['ticker', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios(): array
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params): ActiveDataProvider
    {
        $query = Stock::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['ticker' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['ilike', 'ticker', $this->ticker]);

        if ($this->isPopular) {
            $query->andFilterWhere(['is_portfolio' => true]);
        }

        return $dataProvider;
    }
}
