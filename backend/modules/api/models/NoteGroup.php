<?php

namespace backend\modules\api\models;

use common\helpers\CommonHelper;

/**
 * This is the model class for table "note_group".
 */
class NoteGroup extends \common\models\NoteGroup
{
    public static $isRelation = false;

    public function fields(): array
    {
        return [
            'id',
            'name',
            'items' => function() {
                return self::$isRelation ? $this->notes : [];
            },
        ];
    }

    public function load($data, $formName = null): bool
    {
        $result = parent::load($data, $formName);

        if ($result) {
            if (!$this->user_id) {
                $this->user_id = CommonHelper::getUserId();
            }
        }

        return $result;
    }
}
