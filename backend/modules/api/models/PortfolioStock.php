<?php

namespace backend\modules\api\models;

use Yii;

class PortfolioStock extends \common\models\PortfolioStock
{
    public function fields(): array
    {
        return [
            'id',
            'price',
            'quantity',
            'price_current' => function() {
                return $this->getPriceClose();
            },
        ];
    }
}
