<?php

namespace backend\modules\api\models;

use common\models\stock\StockEntity;

/**
 * This is the model class for table "stock".
 */
class Stock extends StockEntity
{
    public function fields(): array
    {
        return [
            'id',
            'name',
            'ticker',
            'symbol' => function() {
                return $this->ticker;
            },
            'price' => function() {
                return $this->price_prev;
            },
            'icon' => function() {
                return $this->getLogo();
            },
        ];
    }
}
