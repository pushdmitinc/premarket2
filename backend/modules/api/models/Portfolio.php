<?php

namespace backend\modules\api\models;

use common\models\portfolio\PortfolioCollection;
use common\models\portfolio\PortfolioStockCollection;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "stock".
 */
class Portfolio extends \common\models\Portfolio
{
    public bool $isFullData = false;

    public int $groupType = PortfolioStockCollection::GROUP_TYPE_CURRENCY;

    public function fields(): array
    {
        $fields = [
            'id',
            'name',
            'price' => function() {
                return round($this->totalPrice(), 2);
            },
            'price_current' => function() {
                return round($this->totalPriceCurrent(), 2);
            },
            'profit' => function() {
                return round($this->profit(), 2);
            },
            'percent' => function() {
                return $this->changePercent();
            },
            'count_stock' => function() {
                return count($this->portfolioStocks);
            },
        ];

        if ($this->isFullData) {
            $fields['collections'] = function() {
                return $this->getPositions();
            };
        }

        return $fields;
    }

    /**
     * @return PortfolioStockCollection[]
     */
    public function getStockGroup(): array
    {
        static $stockGroup = null;

        if ($stockGroup === null) {
            $stockGroup = PortfolioStockCollection::getStockGroup($this);
        }

        return $stockGroup;
    }

    /**
     * @return PortfolioStockCollection[]
     */
    public function getPositions(): array
    {
        /** @var PortfolioCollection[] $positions */
        $positions = [];

        foreach ($this->getStockGroup() as $collectionStock) {
            $index = $collectionStock->getGroupType($this->groupType);
            
            $price = $collectionStock->totalPrice() + ($positions[$index]->price ?? 0);
            $priceCurrent = $collectionStock->totalPriceClose() + ($positions[$index]->price_current ?? 0);
            $profit = $collectionStock->profit() + ($positions[$index]->profit ?? 0);

            $collection = $positions[$index] ?? new PortfolioCollection();

            $collection->id = $index;
            $collection->name = 'Тип ' . $index;
            $collection->price = $price;
            $collection->price_current = $priceCurrent;
            $collection->profit = $profit;
            $collection->currency = $collectionStock->getCurrency();
            $collection->currency_symbol = $collectionStock->getCurrencySymbol();

            $collection->add($collectionStock);

            $positions[$index] = $collection;
        }

        usort($positions, function($a, $b) {
            return $b->price_current - $a->price_current;
        });

        return array_values($positions);
    }

    public function getPortfolioStocksFull(): ActiveQuery
    {
        return $this->hasMany(PortfolioStock::class, ['portfolio_id' => 'id'])
            ->with(['stock', 'dividendTotal'])
            ->orderBy(['date' => SORT_ASC]);
    }
}
