<?php

namespace backend\modules\api\models;

class StockNotification extends \common\models\stock\StockNotification
{
    /**
     * {@inheritdoc}
     */
    public function fields(): array
    {
        return [
            'id',
            'user_id',
            'stock_id',
            'portfolio_id',
            'stock_price',
            'take_profit',
            'stop_loss',
            'text',
            'price_current' => function () {
                return $this->stock->price_prev;
            },
            'name' => function () {
                return $this->stock->name;
            },
            'ticker' => function () {
                return $this->stock->ticker;
            },
            'logo' => function () {
                return $this->stock->getLogo();
            },
            'currency' => function () {
                return $this->stock->currency;
            },
            'currency_symbol' => function () {
                return $this->stock->getCurrencySymbol();
            },
            'error' => function () {
                return $this->getFirstErrors()[0] ?? "";
            },
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate(): bool
    {
        if ($this->user_id == null) {
            $this->user_id = $this->portfolio->user_id ?? null;
        }

        if ($this->stock_price == null) {
            $this->stock_price = $this->stock->price_prev;
        }

//        var_dump($this->date); die;

        return parent::beforeValidate();
    }
}

