<?php

namespace backend\modules\api\models;

use common\helpers\CommonHelper;

/**
 * This is the model class for table "note".
 */
class Note extends \common\models\Note
{
//    public function fields(): array
//    {
//        return [
//            'id',
//            'text',
//        ];
//    }

    public function load($data, $formName = null): bool
    {
        $result = parent::load($data, $formName);

        if ($result) {
            if (!$this->user_id) {
                $this->user_id = CommonHelper::getUserId();
            }

            $group = $data['group'] ?? null;

            if ($group !== null) {
                $groupModel = NoteGroup::findOne(['name' => $group]);

                if ($groupModel === null) {
                    $groupModel = new NoteGroup();
                    $groupModel->user_id = $this->user_id;
                    $groupModel->name = $group;
                    $groupModel->save();
                }

                $this->group_id = $groupModel->id;
            }
        }

        return $result;
    }
}
