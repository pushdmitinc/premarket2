<?php

namespace backend\modules\api\models;

use yii\db\ActiveQuery;

class PortfolioSearch extends \common\models\PortfolioSearch
{
    /**
     * @inheritdoc
     */
    public function formName(): string
    {
        return '';
    }

    /**
     * @inheritdoc
     */
    public function query(): ActiveQuery
    {
        return Portfolio::find();
    }
}
