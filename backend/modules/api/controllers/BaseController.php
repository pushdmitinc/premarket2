<?php

namespace backend\modules\api\controllers;

use Yii;
use yii\filters\ContentNegotiator;
use yii\rest\ActiveController;
use yii\rest\Controller;
use yii\web\Response;

class BaseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors(): array
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function afterAction($action, $result)
    {
        Yii::$app->response->headers->add('Access-Control-Allow-Origin', '*');

        return parent::afterAction($action, $result);
    }
}
